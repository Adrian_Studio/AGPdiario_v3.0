const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');
const methodOverride = require('method-override');
const session = require('express-session');
const flash = require('connect-flash');
const passport = require('passport');
const device = require('express-device');
const schedule = require('node-schedule');
const logger = require('morgan')
const hbsHeplers = require('./helpers/handlebars');
const getAlerts = require('./helpers/getAlerts');
const backVariables = require('./globalVariables/variables');
const dateFormat = require('dateformat');

// Initializations
const app = express();
require('./database');
require('./config/passport');

//Static Files
app.use(express.static(path.join(__dirname, 'public')));

//Settings
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('helpers', path.join(__dirname, 'helpers'));
var hbs = exphbs.create({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs'
});
app.engine("hbs", hbs.engine);
app.set('view engine', '.hbs');

//Middlewares
app.use(logger('dev'))
app.use(express.urlencoded({ extended: false }));
app.use(methodOverride('_method'));
app.use(session({
    secret: 'mysecretapp',
    resave: true,
    saveUninitialized: true,
    rolling: true,
    cookie: {
        maxAge: 600000
    }
}));
app.use(passport.initialize());
app.use(passport.session())

//Global Variables
app.use(device.capture());
app.use(flash());
app.use((req, res, next) => {
    frontConst = require('./globalVariables/constants');
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    res.locals.deviceType = req.device.type;
    res.locals.globalMonthsArray = frontConst.globalMonthsArray;
    res.locals.initialUrl = backVariables.initialUrl;
    res.locals.currentUrl = backVariables.currentUrl;
    res.locals.currentAlerts = backVariables.currentAlerts;
    res.locals.sections = backVariables.sections;
    res.locals.sidebarCollapsed = backVariables.sidebarCollapsed;
    res.locals.sidebarRight = backVariables.sidebarRight;
    res.locals.signUpEnabled = backVariables.signUpEnabled;
    res.locals.lastBankUpdate = backVariables.lastBankUpdate;
    next();
})

//Helppers
hbsHeplers.register(hbs.handlebars);

//Routes
app.use(require('./routes/index'));
app.use(require('./routes/annotations'));
app.use(require('./routes/users'));
app.use(require('./routes/accounting'));
app.use(require('./routes/inventory'));
app.use(require('./routes/trips'));

//Schedule scripts
schedule.scheduleJob('0 0 0 * * *', async function(){ // It runs every night at midnight
    getAlerts.updateAll();
    console.log("Borrar usuarios sin verificar con más de 3 días")
});

//Server is 
app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
});

