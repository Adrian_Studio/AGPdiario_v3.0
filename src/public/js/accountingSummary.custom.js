/* Add here all your JS customizations */
$(document).ready(function () {
    $('#summaryDatatable').DataTable({

        responsive: true,
        columnDefs: [
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: 1 },
        ],
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        //order: [[ 0, "desc" ]],
        "ordering": false,
        //"autoWidth": false,
        "pageLength": -1,
        "paging": false,
        "searching": false,
        "info": false,
    });
    $('#summaryDatatable').css('opacity', 'unset');
    $('.lds-ellipsis').css('display', 'none');
    $("#date, #startDateFilter, #endDateFilter").datepicker({
        "format": "dd/mm/yyyy",
        "todayHighlight": true,
        "weekStart": 1,
        "autoclose": true,
        //"calendarWeeks": true, 
        "clearBtn": true,
        "language": "es",
        "daysOfWeekHighlighted": [0, 6],
    });

    $("#date, #startDateFilter, #endDateFilter").datepicker({
        "format": "dd/mm/yyyy",
        "todayHighlight": true,
        "weekStart": 1,
        "autoclose": true,
        //"calendarWeeks": true, 
        "clearBtn": true,
        "language": "es",
        "daysOfWeekHighlighted": "0,6",
    });
    $('#startDateFilter').change(function (e) {
        $('#endDateFilter').datepicker('setStartDate', e.target.value);
        if (e.target.value || $('#endDateFilter').val()) {
            $('.specificDates').css('display', 'none');
        } else {
            $('.specificDates').css('display', 'block');
        }
    });
    $('#endDateFilter').change(function (e) {
        $('#startDateFilter').datepicker('setEndDate', e.target.value);
        if (e.target.value || $('#startDateFilter').val()) {
            $('.specificDates').css('display', 'none');
        } else {
            $('.specificDates').css('display', 'block');
        }
    });
    $('#monthFilter, #yearFilter').change(function (e) {
        if ($('#monthFilter').val() || $('#yearFilter').val()) {
            $('.betweenDates').css('display', 'none');
        } else {
            $('.betweenDates').css('display', 'block');
        }
    });
    if ($('#monthFilter').val() || $('#yearFilter').val()) {
        $('.betweenDates').css('display', 'none');
    } else {
        $('.betweenDates').css('display', 'block');
    }
    if ($('#startDateFilter').val() || $('#endDateFilter').val()) {
        $('.specificDates').css('display', 'none');
    } else {
        $('.specificDates').css('display', 'block');
    }
    if(tab == 'annual'){
        $("#yearFilter").removeAttr('multiple');
        $("#yearFilter").multiselect('rebuild');    
    }
    $('#groupSummary').change(function (e) {
        window.location.href = window.location.origin + '/accounting/summary/' + summaryType  + e.target.value;
    });
 });



function closeModal() {
    $('#modal-backdrop').removeClass();
    $('.modal, #modal-form').removeClass('show');
}
function closeModalFilter() {
    $('#modal-backdrop').removeClass();
    $('.modal, #modal-form-filter').removeClass('show');
}

function getFilter(tab) {
    switch (tab) {


        default:

            break;
    }

    $('#modal-form-filter').attr('action', '/accounting/summary/filter/' + tab);
    //$('#modal-form-filter')[0].reset();
    $('#modal, #modal-form-filter').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}