/* Add here all your JS customizations */
$(document).ready(function () {

    $('#inventoryDatatable').DataTable({
        columnDefs: [
            { render: $.fn.dataTable.moment( 'DD/MM/YYYY' ), targets: [7,9] },
            { type: 'formated-sorting', targets: [8] },
        ],
        //order: [[1, "asc"]],
    });
    $('#inventoryDatatable').css('opacity', 'unset');
    $('.lds-ellipsis').css('display', 'none');
    $("#acquisitionDate, #possesionLostDate").datepicker({
        "format": "dd/mm/yyyy",
        "todayHighlight": true,
        "weekStart": 1,
        "autoclose": true,
        //"calendarWeeks": true, 
        "clearBtn": true,
        "language": "es",
        "daysOfWeekHighlighted": "0,6",
    });
});

async function getCopyData(tab, id) {
    $('#formAction').text('Copiar');
    const data = await $.get("/getEditData/element/" + id, function (data) { });

    $('#modal-form').attr('action', '/inventory/element/add/' + tab);
    $('#modal-form')[0].reset();
    $('#place, #type').multiselect("refresh");
    switch (tab) {
        case 'Write tab name':
            break;

        default:
            Object.entries(data.variable).forEach(element => {
                if (element[0] == 'acquisitionDate' || element[0] == 'possesionLostDate') {
                    if(element[1]){
                        $('#' + element[0]).val(new Date(element[1]).toLocaleString('en-GB', { year: "numeric", month: "2-digit", day: "2-digit" }))
                    }                  
                } else if (element[0] == 'acquisitionPrice') {
                    $('#' + element[0]).val(decimalVal(element[1], 2));
                } else {
                    $('#' + element[0]).val(element[1]);
                }
            });
            break;
    }
    $('#place, #type').multiselect("refresh")
    $('#modal, #modal-form').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

async function getEditData(tab, id) {
    $('#formAction').text('Editar');
    const data = await $.get("/getEditData/element/" + id, function (data) { });

    $('#modal-form').attr('action', '/inventory/element/edit/' + id + "/" + tab);
    $('#modal-form')[0].reset();
    $('#place, #type').multiselect("refresh");
    switch (tab) {
        case 'Write tab name':
            break;

        default:
            Object.entries(data.variable).forEach(element => {
                if (element[0] == 'acquisitionDate' || element[0] == 'possesionLostDate') {
                    if(element[1]){
                        $('#' + element[0]).val(new Date(element[1]).toLocaleString('en-GB', { year: "numeric", month: "2-digit", day: "2-digit" }))
                    }   
                } else if (element[0] == 'acquisitionPrice') {
                    $('#' + element[0]).val(decimalVal(element[1], 2));
                } else {
                    $('#' + element[0]).val(element[1]);
                }
            });
            break;
    }
    $('#place, #type').multiselect("refresh");
    $('#modal, #modal-form').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

function getAddData(tab) {
    switch (tab) {
        case 'Write tab name':
            break;
        default:
            break;
    }
    $('#formAction').text('Añadir');
    $('#modal-form').attr('action', '/inventory/element/add/' + tab);
    $('#modal-form')[0].reset();
    $('#place, #type').multiselect("refresh");
    $('#modal, #modal-form').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

function deleteData(tab, id) {
    Swal.fire({
        title: '¿Seguro de que quieres borrar el elemento?',
        type: 'warning',
        confirmButtonColor: '#d33',
        showCancelButton: true,
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Sí, borrar',
        cancelButtonText: 'No, cancelar'
    }).then((result) => {
        if (result.value) {
            window.location.href = "/inventory/element/delete/" + id + "/" + tab;
        }
    });
}

function closeModal() {
    $('#modal-backdrop').removeClass();
    $('.modal, #modal-form').removeClass('show');
}
function closeModalFilter() {
    $('#modal-backdrop').removeClass();
    $('.modal, #modal-form-filter').removeClass('show');
}

function getFilter(tab) {
    switch (tab) {


        default:

            break;
    }

    //$('#modal-form-filter').attr('action', 'inventory/element/filter/' + tab);
    //$('#modal-form-filter')[0].reset();
    $('#modal, #modal-form-filter').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}