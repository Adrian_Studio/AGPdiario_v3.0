var conceptIncomeChartFilled = undefined;
var conceptExpenseChartFilled = undefined;

/* Add here all your JS customizations */
$(document).ready(function () {
  $('#balanceAmountR').text(euros(balanceChartData.BalanceR[balanceChartData.BalanceR.length - 1]));
  $('#balanceAmountT').text(euros(balanceChartData.BalanceT[balanceChartData.BalanceT.length - 1]));
  $('#incomeAmount').text(euros(balanceChartData.Income.reduce((a, b) => a + b)));
  $('#expenseAmount').text(euros(balanceChartData.Expense.reduce((a, b) => a + b)));

  var myBarChart = new Chart($("#balanceChart"), {
    responsive: true,
    type: 'line',
    data: {
      labels: balanceChartData.Label,
      datasets: [{
        label: "Balance (Cashflow)",
        fill: true,
        backgroundColor: "rgba(255, 164, 32, 0.3)",
        borderColor: 'rgba(255,164,32,0.3)',
        pointBackgroundColor: 'rgba(255,164,32,0.6)',
        data: balanceChartData.BalanceR,
        spanGaps: false,
        pointRadius: 1,
        lineTension: 0,
      }, {
        label: "Balance (Teórico)",
        fill: true,
        backgroundColor: "rgba(100, 200, 225, 0.1)",
        borderColor: 'rgba(100, 200, 225,0.3)',
        pointBackgroundColor: 'rgba(100, 200, 225,0.6)',
        data: balanceChartData.BalanceT,
        spanGaps: false,
        pointRadius: 1,
        lineTension: 0,
      }, {
        label: "Ingreso",
        fill: false,
        backgroundColor: "rgba(0, 170, 0, 0.3)",
        borderColor: "rgba(0, 170, 0, 0.6)",
        data: balanceChartData.Income,
        spanGaps: true,
        showLine: false,
        pointRadius: 2,
        lineTension: 0,
      }, {
        label: "Gasto",
        fill: false,
        backgroundColor: "rgba(255, 0, 0, 0.3)",
        borderColor: "rgba(255, 0, 0, 0.6)",
        data: balanceChartData.Expense,
        spanGaps: false,
        showLine: false,
        pointRadius: 2,
        lineTension: 0,
      }, {
        label: "Acciones",
        fill: false,
        backgroundColor: "rgba(100, 200, 225, 0.3)",
        borderColor: "rgba(100, 200, 225, 0.6)",
        data: balanceChartData.Stock,
        spanGaps: false,
        showLine: false,
        pointRadius: 2,
        lineTension: 0,
      }
      ]
    },
    options: {
      aspectRatio: 3,
      scales: {
        // We use this empty structure as a placeholder for dynamic theming.
        xAxes: [{
          display: (window.innerWidth < 500) ? false : true,
          type: 'time',
          time: {
            displayFormats: {
              millisecond: 'DD MMM YYYY',
              second: 'DD MMM YYYY',
              minute: 'DD MMM YYYY',
              hour: 'DD MMM YYYY',
              day: 'MMM YYYY',
              week: 'MMM YYYY',
              month: 'MMM YYYY',
              quarter: 'MMM YYYY',
              year: 'MMM YYYY',
            },
          },
          ticks: {
            callback: function (label, index, labels) {
              return translate_this_label(label);
            }
          }
          // offset: true,
        }],
        yAxes: [{
          //type: 'logarithmic',

          type: 'linear',
          //stacked: true,
          ticks: {
            beginAtZero: true,
            fontSize: (window.innerWidth < 500) ? 10 : 12,
            userCallback: function (tick) {
              return tick.toLocaleString("es", { style: "decimal", maximumFractionDigits: 1 }) + " €";
            },
          },
          scaleLabel: {
            //labelString: 'Voltage',
            //display: false
          }
        }],
      },
      legend: {
        display: (window.innerWidth < 500) ? false : true,
        labels: {
          fontSize: (window.innerWidth < 500) ? 6 : 12,
        }
      },
      onResize: function (chart, size) {
        if (size.width < 400) {
          chart.legend.options.display = false;
          chart.options.scales.xAxes[0].display = false;
          //chart.options.scales.yAxes[0].ticks.fontSize = 10; Da problemas
          chart.legend.options.labels.fontSize = 6;
          //chart.options.scales.yAxes[0].display = false;
        } else {
          chart.legend.options.display = true;
          chart.options.scales.xAxes[0].display = true;
          //chart.options.scales.yAxes[0].ticks.fontSize = 10; Da problemas
          chart.legend.options.labels.fontSize = 12;
          //chart.options.scales.yAxes[0].display = true;
        }
      },
      tooltips: {
        callbacks: {
          title: function (tooltipItem, data) {
            return new Date(tooltipItem[0].label).toLocaleString('en-GB', { year: "numeric", month: "2-digit", day: "2-digit" });
          },
          /*beforeLabel: function(tooltipItem, data) {
            if(tooltipItem.datasetIndex != 0){
                return data.datasets[0].label +': ' +euros(data.datasets[0].data[tooltipItem.index]);
            }                   

          },*/
          label: function (tooltipItem, data) {
            amount = euros(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
            observation = balanceChartData.Observation[tooltipItem.index];
            return data.datasets[tooltipItem.datasetIndex].label + ': ' + amount + " -> " + observation;

          },
          footer: function (tooltipItem, data) {
            //console.log(tooltipItem,data)
            return ".............................................";
          }
        },
      },
    },
  });
  if (pieChartData['Ingreso-Categories']) {
    pieChart($('#categoryIncomeChart'), pieChartData['Ingreso-Categories']);
  }
  if (pieChartData['Gasto-Categories']) {
    pieChart($('#categoryExpenseChart'), pieChartData['Gasto-Categories']);
  }


  $('.lds-ellipsis').css('display', 'none');
  $("#startDateFilter, #endDateFilter").datepicker({
    "format": "dd/mm/yyyy",
    "todayHighlight": true,
    "weekStart": 1,
    "autoclose": true,
    //"calendarWeeks": true, 
    "clearBtn": true,
    "language": "es",
    "daysOfWeekHighlighted": [0, 6],
  });
  $("#date, #startDateFilter, #endDateFilter").datepicker({
    "format": "dd/mm/yyyy",
    "todayHighlight": true,
    "weekStart": 1,
    "autoclose": true,
    //"calendarWeeks": true, 
    "clearBtn": true,
    "language": "es",
    "daysOfWeekHighlighted": "0,6",
  });
  $('#startDateFilter').change(function (e) {
    $('#endDateFilter').datepicker('setStartDate', e.target.value);
    if (e.target.value || $('#endDateFilter').val()) {
      $('.specificDates').css('display', 'none');
    } else {
      $('.specificDates').css('display', 'block');
    }
  });
  $('#endDateFilter').change(function (e) {
    $('#startDateFilter').datepicker('setEndDate', e.target.value);
    if (e.target.value || $('#startDateFilter').val()) {
      $('.specificDates').css('display', 'none');
    } else {
      $('.specificDates').css('display', 'block');
    }
  });
  $('#monthFilter, #yearFilter').change(function (e) {
    if ($('#monthFilter').val() || $('#yearFilter').val()) {
      $('.betweenDates').css('display', 'none');
    } else {
      $('.betweenDates').css('display', 'block');
    }
  });
  if ($('#monthFilter').val() || $('#yearFilter').val()) {
    $('.betweenDates').css('display', 'none');
  } else {
    $('.betweenDates').css('display', 'block');
  }
  if ($('#startDateFilter').val() || $('#endDateFilter').val()) {
    $('.specificDates').css('display', 'none');
  } else {
    $('.specificDates').css('display', 'block');
  }
  $('#graphsPanel').css('display', 'block');
});

arrayColumn = (arr, n) => arr.map(x => x[n]);

function pieChart(ctx, data) {
  colors = dynamicColors(data.length);
  var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
      labels: arrayColumn(data, 0),
      datasets: [{
        label: arrayColumn(data, 0),
        data: arrayColumn(data, 1),
        backgroundColor: arrayColumn(colors, 1),
        borderColor: arrayColumn(colors, 0),
        borderWidth: 1
      }]
    },
    options: {
      aspectRatio: 3,
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          },
          display: false
        }]
      },
      legend: {
        display: (window.innerWidth < 500) ? false : true,
        position: 'right',
        fullWidth: true,
        hidden: false,
        labels: {
          fontSize: 12
        }
      },
      onResize: function (chart, size) {
        //console.log(chart);
        if (size.width < 400) {
          chart.legend.options.display = false;
        } else {
          chart.legend.options.display = true;
        }
        //console.log(data.legend.options.display)
      },
      tooltips: {
        callbacks: {
          title: function (tooltipItem, data) {
            conceptoSeleccionado = data['labels'][tooltipItem[0]['index']];
            return conceptoSeleccionado;

          },
          label: function (tooltipItem, data) {
            var dataset = 0;
            $.each(data['datasets'][0]['data'], function (index, element) {
              dataset += parseInt(element);
            });
            //var dataset = data['datasets'][0]['_meta'][1]['total'];
            var percent = Math.round((data['datasets'][0]['data'][tooltipItem['index']] * 100) / dataset);
            return euros(data['datasets'][0]['data'][tooltipItem['index']]);
          },
          afterLabel: function (tooltipItem, data) {
            var dataset = 0;
            $.each(data['datasets'][0]['data'], function (index, element) {
              dataset += parseInt(element);
            });
            //var dataset = data['datasets'][0]['_meta'][1]['total'];
            var percent = Math.round((data['datasets'][0]['data'][tooltipItem['index']] * 100) / dataset);
            return '(' + percent + '%)';
          },
          footer: function (tooltipItem, data) {
            //console.log(tooltipItem,data)
            return ".............................";
          }
        },
      },
      onClick: function (evt, shape) {
        if (evt.target.id.startsWith('category')) {
          var type = evt.target.id.replace('category', '').replace('Chart', '');
          if (shape.length) {
            var category = shape[0]._chart.controller.legend.legendItems[shape[0]._index].text;
            if (type == 'Income') {
              if (conceptIncomeChartFilled) { conceptIncomeChartFilled.destroy() }
              conceptIncomeChartFilled = pieChart($('#concept' + type + 'Chart'), pieChartData[category + '-Concepts']);
            }
            if (type == 'Expense') {
              if (conceptExpenseChartFilled) { conceptExpenseChartFilled.destroy() }
              conceptExpenseChartFilled = pieChart($('#concept' + type + 'Chart'), pieChartData[category + '-Concepts']);
            }
            $('#' + type.toLowerCase() + 'CategoryAmount').html(category + ': <strong>' + euros(arrayColumn(pieChartData[category + '-Concepts'], 1).reduce((a, b) => a + b)) + '</strong>');
            $('#category' + type + 'Graph').css('display', 'block')
          }
        }
      }
    }
  });
  return myChart
}

function getFilter(tab) {
  switch (tab) {


    default:

      break;
  }
  $('#modal-form-filter').attr('action', '/accounting/graphs/filter/' + tab);
  //$('#modal-form-filter')[0].reset();
  $('#modal, #modal-form-filter').addClass('show');
  $('#modal-backdrop').addClass('modal-backdrop show');
}

function dynamicColors(arrayLenght) {
  var min = 70;
  var max = 220;
  arrayColors = [];
  var r = min;
  var g = max;
  var b = min;

  if (arrayLenght <= 6) {
    var escalon = (max - min);
  } else {
    var escalon = Math.ceil((max - min) / (((arrayLenght / 3) - 1) / 2));
  }
  for (let i = 0; i < arrayLenght; i++) {
    if (!(r <= min) && (g >= max) && (b <= min)) {
      r -= escalon;
    } else if ((r <= min) && (g >= max) && !(b >= max)) {
      b += escalon;
    } else if ((r <= min) && !(g <= min) && (b >= max)) {
      g -= escalon;
    } else if (!(r >= max) && (g <= min) && (b >= max)) {
      r += escalon;
    } else if ((r >= max) && (g <= min) && !(b <= min)) {
      b -= escalon;
    } else if ((r >= max) && !(g >= max) && (b <= min)) {
      g += escalon;
    }
    arrayColors.push(["rgb(" + r + "," + g + "," + b + ", 0.3)", "rgb(" + r + "," + g + "," + b + ", 0.2)"]);
  }
  return arrayColors;
};

function closeModalFilter() {
  $('#modal-backdrop').removeClass();
  $('.modal, #modal-form-filter').removeClass('show');
}

function translate_month(month) {

  var result = month;

  switch (month) {

    case 'Jan':
      result = 'Ene';
      break;
    case 'Apr':
      result = 'Abr';
      break;
    case 'Aug':
      result = 'Ago';
      break;
  }

  return result;
}


function translate_this_label(label) {
  month = label.match(/Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Nov|Dec/g);

  if (!month)
    return label;

  translation = translate_month(month[0]);
  return label.replace(month, translation, 'g');
}