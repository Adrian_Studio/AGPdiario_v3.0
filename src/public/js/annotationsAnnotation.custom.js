/* Add here all your JS customizations */
$(document).ready(function () {
    if (tab == 'schedule') {
        var columnDefs = [
            { render: $.fn.dataTable.moment('DD/MM/YYYY'), targets: [1, 2] },
            //{ type: 'formated-sorting', targets: [3,4] },
        ]
    } else {
        columnDefs = [];
    }
    $('#annotationsDatatable').DataTable({
        columnDefs: columnDefs,
        order: [[1, "desc"]],
    });
    $('#annotationsDatatable').css('opacity', 'unset');
    $('.lds-ellipsis').css('display', 'none');
    $("#date, #alertDate").datepicker({
        "format": "dd/mm/yyyy",
        "todayHighlight": true,
        "weekStart": 1,
        "autoclose": true,
        //"calendarWeeks": true, 
        "clearBtn": true,
        "language": "es",
        "daysOfWeekHighlighted": "0,6",
    });
});

async function getCopyData(tab, id) {
    $('#formAction').text('Copiar');
    const data = await $.get("/getEditData/annotations/" + tab + "/" + id, function (data) { });

    $('#modal-form').attr('action', '/annotations/annotation/add/' + tab);
    $('#modal-form')[0].reset();
    $('#alertActive').multiselect("refresh");
    switch (tab) {
        case 'Write tab name':
            break;

        default:
            Object.entries(data.variable).forEach(element => {
                if (element[0] == 'date' || element[0] == 'alertDate') {
                    if (element[1]) {
                        $('#' + element[0]).val(new Date(element[1]).toLocaleString('en-GB', { year: "numeric", month: "2-digit", day: "2-digit" }))
                    }
                } else {
                    $('#' + element[0]).val(element[1]);
                }
            });
            break;
    }
    $('#alertActive').multiselect("refresh")
    $('#modal, #modal-form').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

async function getEditData(tab, id) {
    $('#formAction').text('Editar');
    const data = await $.get("/getEditData/annotations/" + tab + "/" + id, function (data) { });

    $('#modal-form').attr('action', '/annotations/annotation/edit/' + id + "/" + tab);
    $('#modal-form')[0].reset();
    $('#alertActive').multiselect("refresh");
    switch (tab) {
        case 'Write tab name':
            break;

        default:
            Object.entries(data.variable).forEach(element => {
                if (element[0] == 'date' || element[0] == 'alertDate') {
                    if (element[1]) {
                        $('#' + element[0]).val(new Date(element[1]).toLocaleString('en-GB', { year: "numeric", month: "2-digit", day: "2-digit" }))
                    }
                } else {
                    $('#' + element[0]).val(element[1]);
                }
            });
            break;
    }
    $('#alertActive').multiselect("refresh");
    $('#modal, #modal-form').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

function getAddData(tab) {
    switch (tab) {
        case 'Write tab name':
            break;
        default:
            break;
    }
    $('#formAction').text('Añadir');
    $('#modal-form').attr('action', '/annotations/annotation/add/' + tab);
    $('#modal-form')[0].reset();
    $('#alertActive').multiselect("refresh");
    $('#modal, #modal-form').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

function deleteData(tab, id) {
    Swal.fire({
        title: '¿Seguro de que quieres borrar el elemento?',
        type: 'warning',
        confirmButtonColor: '#d33',
        showCancelButton: true,
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Sí, borrar',
        cancelButtonText: 'No, cancelar'
    }).then((result) => {
        if (result.value) {
            window.location.href = "/annotations/annotation/delete/" + id + "/" + tab;
        }
    });
}

function closeModal() {
    $('#modal-backdrop').removeClass();
    $('.modal, #modal-form').removeClass('show');
}
function closeModalFilter() {
    $('#modal-backdrop').removeClass();
    $('.modal, #modal-form-filter').removeClass('show');
}
