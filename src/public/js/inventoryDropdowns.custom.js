/* Add here all your JS customizations */
$(document).ready(function () {

    $('#dropdownDatatable').DataTable({});
    $('#dropdownDatatable').css('opacity', 'unset');
    $('.lds-ellipsis').css('display', 'none');
});

function getEditData(tab, id) {
    $('#formAction').text('Editar');
    $.get("/getEditData/dropdowns/" + tab + '/' + id, function (data) {
        $('#modal-form').attr('action', '/inventory/dropdowns/' + tab + '/edit/' + id);
        switch (tab) {
            case 'place':
                $('#place').val(data.variable.place);
                break;
            case 'type':
                $('#type').val(data.variable.type);;
                break;

            default:
                alert('La pestaña no tiene asignada ninguna accion')
                break;
        }
        $('#modal, #modal-form').addClass('show');
        $('#modal-backdrop').addClass('modal-backdrop show');
    });

}

function getAddData(tab) {
    switch (tab) {
        case 'place':
            break;
        case 'type':
            break;

        default:
            alert('La pestaña no tiene asignada ninguna accion')
            break;
    }
    $('#formAction').text('Añadir');
    $('#modal-form').attr('action', '/inventory/dropdowns/' + tab + '/add');
    $('#modal-form')[0].reset();
    $('#modal, #modal-form').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

function deleteData(tab, id) {
    Swal.fire({
        title: '¿Seguro de que quieres borrar el elemento?',
        type: 'warning',
        confirmButtonColor: '#d33',
        showCancelButton: true,
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Sí, borrar',
        cancelButtonText: 'No, cancelar'
    }).then((result) => {
        if (result.value) {
            window.location.href = "/inventory/dropdowns/" + tab + "/delete/" + id;
        }
    });
}

function getPlaceData(category) {
    $.get("/getPlaceData/" + category, function (data) {
        $('#typeInfo').val(data.category.type);
        $('#groupInfo').val(data.category.group);

        $('#subgroup').attr('disabled', false);
        if (data.category.group == 'General' && data.category.type == 'Ingreso') {
            $('.homeExpGroup, .vehicleExpGroup').css('display', 'none');
            $('.homeExpGroup, .vehicleExpGroup').attr('disabled', true);
            $('.generalIncGroup').css('display', 'block');
            $('.generalIncGroup').attr('disabled', false);
        } else if (data.category.group == 'Vivienda' && data.category.type == 'Gasto') {
            $('.vehicleExpGroup, .generalIncGroup').css('display', 'none');
            $('.vehicleExpGroup, .generalIncGroup').attr('disabled', true);
            $('.homeExpGroup').css('display', 'block');
            $('.homeExpGroup').attr('disabled', false);
        } else if (data.category.group == 'Vehículo' && data.category.type == 'Gasto') {
            $('.homeExpGroup, .generalIncGroup').css('display', 'none');
            $('.homeExpGroup, .generalIncGroup').attr('disabled', true);
            $('.vehicleExpGroup').css('display', 'block');
            $('.vehicleExpGroup').attr('disabled', false);
        } else {
            $('.homeExpGroup, .vehicleExpGroup, .generalIncGroup').css('display', 'none');
            $('#subgroup').attr('disabled', true);
        }
    });
}
