/* Add here all your JS customizations */
$(document).ready(function () {

    $('#dropdownDatatable').DataTable({});
    $('#dropdownDatatable').css('opacity', 'unset');
    $('.lds-ellipsis').css('display', 'none');
});

function getEditData(tab, id) {
    $('#formAction').text('Editar');
    $.get("/getEditData/dropdowns/" + tab + '/' + id, function (data) {
        $('#modal-form').attr('action', '/accounting/dropdowns/' + tab + '/edit/' + id);
        switch (tab) {
            case 'category':
                $('#type').val(data.variable.type).multiselect("refresh");
                $('#category').val(data.variable.category);
                $('#categoryPriority').val(data.variable.categoryPriority);
                $('#group').val(data.variable.group).multiselect("refresh");
                $('#type, #group').multiselect("disable");
                break;
            case 'concept':
                $('#category').val(data.variable.category).multiselect("refresh");
                $('#concept').val(data.variable.concept);
                $('#conceptPriority').val(data.variable.conceptPriority);
                $('#subgroup').val(data.variable.subgroup).multiselect("refresh");
                $.get("/getCategoryData/" + data.variable.category, function (data) {
                    $('#typeInfo').val(data.category.type);
                    $('#groupInfo').val(data.category.group);
                    $('#subgroup').multiselect("disable");
                    if (data.category.group == 'General' && data.category.type == 'Ingreso') {
                        $('.homeExpGroup, .vehicleExpGroup').css('display', 'none');
                        $('.homeExpGroup, .vehicleExpGroup').attr('disabled', true);
                        $('.generalIncGroup').css('display', 'block');
                        $('.generalIncGroup').attr('disabled', false);
                    } else if (data.category.group == 'Vivienda' && data.category.type == 'Gasto') {
                        $('.vehicleExpGroup, .generalIncGroup').css('display', 'none');
                        $('.vehicleExpGroup, .generalIncGroup').attr('disabled', true);
                        $('.homeExpGroup').css('display', 'block');
                        $('.homeExpGroup').attr('disabled', false);
                    } else if (data.category.group == 'Vehículo' && data.category.type == 'Gasto') {
                        $('.homeExpGroup, .generalIncGroup').css('display', 'none');
                        $('.homeExpGroup, .generalIncGroup').attr('disabled', true);
                        $('.vehicleExpGroup').css('display', 'block');
                        $('.vehicleExpGroup').attr('disabled', false);
                    } else {
                        $('.homeExpGroup, .vehicleExpGroup, .generalIncGroup').css('display', 'none');
                        $('#subgroup').multiselect("disable");
                    }
                    $('#category, #subgroup').multiselect("disable");
                });
                break;
            case 'home':
                $('#address').val(data.variable.address);
                $('#homePriority').val(data.variable.homePriority);
                $('#inOwnership').val(data.variable.inOwnership.toString()).multiselect("refresh");
                break;
            case 'vehicle':
                $('#description').val(data.variable.description);
                $('#vehiclePriority').val(data.variable.vehiclePriority);
                $('#inOwnership').val(data.variable.inOwnership.toString()).multiselect("refresh");
                $('#vehicleType').val(data.variable.vehicleType).multiselect("refresh");
                $('#vehicleType').multiselect("disable");
                break;
            case 'entity':
                $('#entity').val(data.variable.entity);
                $('#entityPriority').val(data.variable.entityPriority);
                $('#quotation').val(data.variable.quotation);
                break;
            default:
                alert('La pestaña no tiene asignada ninguna accion')
                break;
        }
        $('#modal, #modal-form').addClass('show');
        $('#modal-backdrop').addClass('modal-backdrop show');
    });

}

function getAddData(tab) {
    switch (tab) {
        case 'category':
            $('#type, #group').multiselect("enable");
            break;
        case 'concept':
            $('#category, #subgroup').multiselect("enable");
            $('.homeExpGroup').css('display', 'none');
            $('.vehicleExpGroup').css('display', 'none');
            break;
        case 'home':
            break;
        case 'vehicle':
            $('#vehicleType').multiselect("enable");
            break;
        case 'entity':
            break;
        default:
            alert('La pestaña no tiene asignada ninguna accion')
            break;
    }
    $('#formAction').text('Añadir');
    $('#modal-form').attr('action', '/accounting/dropdowns/' + tab + '/add');
    $('#modal-form')[0].reset();
    $('#modal, #modal-form').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

function deleteData(tab, id) {
    Swal.fire({
        title: '¿Seguro de que quieres borrar el elemento?',
        type: 'warning',
        confirmButtonColor: '#d33',
        showCancelButton: true,
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Sí, borrar',
        cancelButtonText: 'No, cancelar'
    }).then((result) => {
        if (result.value) {
            window.location.href = "/accounting/dropdowns/" + tab + "/delete/" + id;
        }
    });
}

function getCategoryData(category) {
    $.get("/getCategoryData/" + category, function (data) {
        $('#typeInfo').val(data.category.type);
        $('#groupInfo').val(data.category.group);

        $('#subgroup').attr('disabled', false);
        if (data.category.group == 'General' && data.category.type == 'Ingreso') {
            $('.homeExpGroup, .vehicleExpGroup').css('display', 'none');
            $('.homeExpGroup, .vehicleExpGroup').attr('disabled', true);
            $('.generalIncGroup').css('display', 'block');
            $('.generalIncGroup').attr('disabled', false);
        } else if (data.category.group == 'Vivienda' && data.category.type == 'Gasto') {
            $('.vehicleExpGroup, .generalIncGroup').css('display', 'none');
            $('.vehicleExpGroup, .generalIncGroup').attr('disabled', true);
            $('.homeExpGroup').css('display', 'block');
            $('.homeExpGroup').attr('disabled', false);
        } else if (data.category.group == 'Vehículo' && data.category.type == 'Gasto') {
            $('.homeExpGroup, .generalIncGroup').css('display', 'none');
            $('.homeExpGroup, .generalIncGroup').attr('disabled', true);
            $('.vehicleExpGroup').css('display', 'block');
            $('.vehicleExpGroup').attr('disabled', false);
        } else {
            $('.homeExpGroup, .vehicleExpGroup, .generalIncGroup').css('display', 'none');
            $('#subgroup').attr('disabled', true);
        }
    });
}
