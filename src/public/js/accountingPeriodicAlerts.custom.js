/* Add here all your JS customizations */
$(document).ready(function () {

    $('#periodicAlertsDatatable').DataTable({
        columnDefs: [
            { render: $.fn.dataTable.moment( 'DD/MM/YYYY' ), targets: [6,7] },
            { type: 'formated-sorting', targets: [5] },
        ],
        order: [[1, "asc"]],
    });
    $('#periodicAlertsDatatable').css('opacity', 'unset');
    $('.lds-ellipsis').css('display', 'none');
    $("#startDate, #endDate").datepicker({
        "format": "dd/mm/yyyy",
        "todayHighlight": true,
        "weekStart": 1,
        "autoclose": true,
        //"calendarWeeks": true, 
        "clearBtn": true,
        "language": "es",
        "daysOfWeekHighlighted": "0,6",
    });
    $('#startDate').change(function (e) {
        $('#endDate').datepicker('setStartDate', e.target.value);
    });
    $('#endDate').change(function (e) {
        $('#startDate').datepicker('setEndDate', e.target.value);
    });
});

async function getCopyData(id) {
    $('#formAction').text('Copiar');
    const data = await $.get("/getEditData/periodicAlerts/" + id, function (data) { });

    $('#modal-form').attr('action', '/accounting/periodicAlerts/add');
    $('#modal-form')[0].reset();
    $('#concept, #status').multiselect("refresh");

    Object.entries(data.variable).forEach(element => {
        if (element[0].endsWith('Date')) {
            $('#' + element[0]).val(new Date(element[1]).toLocaleString('en-GB', { year: "numeric", month: "2-digit", day: "2-digit" }))
        } else {
            $('#' + element[0]).val(element[1]);
        }
    });

    $('#concept, #status').multiselect("refresh")
    $('#modal, #modal-form').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

async function getEditData(id) {
    $('#formAction').text('Editar');
    const data = await $.get("/getEditData/periodicAlerts/" + id, function (data) { });

    $('#modal-form').attr('action', '/accounting/periodicAlerts/edit/' + id);
    $('#modal-form')[0].reset();
    $('#concept').multiselect("refresh");

    Object.entries(data.variable).forEach(element => {
        if (element[0].endsWith('Date')) {
            $('#' + element[0]).val(new Date(element[1]).toLocaleString('en-GB', { year: "numeric", month: "2-digit", day: "2-digit" }))
        } else {
            $('#' + element[0]).val(element[1]);
        }
    });

    $('#concept, #status').multiselect("refresh");
    $('#modal, #modal-form').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

function getAddData() {
    $('#formAction').text('Añadir');
    $('#modal-form').attr('action', '/accounting/periodicAlerts/add');
    $('#modal-form')[0].reset();
    $('#concept, #status').multiselect("refresh");
    $('#modal, #modal-form').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

function deleteData(id) {
    Swal.fire({
        title: '¿Seguro de que quieres borrar el elemento?',
        type: 'warning',
        confirmButtonColor: '#d33',
        showCancelButton: true,
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Sí, borrar',
        cancelButtonText: 'No, cancelar'
    }).then((result) => {
        if (result.value) {
            window.location.href = "/accounting/periodicAlerts/delete/" + id ;
        }
    });
}

async function doCategoryChange(category) {
    await $.get("/getCategoryData/" + category, function (data) {
        $('#concept').multiselect("enable");
        $('#concept').val('').multiselect("refresh");
        $('.generalWageClass, .homeWaterClass, .homeLightClass, .homeGasClass, .vehicleEnergyClass, .vehicleMaintenance').css('display', 'none');
        $('.generalWageClass, .homeWaterClass, .homeLightClass, .homeGasClass, .vehicleEnergyClass, .vehicleMaintenance').find('input, textarea, button, select').attr('disabled', true);
        const categoryClass = data.category.category.replace(" ", "-");
        $("[class$=ClassConcept]").css('display', 'none');
        $("." + categoryClass + "ClassConcept").css('display', 'block');
        $("." + categoryClass + "ClassConcept").attr('disabled', false);
        if (data.category.group == 'Vivienda') {
            $('.homeClass').css('display', 'block');
            $('.homeClass').find('input, textarea, button, select').attr('disabled', false);
            $('.vehicleClass').css('display', 'none');
            $('.vehicleClass').find('input, textarea, button, select').attr('disabled', true);
        } else if (data.category.group == 'Vehículo') {
            $('.homeClass').css('display', 'none');
            $('.homeClass').find('input, textarea, button, select').attr('disabled', true);
            $('.vehicleClass').css('display', 'block');
            $('.vehicleClass').find('input, textarea, button, select').attr('disabled', false);
        } else {
            $('.homeClass').css('display', 'none');
            $('.homeClass').find('input, textarea, button, select').attr('disabled', true);
            $('.vehicleClass').css('display', 'none');
            $('.vehicleClass').find('input, textarea, button, select').attr('disabled', true);
        }
    });
}
async function doConceptChange(concept) {
    $('.generalWageClass, .homeWaterClass, .homeLightClass, .homeGasClass, .vehicleEnergyClass, .vehicleMaintenance').css('display', 'none');
    $('.generalWageClass, .homeWaterClass, .homeLightClass, .homeGasClass, .vehicleEnergyClass, .vehicleMaintenance').find('input, textarea, button, select').attr('disabled', true);
    if (concept) {
        await $.get("/getConceptData/" + concept, function (data) {
            if (data.concept) {
                if (data.concept.subgroup == 'Nómina') {
                    $('.generalWageClass').css('display', 'block');
                    $('.generalWageClass').find('input, textarea, button, select').attr('disabled', false);
                } else if (data.concept.subgroup == 'Agua Vivienda') {
                    $('.homeWaterClass').css('display', 'block');
                    $('.homeWaterClass').find('input, textarea, button, select').attr('disabled', false);
                } else if (data.concept.subgroup == 'Luz Vivienda') {
                    $('.homeLightClass').css('display', 'block');
                    $('.homeLightClass').find('input, textarea, button, select').attr('disabled', false);
                } else if (data.concept.subgroup == 'Gas Vivienda') {
                    $('.homeGasClass').css('display', 'block');
                    $('.homeGasClass').find('input, textarea, button, select').attr('disabled', false);
                } else if (data.concept.subgroup == 'Fuente energía Vehículo') {
                    $('.vehicleEnergyClass').css('display', 'block');
                    $('.vehicleEnergyClass').find('input, textarea, button, select').attr('disabled', false);
                } else if (data.concept.subgroup == 'Mantenimiento Vehículo') {
                    $('.vehicleMaintenance').css('display', 'block');
                    $('.vehicleMaintenance').find('input, textarea, button, select').attr('disabled', false);
                }
            }
        });
    }
}

async function doVehicleChange(vehicle) {
    $('.vehicleEnergyClass, .vehicleMaintenance').css('display', 'none');
    $('.vehicleEnergyClass, .vehicleMaintenance').find('input, textarea, button, select').attr('disabled', true);
    $('.vehicleGasClass, .vehicleElectricClass').removeClass('vehicleEnergyClass');
    await $.get("/getVehicleData/" + vehicle, function (data) {
        if (data.vehicle.vehicleType == 'Eléctrico') {
            $('.vehicleElectricClass').addClass('vehicleEnergyClass');
            $('.vehicleElectricClass').find('input, textarea, button, select').attr('disabled', false);
            $('.vehicleGasClass').find('input, textarea, button, select').attr('disabled', true);
        } else if (data.vehicle.vehicleType == 'Gasolina' || data.vehicle.vehicleType == 'Diesel' || data.vehicle.vehicleType == 'Híbrido') {
            $('.vehicleGasClass').addClass('vehicleEnergyClass');
            $('.vehicleElectricClass').find('input, textarea, button, select').attr('disabled', true);
            $('.vehicleGasClass').find('input, textarea, button, select').attr('disabled', false);
        } else if (data.vehicle.vehicleType == 'Híbrido enchufable') {
            $('.vehicleGasClass, .vehicleElectricClass').addClass('vehicleEnergyClass');
            $('.vehicleGasClass, .vehicleElectricClass').find('input, textarea, button, select').attr('disabled', false);
        }
    });
    doConceptChange($('#concept').val())
}

function closeModal() {
    $('#modal-backdrop').removeClass();
    $('.modal, #modal-form').removeClass('show');
}
function closeModalFilter() {
    $('#modal-backdrop').removeClass();
    $('.modal, #modal-form-filter').removeClass('show');
}

function getFilter(tab) {
    switch (tab) {


        default:

            break;
    }

    //$('#modal-form-filter').attr('action', 'accounting/cashflow/filter/' + tab);
    //$('#modal-form-filter')[0].reset();
    $('#modal, #modal-form-filter').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}