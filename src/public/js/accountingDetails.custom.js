/* Add here all your JS customizations */
$(document).ready(function () {
    $('#detailsDatatable').DataTable({

        responsive: true,
        columnDefs: [
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: 1 },
        ],
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        //order: [[ 0, "desc" ]],
        "lengthChange": false,
        "ordering": false,
        "searching": false,
    });
    $('#detailsDatatable').css('opacity', 'unset');
    $('.lds-ellipsis').css('display', 'none');
    $("#startDateFilter, #endDateFilter").datepicker({
        "format": "dd/mm/yyyy",
        "todayHighlight": true,
        "weekStart": 1,
        "autoclose": true,
        //"calendarWeeks": true, 
        "clearBtn": true,
        "language": "es",
        "daysOfWeekHighlighted": [0, 6],
    });

    $("#startDateFilter, #endDateFilter").datepicker({
        "format": "dd/mm/yyyy",
        "todayHighlight": true,
        "weekStart": 1,
        "autoclose": true,
        //"calendarWeeks": true, 
        "clearBtn": true,
        "language": "es",
        "daysOfWeekHighlighted": "0,6",
    });
    $('#startDateFilter').change(function (e) {
        $('#endDateFilter').datepicker('setStartDate', e.target.value);
        if (e.target.value || $('#endDateFilter').val()) {
            $('.specificDates').css('display', 'none');
        } else {
            $('.specificDates').css('display', 'block');
        }
    });
    $('#endDateFilter').change(function (e) {
        $('#startDateFilter').datepicker('setEndDate', e.target.value);
        if (e.target.value || $('#startDateFilter').val()) {
            $('.specificDates').css('display', 'none');
        } else {
            $('.specificDates').css('display', 'block');
        }
    });
    if (tab == 'water' || tab == 'light' || tab == 'stock') {
        colors = dynamicColors(barChartData.Dataset.length);
        colors.unshift(['rgba(0,125,255,1)', 'rgba(0,125,255,0.8)'], ['rgba(255,0,0,1)', 'rgba(255,0,0,0.8)']);
        if (tab == 'water' || tab == 'light') {
            var datasets = [{
                label: "Importe",
                borderColor: 'rgba(255,164,32,1)',
                backgroundColor: "rgba(255, 164, 32, 0.8)",
                fill: false,
                lineTension: 0,
                data: barChartData.Amount,
                type: 'line',
                yAxisID: '€',
            }]
            var i = 0;
            barChartData.Dataset.forEach(element => {
                datasets.push({
                    label: element,
                    data: barChartData[element],
                    borderColor: colors[i][0],
                    backgroundColor: colors[i][1],
                    yAxisID: 'ud',
                })
                i++;
            });
            var yAxes = [{
                type: 'linear',
                id: '€',
                ticks: {
                    beginAtZero: true,
                    fontSize: (window.innerWidth < 500) ? 10 : 12,
                    userCallback: function (tick) {
                        return tick.toLocaleString("es", { style: "decimal", maximumFractionDigits: 1 }) + " €";
                    },
                },
                //offset: true,
                position: 'left',
            }, {
                type: 'linear',
                id: 'ud',
                ticks: {
                    beginAtZero: true,
                    fontSize: (window.innerWidth < 500) ? 10 : 12,
                    precision: 0,
                    userCallback: function (tick) {
                        var tickFormated = tick.toLocaleString("es", { style: "decimal", maximumFractionDigits: 1 });
                        if (tab == "water") {
                            tickFormated = tickFormated + " m³"
                        } else if (tab == "light") {
                            tickFormated = tickFormated + " kWh"
                        };
                        return tickFormated
                    },
                },
                position: 'right',
            }]
        } else if (tab == 'stock') {
            var datasets = [{
                label: "Balance Real",
                borderColor: 'rgba(255,164,32,1)',
                backgroundColor: "rgba(255, 164, 32, 0.8)",
                fill: false,
                lineTension: 0,
                data: barChartData.BalanceR,
                type: 'line',
                //yAxisID: '€',
            },
            {
                label: "Balance Teórico",
                borderColor: 'rgba(0, 170, 0,1)',
                backgroundColor: "rgba(0, 170, 0, 0.8)",
                fill: false,
                lineTension: 0,
                data: barChartData.BalanceT,
                type: 'line',
                //yAxisID: '€',
            }]
            var i = 0;
            barChartData.Dataset.forEach(element => {
                datasets.push({
                    label: element,
                    data: barChartData[element],
                    borderColor: colors[i][0],
                    backgroundColor: colors[i][1],
                    type: 'line',
                    fill: false,
                    showLine: false,
                    // yAxisID: 'ud',
                })
                i++;
            });
            var yAxes = [{
                type: 'linear',
                id: '€',
                ticks: {
                    beginAtZero: true,
                    fontSize: (window.innerWidth < 500) ? 10 : 12,
                    userCallback: function (tick) {
                        return tick.toLocaleString("es", { style: "decimal", maximumFractionDigits: 1 }) + " €";
                    },
                },
                //offset: true,
                position: 'left',
            }]
        }

        var myBarChart = new Chart($("#" + tab + "Chart"), {
            responsive: true,
            type: 'bar',
            data: {
                labels: barChartData.Label,
                datasets: datasets
            },
            options: {
                aspectRatio: 3,
                scales: {
                    // We use this empty structure as a placeholder for dynamic theming.
                    xAxes: [{
                        display: (window.innerWidth < 500) ? false : true,
                        type: 'time',
                        time: {
                            displayFormats: {
                                millisecond: 'DD MMM YYYY',
                                second: 'DD MMM YYYY',
                                minute: 'DD MMM YYYY',
                                hour: 'DD MMM YYYY',
                                day: 'MMM YYYY',
                                week: 'MMM YYYY',
                                month: 'MMM YYYY',
                                quarter: 'MMM YYYY',
                                year: 'MMM YYYY',
                            },
                        },
                        offset: true,
                        ticks: {
                            callback: function (label, index, labels) {
                                return translate_this_label(label);
                            }
                        }
                    }],
                    yAxes: yAxes,
                },
                legend: {
                    display: (window.innerWidth < 500) ? false : true,
                    labels: {
                        fontSize: (window.innerWidth < 500) ? 6 : 12,
                    }
                },
                onResize: function (chart, size) {
                    if (size.width < 400) {
                        chart.legend.options.display = false;
                        chart.options.scales.xAxes[0].display = false;
                        //chart.options.scales.yAxes[0].ticks.fontSize = 10; Da problemas
                        chart.legend.options.labels.fontSize = 6;
                        //chart.options.scales.yAxes[0].display = false;
                    } else {
                        chart.legend.options.display = true;
                        chart.options.scales.xAxes[0].display = true;
                        //chart.options.scales.yAxes[0].ticks.fontSize = 10; Da problemas
                        chart.legend.options.labels.fontSize = 12;
                        //chart.options.scales.yAxes[0].display = true;
                    }
                },
                tooltips: {
                    callbacks: {
                        title: function (tooltipItem, data) {
                            return new Date(tooltipItem[0].label).toLocaleString('en-GB', { year: "numeric", month: "2-digit", day: "2-digit" });
                        },
                        /*beforeLabel: function(tooltipItem, data) {
                          if(tooltipItem.datasetIndex != 0){
                              return data.datasets[0].label +': ' +euros(data.datasets[0].data[tooltipItem.index]);
                          }                   
              
                        },*/
                        label: function (tooltipItem, data) {
                            amount = euros(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
                            observation = barChartData.Observation[tooltipItem.index];
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + amount + " -> " + observation;

                        },
                        footer: function (tooltipItem, data) {
                            //console.log(tooltipItem,data)
                            return ".............................................";
                        }
                    },
                },
            },
        });
    }

});



function closeModal() {
    $('#modal-backdrop').removeClass();
    $('.modal, #modal-form').removeClass('show');
}
function closeModalFilter() {
    $('#modal-backdrop').removeClass();
    $('.modal, #modal-form-filter').removeClass('show');
}
function getFilter(tab) {
    switch (tab) {
        case 'Your tab':
            break;
        default:

            break;
    }

    //$('#modal-form-filter').attr('action', 'accounting/cashflow/filter/' + tab);
    //$('#modal-form-filter')[0].reset();
    $('#modal, #modal-form-filter').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}
$('#vehicleSelection').change(function (e) {
    window.location.href = window.location.origin + '/accounting/details/vehicle?' + 'vehicle=' + $('#vehicleSelection').val()
});

$('#homeSelection').change(function (e) {
    window.location.href = window.location.origin + '/accounting/details/' + tab + '?' + 'home=' + $('#homeSelection').val()
});
$('#entitySelection').change(function (e) {
    if ($('#entitySelection').val() == 'Todas') {
        window.location.href = window.location.origin + '/accounting/details/' + tab;
    } else {
        window.location.href = window.location.origin + '/accounting/details/' + tab + '?' + 'entity=' + $('#entitySelection').val();
    }

});


function dynamicColors(arrayLenght) {
    var min = 70;
    var max = 220;
    arrayColors = [];
    var r = min;
    var g = max;
    var b = min;

    if (arrayLenght <= 6) {
        var escalon = (max - min);
    } else {
        var escalon = Math.ceil((max - min) / (((arrayLenght / 3) - 1) / 2));
    }
    for (let i = 0; i < arrayLenght; i++) {
        if (!(r <= min) && (g >= max) && (b <= min)) {
            r -= escalon;
        } else if ((r <= min) && (g >= max) && !(b >= max)) {
            b += escalon;
        } else if ((r <= min) && !(g <= min) && (b >= max)) {
            g -= escalon;
        } else if (!(r >= max) && (g <= min) && (b >= max)) {
            r += escalon;
        } else if ((r >= max) && (g <= min) && !(b <= min)) {
            b -= escalon;
        } else if ((r >= max) && !(g >= max) && (b <= min)) {
            g += escalon;
        }
        arrayColors.push(["rgb(" + r + "," + g + "," + b + ", 1)", "rgb(" + r + "," + g + "," + b + ", 0.8)"]);
    }
    return arrayColors;
};

function translate_month(month) {

    var result = month;
  
    switch (month) {
  
      case 'Jan':
        result = 'Ene';
        break;
      case 'Apr':
        result = 'Abr';
        break;
      case 'Aug':
        result = 'Ago';
        break;
    }
  
    return result;
  }
  
  
  function translate_this_label(label) {
    month = label.match(/Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Nov|Dec/g);
  
    if (!month)
      return label;
  
    translation = translate_month(month[0]);
    return label.replace(month, translation, 'g');
  }