/* Add here all your JS customizations */
$(document).ready(function () {

    $('#stockDatatable').DataTable({
        columnDefs: [
            { render: $.fn.dataTable.moment( 'DD/MM/YYYY' ), targets: 0 },
            { type: 'formated-sorting', targets: [3] },
        ],
        order: [[1, "desc"]],
    });
    $('#stockDatatable').css('opacity', 'unset');
    $('.lds-ellipsis').css('display', 'none');
    $("#date, #startDateFilter, #endDateFilter").datepicker({
        "format": "dd/mm/yyyy",
        "todayHighlight": true,
        "weekStart": 1,
        "autoclose": true,
        //"calendarWeeks": true, 
        "clearBtn": true,
        "language": "es",
        "daysOfWeekHighlighted": "0,6",
    });
    $('#startDateFilter').change(function (e) {
        $('#endDateFilter').datepicker('setStartDate', e.target.value);
        if (e.target.value || $('#endDateFilter').val()) {
            $('.specificDates').css('display', 'none');
        } else {
            $('.specificDates').css('display', 'block');
        }
    });
    $('#endDateFilter').change(function (e) {
        $('#startDateFilter').datepicker('setEndDate', e.target.value);
        if (e.target.value || $('#startDateFilter').val()) {
            $('.specificDates').css('display', 'none');
        } else {
            $('.specificDates').css('display', 'block');
        }
    });
    $('#monthFilter, #yearFilter').change(function (e) {
        if ($('#monthFilter').val() || $('#yearFilter').val()) {
            $('.betweenDates').css('display', 'none');
        } else {
            $('.betweenDates').css('display', 'block');
        }
    });
    if ($('#monthFilter').val() || $('#yearFilter').val()) {
        $('.betweenDates').css('display', 'none');
    } else {
        $('.betweenDates').css('display', 'block');
    }
    if ($('#startDateFilter').val() || $('#endDateFilter').val()) {
        $('.specificDates').css('display', 'none');
    } else {
        $('.specificDates').css('display', 'block');
    }
});

async function getCopyData(tab, id) {
    $('#formAction').text('Copiar');
    const data = await $.get("/getEditData/cashflow/" + id, function (data) { });

    $('#modal-form').attr('action', '/accounting/stock/add/' + tab);
    $('#modal-form')[0].reset();
    $('#subgroup, #entity').multiselect("refresh");
    switch (tab) {
        case 'Write tab name':
            break;

        default:
            Object.entries(data.variable).forEach(element => {
                if (element[0] == 'date') {
                    $('#' + element[0]).val(new Date(element[1]).toLocaleString('en-GB', { year: "numeric", month: "2-digit", day: "2-digit" }))
                } else if (element[0] == 'amount') {
                    $('#' + element[0]).val(decimalVal(element[1], 3));
                } else {
                    $('#' + element[0]).val(element[1]);
                }
            });
            break;
    }
    $('#subgroup, #entity').multiselect("refresh")
    $('#modal, #modal-form').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

async function getEditData(tab, id) {
    $('#formAction').text('Editar');
    const data = await $.get("/getEditData/cashflow/" + id, function (data) { });

    $('#modal-form').attr('action', '/accounting/stock/edit/' + id + "/" + tab);
    $('#modal-form')[0].reset();
    $('#subgroup, #entity').multiselect("refresh");
    switch (tab) {
        case 'Write tab name':
            break;

        default:
            Object.entries(data.variable).forEach(element => {
                if (element[0] == 'date') {
                    $('#' + element[0]).val(new Date(element[1]).toLocaleString('en-GB', { year: "numeric", month: "2-digit", day: "2-digit" }))
                } else if (element[0] == 'amount' || element[0] == 'irpf' || element[0] == 'grossAmount' || element[0] == 'vehicleGasConsumtion') {
                    $('#' + element[0]).val(decimalVal(element[1], 2));
                } else {
                    $('#' + element[0]).val(element[1]);
                }
            });
            break;
    }
    $('#subgroup, #entity').multiselect("refresh");
    $('#modal, #modal-form').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

function getAddData(tab) {
    switch (tab) {
        case 'Write tab name':
            break;
        default:
            $('#concept').multiselect("disable");
            $('.homeClass, .vehicleClass, .generalWageClass').css('display', 'none');
            $('.homeClass, .vehicleClass, .generalWageClass').find('input, textarea, button').attr('disabled', true);
            $('.homeClass, .vehicleClass, .generalWageClass').find('select').multiselect("disable");
            break;
    }
    $('#formAction').text('Añadir');
    $('#modal-form').attr('action', '/accounting/stock/add/' + tab);
    $('#modal-form')[0].reset();
    $('#subgroup, #entity').multiselect("refresh");
    $('#modal, #modal-form').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

function deleteData(tab, id) {
    Swal.fire({
        title: '¿Seguro de que quieres borrar el elemento?',
        type: 'warning',
        confirmButtonColor: '#d33',
        showCancelButton: true,
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Sí, borrar',
        cancelButtonText: 'No, cancelar'
    }).then((result) => {
        if (result.value) {
            window.location.href = "/accounting/stock/delete/" + id + "/" + tab;
        }
    });
}


function closeModal() {
    $('#modal-backdrop').removeClass();
    $('.modal, #modal-form').removeClass('show');
}
function closeModalFilter() {
    $('#modal-backdrop').removeClass();
    $('.modal, #modal-form-filter').removeClass('show');
}

function getFilter(tab) {
    switch (tab) {


        default:

            break;
    }

    //$('#modal-form-filter').attr('action', 'accounting/stock/filter/' + tab);
    //$('#modal-form-filter')[0].reset();
    $('#modal, #modal-form-filter').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}