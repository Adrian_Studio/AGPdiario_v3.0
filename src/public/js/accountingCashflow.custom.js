/* Add here all your JS customizations */
$(document).ready(function () {

    $('#cashflowDatatable').DataTable({
        columnDefs: [
            { render: $.fn.dataTable.moment( 'DD/MM/YYYY' ), targets: 0 },
            { type: 'formated-sorting', targets: [3] },
        ],
        //order: [[2, "desc"]],
    });
    $('#cashflowDatatable').css('opacity', 'unset');
    $('.lds-ellipsis').css('display', 'none');
    $("#date, #startDateFilter, #endDateFilter").datepicker({
        "format": "dd/mm/yyyy",
        "todayHighlight": true,
        "weekStart": 1,
        "autoclose": true,
        //"calendarWeeks": true, 
        "clearBtn": true,
        "language": "es",
        "daysOfWeekHighlighted": "0,6",
    });
    $('#startDateFilter').change(function (e) {
        $('#endDateFilter').datepicker('setStartDate', e.target.value);
        if (e.target.value || $('#endDateFilter').val()) {
            $('.specificDates').css('display', 'none');
        } else {
            $('.specificDates').css('display', 'block');
        }
    });
    $('#endDateFilter').change(function (e) {
        $('#startDateFilter').datepicker('setEndDate', e.target.value);
        if (e.target.value || $('#startDateFilter').val()) {
            $('.specificDates').css('display', 'none');
        } else {
            $('.specificDates').css('display', 'block');
        }
    });
    $('#monthFilter, #yearFilter').change(function (e) {
        if ($('#monthFilter').val() || $('#yearFilter').val()) {
            $('.betweenDates').css('display', 'none');
        } else {
            $('.betweenDates').css('display', 'block');
        }
    });
    if ($('#monthFilter').val() || $('#yearFilter').val()) {
        $('.betweenDates').css('display', 'none');
    } else {
        $('.betweenDates').css('display', 'block');
    }
    if ($('#startDateFilter').val() || $('#endDateFilter').val()) {
        $('.specificDates').css('display', 'none');
    } else {
        $('.specificDates').css('display', 'block');
    }
});

async function getCopyData(tab, id) {
    $('#formAction').text('Copiar');
    const data = await $.get("/getEditData/cashflow/" + id, function (data) { });

    $('#modal-form').attr('action', '/accounting/cashflow/add/' + tab);
    $('#modal-form')[0].reset();
    $('#category, #concept, #vehicle, #home').multiselect("refresh");
    switch (tab) {
        case 'Write tab name':
            break;

        default:
            if (data.variable.vehicle) { await doVehicleChange(data.variable.vehicle); };
            await doCategoryChange(data.variable.category);
            await doConceptChange(data.variable.concept);
            Object.entries(data.variable).forEach(element => {
                if (element[0] == 'date') {
                    $('#' + element[0]).val(new Date(element[1]).toLocaleString('en-GB', { year: "numeric", month: "2-digit", day: "2-digit" }))
                } else if (element[0] == 'amount' || element[0] == 'irpf' || element[0] == 'grossAmount' || element[0] == 'vehicleGasConsumtion') {
                    $('#' + element[0]).val(decimalVal(element[1], 2));
                } else {
                    $('#' + element[0]).val(element[1]);
                }
            });
            break;
    }
    $('#category, #concept, #vehicle, #home').multiselect("refresh")
    $('#modal, #modal-form').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

async function getEditData(tab, id) {
    $('#formAction').text('Editar');
    const data = await $.get("/getEditData/cashflow/" + id, function (data) { });

    $('#modal-form').attr('action', '/accounting/cashflow/edit/' + id + "/" + tab);
    $('#modal-form')[0].reset();
    $('#category, #concept, #vehicle, #home').multiselect("refresh");
    switch (tab) {
        case 'Write tab name':
            break;

        default:
            if (data.variable.vehicle) { await doVehicleChange(data.variable.vehicle); };
            await doCategoryChange(data.variable.category);
            await doConceptChange(data.variable.concept);
            Object.entries(data.variable).forEach(element => {
                if (element[0] == 'date') {
                    $('#' + element[0]).val(new Date(element[1]).toLocaleString('en-GB', { year: "numeric", month: "2-digit", day: "2-digit" }))
                } else if (element[0] == 'amount' || element[0] == 'irpf' || element[0] == 'grossAmount' || element[0] == 'vehicleGasConsumtion') {
                    $('#' + element[0]).val(decimalVal(element[1], 2));
                } else {
                    $('#' + element[0]).val(element[1]);
                }
            });
            break;
    }
    $('#category, #concept, #vehicle, #home').multiselect("refresh");
    $('#modal, #modal-form').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

function getAddData(tab) {
    switch (tab) {
        case 'Write tab name':
            break;
        default:
            $('#concept').multiselect("disable");
            $('.homeClass, .vehicleClass, .generalWageClass').css('display', 'none');
            $('.homeClass, .vehicleClass, .generalWageClass').find('input, textarea, button').attr('disabled', true);
            $('.homeClass, .vehicleClass, .generalWageClass').find('select').multiselect("disable");
            break;
    }
    $('#formAction').text('Añadir');
    $('#modal-form').attr('action', '/accounting/cashflow/add/' + tab);
    $('#modal-form')[0].reset();
    $('#category, #concept, #vehicle, #home').multiselect("refresh");
    $('#modal, #modal-form').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

function deleteData(tab, id) {
    Swal.fire({
        title: '¿Seguro de que quieres borrar el elemento?',
        type: 'warning',
        confirmButtonColor: '#d33',
        showCancelButton: true,
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Sí, borrar',
        cancelButtonText: 'No, cancelar'
    }).then((result) => {
        if (result.value) {
            window.location.href = "/accounting/cashflow/delete/" + id + "/" + tab;
        }
    });
}

async function doCategoryChange(category) {
    await $.get("/getCategoryData/" + category, function (data) {
        $('#concept').multiselect("enable");
        $('#concept').val('').multiselect("refresh");
        $('.generalWageClass, .homeWaterClass, .homeLightClass, .homeGasClass, .vehicleEnergyClass, .vehicleMaintenance').css('display', 'none');
        $('.generalWageClass, .homeWaterClass, .homeLightClass, .homeGasClass, .vehicleEnergyClass, .vehicleMaintenance').find('input, textarea, button').attr('disabled', true);
        $('.generalWageClass, .homeWaterClass, .homeLightClass, .homeGasClass, .vehicleEnergyClass, .vehicleMaintenance').find('select').multiselect("disable");
        const categoryClass = data.category.category.replace(" ", "-");
        $("[class$=ClassConcept]").css('display', 'none');
        $("." + categoryClass + "ClassConcept").css('display', 'block');
        $("." + categoryClass + "ClassConcept").attr('disabled', false);
        if (data.category.group == 'Vivienda') {
            $('.homeClass').css('display', 'block');
            $('.homeClass').find('input, textarea, button').attr('disabled', false);
            $('.homeClass').find('select').multiselect("enable");
            $('.vehicleClass').css('display', 'none');
            $('.vehicleClass').find('input, textarea, button').attr('disabled', true);
            $('.vehicleClass').find('select').multiselect("disable");
            
        } else if (data.category.group == 'Vehículo') {
            $('.homeClass').css('display', 'none');
            $('.homeClass').find('input, textarea, button').attr('disabled', true);
            $('.homeClass').find('select').multiselect("disable");
            $('.vehicleClass').css('display', 'block');
            $('.vehicleClass').find('input, textarea, button').attr('disabled', false);
            $('.vehicleClass').find('select').multiselect("enable");
        } else {
            $('.homeClass').css('display', 'none');
            $('.homeClass').find('input, textarea, button').attr('disabled', true);
            $('.homeClass').find('select').multiselect("disable");
            $('.vehicleClass').css('display', 'none');
            $('.vehicleClass').find('input, textarea, button').attr('disabled', true);
            $('.vehicleClass').find('select').multiselect("disable");
        }
    });
}
async function doConceptChange(concept) {
    $('.generalWageClass, .homeWaterClass, .homeLightClass, .homeGasClass, .vehicleEnergyClass, .vehicleMaintenance').css('display', 'none');
    $('.generalWageClass, .homeWaterClass, .homeLightClass, .homeGasClass, .vehicleEnergyClass, .vehicleMaintenance').find('input, textarea, button').attr('disabled', true);
    $('.generalWageClass, .homeWaterClass, .homeLightClass, .homeGasClass, .vehicleEnergyClass, .vehicleMaintenance').find('select').multiselect("disable");
    if (concept) {
        await $.get("/getConceptData/" + concept, function (data) {
            if (data.concept) {
                if (data.concept.subgroup == 'Nómina') {
                    $('.generalWageClass').css('display', 'block');
                    $('.generalWageClass').find('input, textarea, button').attr('disabled', false);
                    $('.generalWageClass').find('select').multiselect("enable");
                } else if (data.concept.subgroup == 'Agua Vivienda') {
                    $('.homeWaterClass').css('display', 'block');
                    $('.homeWaterClass').find('input, textarea, button').attr('disabled', false);
                    $('.homeWaterClass').find('select').multiselect("enable");
                } else if (data.concept.subgroup == 'Luz Vivienda') {
                    $('.homeLightClass').css('display', 'block');
                    $('.homeLightClass').find('input, textarea, button').attr('disabled', false);
                    $('.homeLightClass').find('select').multiselect("enable");
                } else if (data.concept.subgroup == 'Gas Vivienda') {
                    $('.homeGasClass').css('display', 'block');
                    $('.homeGasClass').find('input, textarea, button').attr('disabled', false);
                    $('.homeGasClass').find('select').multiselect("enable");
                } else if (data.concept.subgroup == 'Fuente energía Vehículo') {
                    $('.vehicleEnergyClass').css('display', 'block');
                    $('.vehicleEnergyClass').find('input, textarea, button').attr('disabled', false);
                    $('.vehicleEnergyClass').find('select').multiselect("enable");
                } else if (data.concept.subgroup == 'Mantenimiento Vehículo') {
                    $('.vehicleMaintenance').css('display', 'block');
                    $('.vehicleMaintenance').find('input, textarea, button').attr('disabled', false);
                    $('.vehicleMaintenance').find('select').multiselect("enable");
                }
            }
        });
    }
}

async function doVehicleChange(vehicle) {
    $('.vehicleEnergyClass, .vehicleMaintenance').css('display', 'none');
    $('.vehicleEnergyClass, .vehicleMaintenance').find('input, textarea, button, select').attr('disabled', true);
    $('.vehicleMaintenance').find('select').multiselect("disable");
    $('.vehicleGasClass, .vehicleElectricClass').removeClass('vehicleEnergyClass');
    await $.get("/getVehicleData/" + vehicle, function (data) {
        if (data.vehicle.vehicleType == 'Eléctrico') {
            $('.vehicleElectricClass').addClass('vehicleEnergyClass');
            $('.vehicleElectricClass').find('input, textarea, button').attr('disabled', false);
            $('.vehicleElectricClass').find('select').multiselect("enable");
            $('.vehicleGasClass').find('input, textarea, button').attr('disabled', true);
            $('.vehicleGasClass').find('select').multiselect("disable");
        } else if (data.vehicle.vehicleType == 'Gasolina' || data.vehicle.vehicleType == 'Diesel' || data.vehicle.vehicleType == 'Híbrido') {
            $('.vehicleGasClass').addClass('vehicleEnergyClass');
            $('.vehicleElectricClass').find('input, textarea, button').attr('disabled', true);
            $('.vehicleElectricClass').find('select').multiselect("disable");
            $('.vehicleGasClass').find('input, textarea, button').attr('disabled', false);
            $('.vehicleGasClass').find('select').multiselect("enable");
        } else if (data.vehicle.vehicleType == 'Híbrido enchufable') {
            $('.vehicleGasClass, .vehicleElectricClass').addClass('vehicleEnergyClass');
            $('.vehicleGasClass, .vehicleElectricClass').find('input, textarea, button').attr('disabled', false);
            $('.vehicleGasClass, .vehicleElectricClass').find('select').multiselect("enable");
        }
    });
    doConceptChange($('#concept').val())
}

function closeModal() {
    $('#modal-backdrop').removeClass();
    $('.modal, #modal-form').removeClass('show');
}
function closeModalFilter() {
    $('#modal-backdrop').removeClass();
    $('.modal, #modal-form-filter').removeClass('show');
}

function getFilter(tab) {
    switch (tab) {


        default:

            break;
    }

    //$('#modal-form-filter').attr('action', 'accounting/cashflow/filter/' + tab);
    //$('#modal-form-filter')[0].reset();
    $('#modal, #modal-form-filter').addClass('show');
    $('#modal-backdrop').addClass('modal-backdrop show');
}

async function updateBankCheck() {
    Swal.fire({
        title: 'Are you sure you want to Update?',
        text: "This can be undone!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Update!'
    }).then(async(result) => {
        if (result.value) {
            await $.get("/accounting/lastBankUptadeToday", function (data) {
                $('#lastBankUpdateDate').text(formatDate(data)) 
            });
        }
    })
}