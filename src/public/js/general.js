/* Add here all your JS customizations */
(function ($) {

    'use strict';

    if (window.performance && window.performance.navigation.type == window.performance.navigation.TYPE_BACK_FORWARD) {
        location.reload();
    }


    (function (seconds) {
        var refresh,
            intvrefresh = function () {
                clearInterval(refresh);
                refresh = setTimeout(function () {
                    location.href = location.href;
                }, 600000);
            };

        $(document).on('keypress click', function () { intvrefresh() });
        intvrefresh();

    }(15));

    $('#treeCheckbox').jstree({
        'core': {
            'data': [
                {
                    "id": "j1_1",
                    "text": "Notas",
                    "icon": "fa fa-file",
                    "li_attr": {
                        "id": "j1_1"
                    },
                    "a_attr": {
                        "href": "#"
                    },
                    "state": {
                        "loaded": true,
                        "opened": false,
                        "selected": sectionsArray.annotations,
                        "disabled": false
                    },
                    "data": null,
                    "parent": "#",
                    "type": "default"
                },
                {
                    "id": "j1_2",
                    "text": "Contabilidad",
                    "icon": "fa fa-folder",
                    "li_attr": {
                        "id": "j1_2"
                    },
                    "a_attr": {
                        "href": "#"
                    },
                    "state": {
                        "loaded": true,
                        "opened": true,
                        "selected": false,
                        "disabled": false
                    },
                    "data": null,
                    "parent": "#",
                    "type": "default"
                },
                {
                    "id": "j1_3",
                    "text": "Sección",
                    "icon": "fa fa-file",
                    "li_attr": {
                        "id": "j1_3"
                    },
                    "a_attr": {
                        "href": "#"
                    },
                    "state": {
                        "loaded": true,
                        "opened": false,
                        "selected": sectionsArray.accounting.section,
                        "disabled": false
                    },
                    "data": null,
                    "parent": "j1_2",
                    "type": "default"
                },
                {
                    "id": "j1_4",
                    "text": "Acciones",
                    "icon": "fa fa-file",
                    "li_attr": {
                        "id": "j1_4"
                    },
                    "a_attr": {
                        "href": "#"
                    },
                    "state": {
                        "loaded": true,
                        "opened": false,
                        "selected": sectionsArray.accounting.stock,
                        "disabled": false
                    },
                    "data": null,
                    "parent": "j1_2",
                    "type": "default"
                },
                {
                    "id": "j1_5",
                    "text": "Detalles",
                    "icon": "fa fa-folder",
                    "li_attr": {
                        "id": "j1_5"
                    },
                    "a_attr": {
                        "href": "#"
                    },
                    "state": {
                        "loaded": true,
                        "opened": true,
                        "selected": false,
                        "disabled": false
                    },
                    "data": null,
                    "parent": "j1_2",
                    "type": "default"
                },
                {
                    "id": "j1_6",
                    "text": "Vehículo",
                    "icon": "fa fa-file",
                    "li_attr": {
                        "id": "j1_6"
                    },
                    "a_attr": {
                        "href": "#"
                    },
                    "state": {
                        "loaded": true,
                        "opened": false,
                        "selected": sectionsArray.accounting.details.vehicle,
                        "disabled": false
                    },
                    "data": null,
                    "parent": "j1_5",
                    "type": "default"
                },
                {
                    "id": "j1_7",
                    "text": "Nómina",
                    "icon": "fa fa-file",
                    "li_attr": {
                        "id": "j1_7"
                    },
                    "a_attr": {
                        "href": "#"
                    },
                    "state": {
                        "loaded": true,
                        "opened": false,
                        "selected": sectionsArray.accounting.details.salary,
                        "disabled": false
                    },
                    "data": null,
                    "parent": "j1_5",
                    "type": "default"
                },
                {
                    "id": "j1_8",
                    "text": "Luz",
                    "icon": "fa fa-file",
                    "li_attr": {
                        "id": "j1_8"
                    },
                    "a_attr": {
                        "href": "#"
                    },
                    "state": {
                        "loaded": true,
                        "opened": false,
                        "selected": sectionsArray.accounting.details.light,
                        "disabled": false
                    },
                    "data": null,
                    "parent": "j1_5",
                    "type": "default"
                },
                {
                    "id": "j1_9",
                    "text": "Agua",
                    "icon": "fa fa-file",
                    "li_attr": {
                        "id": "j1_9"
                    },
                    "a_attr": {
                        "href": "#"
                    },
                    "state": {
                        "loaded": true,
                        "opened": false,
                        "selected": sectionsArray.accounting.details.water,
                        "disabled": false
                    },
                    "data": null,
                    "parent": "j1_5",
                    "type": "default"
                },
                {
                    "id": "j1_10",
                    "text": "Inventario",
                    "icon": "fa fa-file",
                    "li_attr": {
                        "id": "j1_10"
                    },
                    "a_attr": {
                        "href": "#"
                    },
                    "state": {
                        "loaded": true,
                        "opened": false,
                        "selected": sectionsArray.inventory,
                        "disabled": false
                    },
                    "data": null,
                    "parent": "#",
                    "type": "default"
                },
                {
                    "id": "j1_11",
                    "text": "Viajes",
                    "icon": "fa fa-file",
                    "li_attr": {
                        "id": "j1_11"
                    },
                    "a_attr": {
                        "href": "#"
                    },
                    "state": {
                        "loaded": true,
                        "opened": false,
                        "selected": sectionsArray.trips,
                        "disabled": false
                    },
                    "data": null,
                    "parent": "#",
                    "type": "default"
                }
            ],
            'themes': {
                'responsive': false
            }
        },
        'types': {
            'default': {
                'icon': 'fa fa-folder'
            },
            'file': {
                'icon': 'fa fa-file'
            }
        },
        'plugins': ['types', 'checkbox']
    });
}).apply(this, [jQuery]);

function formatDate(date) {
    return new Date(date).toLocaleString('en-GB', { year: "numeric", month: "2-digit", day: "2-digit" })//.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");
}
function euros(amount) {
    return (
        amount
            .toFixed(2) // always two decimal digits
            .replace('.', ',') // replace decimal point character with ,
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + ' €'
    )
}

function decimalVal(amount, decimals) {
    if (!amount) { return undefined };
    return (
        amount
            .toFixed(decimals) // always two decimal digits
        /*.replace('.', ',') // replace decimal point character with ,
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + ' €'*/
    )
}

function changeDefaultLeftSidebar() {
    $.post("/changeLeftSidebar");
}
function changeDefaultRightSidebar(part) {
    $.post("/changeRightSidebar/" + part);
}
function closeRightSidebar() {
    $.post("/closeRightSidebar");
    $('.sidebar-right-opened').removeClass('sidebar-right-opened');
}

function changePaswordVisibility(page) {
    switch (page) {
        case 'signIn':
            if ($('#signInShowPasword')[0].checked) {
                $('#signInPass')[0].type = 'text';
            } else {
                $('#signInPass')[0].type = 'password';
            }
            break;
        case 'signUp':
            if ($('#signUpShowPasword')[0].checked) {
                $('#signUpPass')[0].type = 'text';
                $('#signUpPass2')[0].type = 'text';
            } else {
                $('#signUpPass')[0].type = 'password';
                $('#signUpPass2')[0].type = 'password';
            }
            break;
        case 'changePwd':
            if ($('#signUpShowPasword')[0].checked) {
                $('#password')[0].type = 'text';
                $('#passwordNew')[0].type = 'text';
                $('#passwordNew2')[0].type = 'text';
            } else {
                $('#password')[0].type = 'password';
                $('#passwordNew')[0].type = 'password';
                $('#passwordNew2')[0].type = 'password';
            }
            break;
        case 'recoverPwd':
            if ($('#signUpShowPasword')[0].checked) {
                $('#passwordNew')[0].type = 'text';
                $('#passwordNew2')[0].type = 'text';
            } else {
                $('#passwordNew')[0].type = 'password';
                $('#passwordNew2')[0].type = 'password';
            }
            break;
        default:
            break;
    }
}
function logout() {
    Swal.fire({
        title: 'Are you sure you want to logout?',
        text: "You will have to login again!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, logout!'
    }).then((result) => {
        if (result.value) {
            location.href = '/users/logout'
        }
    })

}

function changePwd() {
    $.post("/changeRightSidebar/sidebar");
    location.href = '/users/changePwd'
}

function toggleEnabledSignUp() {
    if ($('#enableSignUp')[0].innerText == 'Habilitar registro') {
        Swal.fire({
            title: '¿Seguro que quieres habilitar la opción de registro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Habilitar!'
        }).then((result) => {
            if (result.value) {
                $('#enableSignUp')[0].innerText = 'Deshabilitar registro'
                $.post("/toggleEnabledSignUp");
            }
        })
    } else {
        Swal.fire({
            title: '¿Seguro que quieres deshabilitar la opción de registro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Deshabilitar!'
        }).then((result) => {
            if (result.value) {
                $('#enableSignUp')[0].innerText = 'Habilitar registro'
                $.post("/toggleEnabledSignUp");
            }
        })
    }
}

function saveSections() {
    var sections = $('#treeCheckbox').jstree(true).get_json('#', { flat: true })
    sections.forEach(section => {
        switch (section.text) {
            case "Notas":
                sectionsArray.annotations = section.state.selected;
                break;
            case "Sección":
                sectionsArray.accounting.section = section.state.selected;
                break;
            case "Acciones":
                sectionsArray.accounting.stock = section.state.selected;
                break;
            case "Vehículo":
                sectionsArray.accounting.details.vehicle = section.state.selected;
                break;
            case "Nómina":
                sectionsArray.accounting.details.salary = section.state.selected;
                break;
            case "Luz":
                sectionsArray.accounting.details.light = section.state.selected;
                break;
            case "Agua":
                sectionsArray.accounting.details.water = section.state.selected;
                break;
            case "Inventario":
                sectionsArray.inventory = section.state.selected;
                break;
            case "Viajes":
                sectionsArray.trips = section.state.selected;
                break;
            default:
                break;
        }
    });
    if (sectionsArray.accounting.details.vehicle || sectionsArray.accounting.details.salary || sectionsArray.accounting.details.light || sectionsArray.accounting.details.water) {
        sectionsArray.accounting.details.section = true;
    } else {
        sectionsArray.accounting.details.section = false;
    }
    $.get("/users/shownSectionsSave?sections=" + JSON.stringify(sectionsArray));
    location.reload();
}

var enviando
function checkSubmit() {
    if (!enviando) {
        enviando= true;
        return true;
    } else {
        //Si llega hasta aca significa que pulsaron 2 veces el boton submit
        alert("El formulario ya se esta enviando");
        return false;
    }
}