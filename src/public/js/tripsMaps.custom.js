var etiquetas = new Array();
var datosGraf = new Array();
var continentes = new Array();
var paises = new Array();
var marcadores = new Array();
var lugares = new Array();
var gdpData = new Array();
var mapMarkersScale= new Array();
var mapScale;
var mapaGuardado;
var initialMapScale;
//var map = "es";


$(document).ready(function() {
    cargaMapa(map+'_mill');
});

function cargaMapa(map){
    //console.log(gdpData);
    mapaCargado = map;

    $('#world-map').vectorMap({
        map: map,
        markers: markers,
        backgroundColor: '#589bd7',
        regionStyle: {
            initial: {
                fill            : '#e7dcd8',
                'fill-opacity'  : 1,
                stroke          : 'none',
                'stroke-width'  : 0,
                'stroke-opacity': 1
            }
        },
        series: {
           /*markers: [{
                attribute: 'image',
                scale: {
                    'Viviendo': './img/house_30x30.png',
                    'Programado': './img/programed_30x30.png',
                    'Visitado': './img/visited_30x30.png',
                    'Trabajo': './img/visited_30x30.png'
                  },
                values: marcadores.reduce(function(p, c, i){ p[i] = c.status; return p }, {}),
            }],*/ //Los iconos no se ven bien
            markers: [{
                attribute: 'fill',
                scale: { "Viviendo" : "#226E01",
                        "Programado" : "#85E3D6",   
                        "Visitado" : "#329E56",   
                        "Trabajo" : "#FFC202" }, 
                values: markers.reduce(function(p, c, i){ p[i] = c.status; return p }, {}),
            }],
            regions: [{
                values: countries,
                scale: { "Viviendo" : "#226E01",
                        "Programado" : "#85E3D6",   
                        "Visitado" : "#329E56",   
                        "Trabajo" : "#FFC202" }, 
                normalizeFunction: 'polynomial'
            }],
        },
        markerStyle: {
            initial: {
                fill: '#F8E23B',
                "stroke-width": 1,
                stroke: 'black',
                r: 4
            },
            hover: {
                fill: 'white',
                "stroke-width": 2,
                cursor: 'pointer'
              },
        },
        onRegionClick: function (event, code, region) {

        },
        onRegionOver: function (e, code, region) {
            document.body.style.cursor = "pointer";
        },
        onRegionOut: function (e, code, region) {
            document.body.style.cursor = "default";
        },
        onViewportChange: function (e, scale) {
            //e.preventDefault();
        },
        onMarkerTipShow: function (e, tip, code) {
            //e.preventDefault();
        },
        onMarkerOver: function (e, code) {
            //e.preventDefault();
        },
        onMarkerOut: function (e, code) {
            e.preventDefault();
        },
        onMarkerClick: function (e, code) {
            e.preventDefault();
        },
        onMarkerSelected: function (e, code) {
            e.preventDefault();
        },  
        onLabelShow: function(e, label, code) {
            e.preventDefault();
        }
        
    });
    $('.mapSelected').removeClass('mapSelected');
    $('#'+map).addClass('mapSelected');
    initialMapScale = $('#world-map .jvectormap-container').attr('transform');//.substring(6, 24);
}