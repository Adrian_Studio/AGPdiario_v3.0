//AGP - Script para ordenar en datatables (-pre funciona sobreescribe a -asc y -desc y da el formato de ordenacion)

(function() {
    jQuery.extend( jQuery.fn.dataTableExt.oSort, {
        "formated-sorting-pre": function ( a ) {
            if (!a){
                int = 0;
            }else{
                number = a.split(' ')[0]
                if(number.split('.').length == 1){
                    int = number.split('.')[0]
                }else{
                    int = number.split('.')[0] + number.split('.')[1]
                }
            }
            return parseInt(int);
        },
     
        "formated-sorting-asc": function ( a, b ) {
            return ((a < b) ? -1 : 0);
        },
     
        "formated-sorting-desc": function ( a, b ) {
            return ((a < b) ? 1 : 0);
        }
    } );

}());