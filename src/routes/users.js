const express = require('express');
const router = express.Router();
const User = require('../models/User');
const { isAuthenticated } = require('../helpers/auth');
const passport = require('passport');
const globalVariable = require('../globalVariables/variables');
const usuarios = require('../globalVariables/usuarios');
const nodemailer = require("nodemailer");
const getAlerts = require('../helpers/getAlerts');

var smtpTransport = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: usuarios.correo.user,
        pass: usuarios.correo.password
    },
    tls: { rejectUnauthorized: false }
});

router.get('/users/signIn', (req, res) => {
    req.logout();
    res.render('users/signIn');
});

router.post('/users/signIn', passport.authenticate('local', {
    //successRedirect: res.locals.currentUrl,
    failureRedirect: '/users/signIn',
    failureFlash: true
}), async (req, res) => {
    const user = await User.findById(req.user.id);
    await getAlerts.getAll(req.user.id);
    globalVariable.currentAlerts.periodicAlerts = user.accounting.periodicAlertMessages;
    globalVariable.sections = user.sections;
    globalVariable.lastBankUpdate = user.accounting.lastBankUpdate.toString()
    res.redirect(res.locals.currentUrl);
});

router.get('/users/signUp', (req, res) => {
    req.logout();
    res.render('users/signUp');
});

router.post('/users/signUp', async (req, res) => {
    const { name, surname, username, email, password, confirm_password } = req.body;
    const errors = [];
    if (password != confirm_password) {
        errors.push({ text: 'Las contraseñas no coinciden' })
    }
    if (password.length < 4) {
        errors.push({ text: 'La contraseña debe contener al menos 4 caracteres' })
    }
    if (errors.length > 0) {
        res.render('users/signUp', { errors, username, password, confirm_password });
    } else {
        const userUsername = await User.findOne({ username: username });
        const emailUser = await User.findOne({ email: email });
        if (userUsername || emailUser) {
            if (userUsername) {
                req.flash('error_msg', 'El usuario ya esta usado');
            }
            if (emailUser) {
                req.flash('error_msg', ' El correo ya esta usado');
            }
            res.redirect('/users/signUp');
        } else {
            const newUser = new User({ name, email, surname, username, password });
            newUser.password = await newUser.encryptPassword(password);
            await newUser.save();
            id = newUser.id;
            link = "http://" + req.get('host') + "/verify?id=" + id;
            mailOptions = {
                to: newUser.email,
                subject: "Correo de confirmación AGPdiario",
                html: "Hola " + newUser.name + ",<br> Verifiqué su cuenta pulsando en el siguiente enlace:<br><a href=" + link + ">Pulsa aquí para verificar</a>"
            }
            smtpTransport.sendMail(mailOptions, function (error, response) {
                if (error) {
                    console.log(error);
                } else {
                    console.log("Message sent");
                }
            });
            req.flash('success_msg', 'Nuevo registro completado. Se le ha enviado un correo para verificar.');
            res.redirect('/users/signIn');
        }
    }
});

router.get('/users/logout', (req, res) => {
    res.locals.currentUrl = globalVariable.initialUrl;
    globalVariable.sidebarRight['sidebar'] = false;
    req.logout();
    req.flash('success_msg', 'Has salido de la aplicación');
    res.redirect('/users/signIn');
});

router.post('/users/profileSave', isAuthenticated, async (req, res) => {
    const { name, surname, email } = req.body;
    const user = await User.findById(req.user.id);
    user.name = name;
    user.surname = surname;
    user.email = email;
    await user.save();
    res.redirect(res.locals.currentUrl);
});

router.get('/verify', async function (req, res) {
    if ((req.protocol + "://" + req.get('host')) == ("http://" + req.get('host'))) {
        const user = await User.findById(req.query.id);
        if (user) {
            user.emailVerified = true;
            await user.save();
            req.flash('success_msg', 'El correo ha sido verificado con éxito');
            res.redirect('/users/signIn');
        }
        else {
            req.flash('success_msg', 'El correo no coresponde a ningun usuario. Es posible que el enalce este caducado');
            res.redirect('/users/signIn');
        }
    }
    else {
        res.end("<h1>Petición de una fuente desconocida");
    }
});


router.get('/users/shownSectionsSave', isAuthenticated, async (req, res) => {
    var newShownSections = JSON.parse(req.query.sections);
    const user = await User.findById(req.user.id);
    user.sections = newShownSections;
    await user.save();
    globalVariable.sections = newShownSections;
    req.flash('Secciones guardadas con éxito');
    res.redirect(res.locals.currentUrl);
});

router.post('/users/saveNewPassword', async (req, res) => {
    const { username, password, passwordNew, passwordNew2 } = req.body;
    const errors = [];
    if (passwordNew != passwordNew2) {
        errors.push({ text: 'Las nuevas contraseñas no coinciden' })
        req.flash('error_msg', '  Las nuevas contraseñas no coinciden');
    }
    if (passwordNew.length < 4) {
        errors.push({ text: 'La nueva contraseña debe contener al menos 4 caracteres' })
        req.flash('error_msg', '  La nueva contraseña debe contener al menos 4 caracteres');
    }
    if (errors.length > 0) {
        if (!username) {
            res.redirect('/users/changePwd');
        } else {
            res.redirect('/users/recoverPwd');
        }
    } else {
        if (!username){
            var user = await User.findById(req.user.id);
            var match = await user.matchPassword(password);
        }else{
            var user = await User.findOne({username: username});
            var match = user;
        }
        
        if (match) {
            user.password = await user.encryptPassword(passwordNew);
            await user.save();
            req.flash('success_msg', 'Contraseña cambiada con éxito.');
            res.redirect('/users/signIn');
        } else {
            if (!username){
                errors.push({ text: 'La contraseña actual introducida no es correcta' });
                req.flash('error_msg', '  La contraseña actual introducida no es correcta');
                res.redirect('/users/changePwd');
            }else{
                errors.push({ text: 'Este usuarion no pertenece a esta dirección de correo' });
                req.flash('error_msg', '  Este usuarion no pertenece a esta dirección de correo');
                res.redirect('/users/recoverPwd');
            }
            
        }
        
    }

});

router.get('/users/recoverPwdEmail', async (req, res) => {
    res.render('users/recoverPwdEmail');
});

router.post('/users/recoverPwdSendEmail', async (req, res) => {
    const { email } = req.body;
    var user = await User.findOne({ email: email });
    if (user) {
        id = user.id;
        link = "http://" + req.get('host') + "/users/recoverPwd?id=" + id;
        mailOptions = {
            to: user.email,
            subject: "Correo de recuperación AGPdiario",
            html: "Hola " + user.name + ",<br>Recuerda que tu username es: " + user.username + "<br> Recuperé su cuenta pulsando en el siguiente enlace:<br><a href=" + link + ">Pulsa aquí para recuperar la cuenta</a>"
        }
        smtpTransport.sendMail(mailOptions, function (error, response) {
            if (error) {
                console.log(error);
            } else {
                console.log("Message sent");
            }
        });
        req.flash('success_msg', 'Se ha enviado el email a su correo');
    }else{
        req.flash('success_msg', 'Si ya tenía una cuenta se le envíara un correo para recuperarla');
    }

    res.redirect('/users/signIn');
});

router.get('/users/recoverPwd', async (req, res) => {
    var id = req.query.id
    res.render('users/recoverPwd', { id });
});

router.get('/users/changePwd', isAuthenticated, async (req, res) => {
    res.render('users/changePwd');
});

module.exports = router;