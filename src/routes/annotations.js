const express = require('express');
const router = express.Router();
const User = require('../models/User');
const Note = require('../models/user/annotations/Note');
const Schedule = require('../models/user/annotations/Schedule');
const Task = require('../models/user/annotations/Task');
const getAlerts = require('../helpers/getAlerts');
const { isAuthenticated } = require('../helpers/auth')


router.get('/annotations/annotation/notes', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    const user = await User.findById(req.user.id);
    const note = user.annotations.notes
    res.locals.dashboard = "Anotaciones";
    tab = 'notes';
    res.render('annotations/annotations', { note, tab });
});

router.post('/annotations/annotation/add/notes', isAuthenticated, async (req, res) => {
    const { title, description, priority } = req.body;
    newNote = new Note({ title, description, priority });
    const user = await User.findById(req.user.id);
    user.annotations.notes.push(newNote);
    await user.save();
    req.flash('success_msg', 'Nota agregada con éxito');
    res.redirect('/annotations/annotation/notes');
});

router.get('/getEditData/annotations/notes/:id', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    const user = await User.findById(req.user.id);
    const note = await user.annotations.notes.find(function (element) {
        return element._id == req.params.id;
    });
    res.json({ variable: note });
});

router.post('/annotations/annotation/edit/:id/notes', isAuthenticated, async (req, res) => {
    const { title, description, priority } = req.body;
    const user = await User.findById(req.user.id);
    await user.annotations.notes.find(function (element) {
        if (element._id == req.params.id) {
            element.title = title;
            element.description = description;
            element.priority = priority;
        }
    });

    await user.save();

    req.flash('success_msg', 'Nota actualizada con éxito');
    res.redirect('/annotations/annotation/notes');

});

router.get('/annotations/annotation/delete/:id/notes', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const notes = await user.annotations.notes.filter(function (element) {
        return element._id != req.params.id;
    });
    user.annotations.notes = notes;

    await user.save()

    req.flash('success_msg', 'Nota eliminada con éxito');
    res.redirect('/annotations/annotation/notes');
});

router.get('/annotations/annotation/schedule', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    const user = await User.findById(req.user.id);
    const schedule = user.annotations.schedule
    res.locals.dashboard = "Anotaciones";
    tab = 'schedule';
    res.render('annotations/annotations', { schedule, tab });
});

router.post('/annotations/annotation/add/schedule', isAuthenticated, async (req, res) => {
    const { title, description } = req.body;
    var { date, alertDate } = req.body;
    date = new Date(date.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    if (alertDate) {
        alertDate = new Date(alertDate.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    }

    newSchedule = new Schedule({ title, description, date, alertDate });
    const user = await User.findById(req.user.id);
    user.annotations.schedule.push(newSchedule);
    await user.save();
    await getAlerts.scheduleAlerts(req.user.id);
    req.flash('success_msg', 'Cita agregada con éxito');
    res.redirect('/annotations/annotation/schedule');
});

router.get('/getEditData/annotations/schedule/:id', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    const user = await User.findById(req.user.id);
    const schedule = await user.annotations.schedule.find(function (element) {
        return element._id == req.params.id;
    });
    res.json({ variable: schedule });
});

router.post('/annotations/annotation/edit/:id/schedule', isAuthenticated, async (req, res) => {
    const { title, description } = req.body;
    var { date, alertDate } = req.body;
    date = new Date(date.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    if (alertDate) {
        alertDate = new Date(alertDate.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    }
    const user = await User.findById(req.user.id);
    await user.annotations.schedule.find(function (element) {
        if (element._id == req.params.id) {
            element.title = title;
            element.description = description;
            element.date = date;
            element.alertDate = alertDate;
        }
    });

    await user.save();
    await getAlerts.scheduleAlerts(req.user.id);

    req.flash('success_msg', 'Cita actualizada con éxito');
    res.redirect('/annotations/annotation/schedule');

});

router.get('/annotations/annotation/delete/:id/schedule', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const schedule = await user.annotations.schedule.filter(function (element) {
        return element._id != req.params.id;
    });
    user.annotations.schedule = schedule;

    await user.save()
    await getAlerts.scheduleAlerts(req.user.id);
    req.flash('success_msg', 'Cita eliminada con éxito');
    res.redirect('/annotations/annotation/schedule');
});

router.get('/annotations/annotation/tasks', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    const user = await User.findById(req.user.id);
    const task = user.annotations.tasks
    res.locals.dashboard = "Anotaciones";
    tab = 'tasks';
    res.render('annotations/annotations', { task, tab });
});

router.post('/annotations/annotation/add/tasks', isAuthenticated, async (req, res) => {
    const { title, description, priority, alertActive } = req.body;
    newTask = new Task({ title, description, priority, alertActive });
    const user = await User.findById(req.user.id);
    user.annotations.tasks.push(newTask);
    await user.save();
    await getAlerts.tasksAlerts(req.user.id);
    req.flash('success_msg', 'Tarea agregada con éxito');
    res.redirect('/annotations/annotation/tasks');
});

router.get('/getEditData/annotations/tasks/:id', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    const user = await User.findById(req.user.id);
    const task = await user.annotations.tasks.find(function (element) {
        return element._id == req.params.id;
    });
    res.json({ variable: task });
});

router.post('/annotations/annotation/edit/:id/tasks', isAuthenticated, async (req, res) => {
    const { title, description, priority, alertActive } = req.body;
    const user = await User.findById(req.user.id);
    await user.annotations.tasks.find(function (element) {
        if (element._id == req.params.id) {
            element.title = title;
            element.description = description;
            element.priority = priority;
            element.alertActive = alertActive;
        }
    });

    await user.save();
    await getAlerts.tasksAlerts(req.user.id);
    req.flash('success_msg', 'Tarea actualizada con éxito');
    res.redirect('/annotations/annotation/tasks');

});

router.get('/annotations/annotation/delete/:id/tasks', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const tasks = await user.annotations.tasks.filter(function (element) {
        return element._id != req.params.id;
    });
    user.annotations.tasks = tasks;
    await user.save()
    await getAlerts.tasksAlerts(req.user.id);
    req.flash('success_msg', 'Tarea eliminada con éxito');
    res.redirect('/annotations/annotation/tasks');
});

module.exports = router;