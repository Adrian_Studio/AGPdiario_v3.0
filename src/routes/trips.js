const express = require('express');
const router = express.Router();
const User = require('../models/User');
const Country = require('../models/Country');
const Trip = require('../models/user/trips/Trip');
const { isAuthenticated } = require('../helpers/auth');

const getAlerts = require('../helpers/getAlerts');
const filterTripsArray = require('../helpers/filterTripsArray');
const globalVariable = require('../globalVariables/variables');
const globalConstant = require('../globalVariables/constants');

router.get('/trips/maps/:tab', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Viajes";

    const user = await User.findById(req.user.id);
    var tab = req.params.tab;
    var trips = user.trips.trips//{latLng: [lat, lon], name: element.lugar, status: element.categoria}
    var markers = new Array();
    var countries = {};
    if (tab == 'es'){
        var trips = trips.filter(trip => trip.countryCode == 'ES');
    };
    trips.forEach(element => {
        if (element.latitude && element.longitude && element.place) {
            markers.push({ latLng: [element.latitude, element.longitude], name: element.place, status: element.category })
        }
        if (countries[element.countryCode] == undefined ||
            (countries[element.countryCode] == "Trabajo") ||
            (countries[element.countryCode] == "Visitado" && element.category != "Trabajo") ||
            (countries[element.countryCode] == "Programado" && element.category != "Visitado" && element.category != "Trabajo") ||
            element.category == "Viviendo") {

            countries[element.countryCode] = element.category
        }
    });
    res.render('trips/map', { tab, markers, countries });
});

router.get('/trips/tables/:tab', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Viajes";
    var tripsFilterArray = globalVariable.tripsFilterArray;
    const user = await User.findById(req.user.id);
    var tab = req.params.tab;
    var filter = false;
    var region = globalConstant.tripsRegionsArray;
    var category = globalConstant.tripsCategoriesArray;
    const country = await Country.find();
    if (tab.endsWith('Filtered')) {
        filter = true;
        tab = tab.replace('Filtered', '');
    } else {
        globalVariable.tripsFilterArray = {};
    }
    var trip = user.trips.trips;
    if (filter) {
        trip = filterTripsArray.filterTripsArray(trip, globalVariable.tripsFilterArray);
    } else {
        trip = trip/*.filter(dateLimitTable)*/;
    }
    res.render('trips/trip', { trip, tab, country, region, category, filter, tripsFilterArray });
});

router.post('/trips/tables/add/:tab', isAuthenticated, async (req, res) => {
    const { countryCode, latitude, longitude, place, category, annotation } = req.body;
    var { date } = req.body;
    const tab = req.params.tab;
    date = new Date(date.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    const user = await User.findById(req.user.id);
    var countryArray = await Country.findOne({ "alpha-2": countryCode });
    var country = countryArray['name-Spanish'];
    var region = countryArray['region-Spanish'];
    const newTrip = new Trip({
        date, region, country, countryCode, latitude, longitude, place, category, annotation
    });
    user.trips.trips.push(newTrip);
    await user.save();
    await getAlerts.tripsAlerts(req.user.id);
    req.flash('success_msg', 'Viaje agregado con éxito');
    if (Object.keys(globalVariable.tripsFilterArray).length === 0) {
        res.redirect('/trips/tables/' + tab);
    } else {
        res.redirect('/trips/tables/' + tab + 'Filtered');
    }

});

router.get('/getEditData/trip/:id', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const element = await user.trips.trips.find(function (element) {
        return element._id == req.params.id;
    });
    res.json({ variable: element });
});

router.post('/trips/tables/edit/:id/:tab', isAuthenticated, async (req, res) => {
    const { countryCode, latitude, longitude, place, category, annotation } = req.body;
    var { date } = req.body;
    const tab = req.params.tab;
    date = new Date(date.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    const user = await User.findById(req.user.id);
    var countryArray = await Country.findOne({ "alpha-2": countryCode });
    var country = countryArray['name-Spanish'];
    var region = countryArray['region-Spanish'];

    await user.trips.trips.find(function (elementEdit) {
        if (elementEdit._id == req.params.id) {
            elementEdit.date = date;
            elementEdit.region = region;
            elementEdit.country = country;
            elementEdit.countryCode = countryCode;
            elementEdit.latitude = latitude;
            elementEdit.longitude = longitude;
            elementEdit.place = place;
            elementEdit.category = category;
            elementEdit.annotation = annotation;
        }
    });
    await user.save();
    await getAlerts.tripsAlerts(req.user.id);
    req.flash('success_msg', 'Viaje actualizado con éxito');
    if (Object.keys(globalVariable.tripsFilterArray).length === 0) {
        res.redirect('/trips/tables/' + tab);
    } else {
        res.redirect('/trips/tables/' + tab + 'Filtered');
    }

});

router.get('/trips/tables/delete/:id/:tab', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const tab = req.params.tab;
    const trip = await user.trips.trips.filter(function (element) {
        return element._id != req.params.id;
    });
    user.trips.trips = trip;

    await user.save();
    await getAlerts.tripsAlerts(req.user.id);
    req.flash('success_msg', 'Viaje eliminado con éxito');
    res.redirect('/trips/tables/' + tab);

});

router.post('/trips/:group/filter/:tab', isAuthenticated, async (req, res) => {
    const { regionFilter, categoryFilter } = req.body;
    var filter = {};
    const tab = req.params.tab;
    const group = req.params.group;
    globalVariable.tripsFilterArray = {}
    if (regionFilter) { filter.region = regionFilter };
    if (categoryFilter) { filter.category = categoryFilter };
    globalVariable.tripsFilterArray = filter;
    if (Object.keys(globalVariable.tripsFilterArray).length === 0) {
        res.redirect('/trips/' + group + '/' + tab);
    } else {
        res.redirect('/trips/' + group + '/' + tab + 'Filtered');
    }
});

module.exports = router;