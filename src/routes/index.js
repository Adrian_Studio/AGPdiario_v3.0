const express = require('express');
const router = express.Router();
const globalVariable = require('../globalVariables/variables');

const { isAuthenticated } = require('../helpers/auth')


router.get('/', isAuthenticated, (req, res) => {
    req.logout();
    res.redirect('/users/signIn');
});

router.post('/changeLeftSidebar', isAuthenticated, (req, res) => {
    globalVariable.sidebarCollapsed = !globalVariable.sidebarCollapsed
    res.end()
});
router.post('/changeRightSidebar/:part', isAuthenticated, (req, res) => {
    globalVariable.sidebarRight[req.params.part] = !globalVariable.sidebarRight[req.params.part]
    res.end()
});

router.post('/closeRightSidebar', isAuthenticated, (req, res) => {
    globalVariable.sidebarRight['sidebar'] = false;
    res.end()
});

router.post('/toggleEnabledSignUp', isAuthenticated, (req, res) => {
    globalVariable.signUpEnabled = !globalVariable.signUpEnabled;
    res.end()
});

module.exports = router;