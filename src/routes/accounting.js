const express = require('express');
const router = express.Router();
const User = require('../models/User');
const Cashflow = require('../models/user/accounting/Cashflow');
const Category = require('../models/user/accounting/dropdowns/Category');
const Concept = require('../models/user/accounting/dropdowns/Concept');
const Home = require('../models/user/accounting/dropdowns/Home');
const Vehicle = require('../models/user/accounting/dropdowns/Vehicle');
const Entity = require('../models/user/accounting/dropdowns/Entity');
const PeriodicAlert = require('../models/user/accounting/PeriodicAlert');
const { isAuthenticated } = require('../helpers/auth');
const filterAccountingArray = require('../helpers/filterAccountingArray');
const getCashflowYearsList = require('../helpers/getCashflowYearsList');
const graphsCashflowFormat = require('../helpers/graphsCashflowFormat');
const getCashflowUsedList = require('../helpers/getCashflowUsedList');
const processCashflowData = require('../helpers/processCashflowData');
const getAlerts = require('../helpers/getAlerts');
const getSummary = require('../helpers/getSummary');
const globalVariable = require('../globalVariables/variables');

router.get('/accounting/cashflow', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Movimientos contablilidad";
    globalVariable.globalFilterArray = {}
    res.redirect('/accounting/cashflow/expensesCashflow');
});

router.get('/accounting/cashflow/:tab', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Movimientos contablilidad";
    const user = await User.findById(req.user.id);
    var tab = req.params.tab;
    var globalFilterArray = globalVariable.globalFilterArray;
    var type;
    var cashflow;
    var filter = false;
    if (tab.endsWith('Filtered')) {
        filter = true;
        tab = tab.replace('Filtered', '');
    } else {
        globalVariable.globalFilterArray = {};
    }
    switch (tab) {
        case 'incomesCashflow':
            incomesCashflowConditions = data => (data.type == 'Ingreso');
            var group = 'General';
            type = 'Ingreso';
            break;
        case 'expensesCashflow':
            expensesCashflowConditions = data => (data.type == 'Gasto');
            var group = 'General';
            type = 'Gasto';
            break;
        case 'incomesHomeCashflow':
            incomesHomeCashflowConditions = data => (data.type == 'Ingreso' && data.group == 'Vivienda');
            var group = 'Vivienda';
            type = 'Ingreso';
            break;
        case 'expensesHomeCashflow':
            expensesHomeCashflowConditions = data => (data.type == 'Gasto' && data.group == 'Vivienda');
            var group = 'Vivienda';
            type = 'Gasto';
            break;
        case 'incomesVehicleCashflow':
            incomesVehicleCashflowConditions = data => (data.type == 'Ingreso' && data.group == 'Vehículo');
            var group = 'Vehículo';
            type = 'Ingreso';
            break;
        case 'expensesVehicleCashflow':
            expensesVehicleCashflowConditions = data => (data.type == 'Gasto' && data.group == 'Vehículo');
            var group = 'Vehículo';
            type = 'Gasto';
            break;
        default:
            break;
    }
    const grossCashflow = user.accounting.cashflow.filter(eval(tab + 'Conditions'));
    const category = user.accounting.dropdowns.categories.filter(eval(tab + 'Conditions')).sort((a, b) => (a.type < b.type) ? 1 : (a.type === b.type) ? ((a.categoryPriority > b.categoryPriority) ? 1 : -1) : -1);
    const concept = user.accounting.dropdowns.concepts.filter(eval(tab + 'Conditions')).sort((a, b) => (a.type < b.type) ? 1 : (a.type === b.type) ? ((a.categoryPriority > b.categoryPriority) ? 1 : (a.categoryPriority === b.categoryPriority) ? ((a.conceptPriority > b.conceptPriority) ? 1 : -1) : -1) : -1);
    const vehicle = user.accounting.dropdowns.vehicles;
    const home = user.accounting.dropdowns.homes;
    if (filter) {
        cashflow = filterAccountingArray.filterCashflowArray(grossCashflow, globalVariable.globalFilterArray);
    } else {
        function dateLimitTable(data) {
            var today = new Date();
            var dateLimit = new Date(today.setMonth(today.getMonth() - 6)); //La tabla por defecto solo muestra 6 meses.
            var date = new Date(data.date);
            return date >= dateLimit;
        }
        cashflow = grossCashflow/*.filter(dateLimitTable)*/;
    }
    cashflow.sort((a, b) => (a.date < b.date) ? 1 : (a.date === b.date) ? ((a.creationDate > b.creationDate) ? 1 : -1) : -1)
    const year = getCashflowYearsList.getCashflowYearsList(grossCashflow, 'desc');
    const usedCategories = getCashflowUsedList.getCashflowUsedList(grossCashflow, 'category').sort((a, b) => (a.type < b.type) ? 1 : (a.type === b.type) ? ((a.categoryPriority > b.categoryPriority) ? 1 : -1) : -1);
    var usedCategoriesIncome = usedCategories.filter(data => (data.type == 'Ingreso'));
    var usedCategoriesExpense = usedCategories.filter(data => (data.type == 'Gasto'));
    const usedConcepts = getCashflowUsedList.getCashflowUsedList(grossCashflow, 'concept').sort((a, b) => (a.type < b.type) ? 1 : (a.type === b.type) ? ((a.categoryPriority > b.categoryPriority) ? 1 : (a.categoryPriority === b.categoryPriority) ? ((a.conceptPriority > b.conceptPriority) ? 1 : -1) : -1) : -1);
    const usedHomes = getCashflowUsedList.getCashflowUsedList(grossCashflow, 'home').sort((a, b) => (a.homePriority > b.homePriority) ? 1 : -1);
    const usedVehicles = getCashflowUsedList.getCashflowUsedList(grossCashflow, 'vehicle').sort((a, b) => (a.vehiclePriority > b.vehiclePriority) ? 1 : -1);
    res.render('accounting/cashflow', { cashflow, tab, group, category, concept, vehicle, home, year, type, usedCategories, usedCategoriesIncome, usedCategoriesExpense, usedConcepts, usedHomes, usedVehicles, globalFilterArray, filter });
});

router.post('/accounting/cashflow/add/:tab', isAuthenticated, async (req, res) => {
    const { category, concept, amount, irpf, grossAmount, home, homeWaterConsumtion, homeLightConsumtion, homeGasConsumtion,
        vehicle, vehicleMilometer, vehicleGasConsumtion, vehicleLightConsumtion, observation } = req.body;
    var { date } = req.body;
    const tab = req.params.tab;
    date = new Date(date.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    const user = await User.findById(req.user.id);
    const conceptData = await user.accounting.dropdowns.concepts.find(data => (data.concept == concept));
    const type = conceptData.type;
    const group = conceptData.group;
    const subgroup = conceptData.subgroup;
    const categoryPriority = conceptData.categoryPriority;
    const conceptPriority = conceptData.conceptPriority;
    var vehicleType = undefined;
    var vehiclePriority = undefined;
    var homePriority = undefined;
    if (vehicle) {
        const vehicleData = await user.accounting.dropdowns.vehicles.find(data => (data.description == vehicle));
        vehicleType = vehicleData.vehicleType;
        vehiclePriority = vehicleData.vehiclePriority
    }
    if (home) {
        const homeData = await user.accounting.dropdowns.homes.find(data => (data.address == home));
        homePriority = homeData.homePriority
    }

    const newCashflow = new Cashflow({
        type, category, categoryPriority, group, concept, conceptPriority, subgroup, date, amount, irpf, grossAmount, home, homePriority, homeWaterConsumtion, homeLightConsumtion,
        homeGasConsumtion, vehicle, vehiclePriority, vehicleType, vehicleMilometer, vehicleGasConsumtion, vehicleLightConsumtion, observation
    });

    user.accounting.cashflow.push(newCashflow);
    await user.save()
    await getAlerts.periodicAlerts(req.user.id)
    req.flash('success_msg', 'Movimiento agregado con éxito');
    if (Object.keys(globalVariable.globalFilterArray).length === 0) {
        res.redirect('/accounting/cashflow/' + tab);
    } else {
        res.redirect('/accounting/cashflow/' + tab + 'Filtered');
    }

});

router.get('/getEditData/cashflow/:id', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const cashflow = await user.accounting.cashflow.find(function (element) {
        return element._id == req.params.id;
    });
    res.json({ variable: cashflow });
});

router.post('/accounting/cashflow/edit/:id/:tab', isAuthenticated, async (req, res) => {
    const { category, concept, amount, irpf, grossAmount, home, homeWaterConsumtion, homeLightConsumtion, homeGasConsumtion,
        vehicle, vehicleMilometer, vehicleGasConsumtion, vehicleLightConsumtion, observation } = req.body;
    var { date } = req.body;
    const tab = req.params.tab;
    date = new Date(date.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    const user = await User.findById(req.user.id);
    const conceptData = await user.accounting.dropdowns.concepts.find(data => (data.concept == concept));
    const type = conceptData.type;
    const group = conceptData.group;
    const subgroup = conceptData.subgroup;
    const categoryPriority = conceptData.categoryPriority;
    const conceptPriority = conceptData.conceptPriority;
    var vehicleType = undefined;
    var vehiclePriority = undefined;
    var homePriority = undefined;
    if (vehicle) {
        const vehicleData = await user.accounting.dropdowns.vehicles.find(data => (data.description == vehicle));
        vehicleType = vehicleData.vehicleType;
        vehiclePriority = vehicleData.vehiclePriority;
    }
    if (home) {
        const homeData = await user.accounting.dropdowns.homes.find(data => (data.address == home));
        homePriority = homeData.homePriority;
    }
    await user.accounting.cashflow.find(function (element) {
        if (element._id == req.params.id) {
            element.type = type;
            element.category = category;
            element.categoryPriority = categoryPriority;
            element.group = group;
            element.concept = concept;
            element.conceptPriority = conceptPriority;
            element.subgroup = subgroup;
            element.date = date;
            element.amount = amount;
            element.irpf = irpf;
            element.grossAmount = grossAmount;
            element.home = home;
            element.homePriority = homePriority;
            element.homeWaterConsumtion = homeWaterConsumtion;
            element.homeLightConsumtion = homeLightConsumtion;
            element.homeGasConsumtion = homeGasConsumtion;
            element.vehicle = vehicle;
            element.vehiclePriority = vehiclePriority;
            element.vehicleType = vehicleType;
            element.vehicleMilometer = vehicleMilometer;
            element.vehicleGasConsumtion = vehicleGasConsumtion;
            element.vehicleLightConsumtion = vehicleLightConsumtion;
            element.observation = observation;
        }
    });
    await user.save();
    await getAlerts.periodicAlerts(req.user.id)
    req.flash('success_msg', 'Movimiento actualizado con éxito');
    if (Object.keys(globalVariable.globalFilterArray).length === 0) {
        res.redirect('/accounting/cashflow/' + tab);
    } else {
        res.redirect('/accounting/cashflow/' + tab + 'Filtered');
    }

});

router.get('/accounting/cashflow/delete/:id/:tab', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const tab = req.params.tab;
    const cashflow = await user.accounting.cashflow.filter(function (element) {
        return element._id != req.params.id;
    });
    user.accounting.cashflow = cashflow;

    await user.save()
    await getAlerts.periodicAlerts(req.user.id)
    req.flash('success_msg', 'Movimiento eliminado con éxito');
    res.redirect('/accounting/cashflow/' + tab);

});

router.get('/accounting/stock/:tab', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Acciones de bolsa";
    const user = await User.findById(req.user.id);
    var tab = req.params.tab;
    var globalFilterArray = globalVariable.globalFilterArray;
    var type;
    var cashflow;
    var filter = false;
    if (tab.endsWith('Filtered')) {
        filter = true;
        tab = tab.replace('Filtered', '');
    } else {
        globalVariable.globalFilterArray = {};
    }
    switch (tab) {
        case 'general':
            generalConditions = data => (data.type == 'Acciones');
            var group = 'General';
            type = 'Acciones';
            break;
        default:
            break;
    }
    const grossCashflow = user.accounting.cashflow.filter(eval(tab + 'Conditions'));
    const entity = user.accounting.dropdowns.entities.sort((a, b) => (a.entity < b.entity) ? 1 : (a.entity === b.entity) ? ((a.entityPriority > b.entityPriority) ? 1 : -1) : -1);;
    if (filter) {
        cashflow = filterAccountingArray.filterCashflowArray(grossCashflow, globalVariable.globalFilterArray);
    } else {
        function dateLimitTable(data) {
            var today = new Date();
            var dateLimit = new Date(today.setMonth(today.getMonth() - 6)); //La tabla por defecto solo muestra 6 meses.
            var date = new Date(data.date);
            return date >= dateLimit;
        }
        cashflow = grossCashflow/*.filter(dateLimitTable)*/;
    }
    const year = getCashflowYearsList.getCashflowYearsList(grossCashflow, 'desc');
    const usedEntities = getCashflowUsedList.getCashflowUsedList(grossCashflow, 'entity').sort((a, b) => (a.entityPriority > b.entityPriority) ? 1 : -1);
    res.render('accounting/stock', { cashflow, tab, group, entity, year, type, usedEntities, globalFilterArray, filter });
});


router.post('/accounting/stock/add/:tab', isAuthenticated, async (req, res) => {
    const { subgroup, entity, amount, stockNumber, observation } = req.body;
    var { date } = req.body;
    var type = 'Acciones';
    const tab = req.params.tab;
    date = new Date(date.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    const user = await User.findById(req.user.id);
    const entityData = await user.accounting.dropdowns.entities.find(data => (data.entity == entity));
    const entityPriority = entityData.entityPriority;

    const newCashflow = new Cashflow({ type, subgroup, entity, entityPriority, date, amount, stockNumber, observation });

    user.accounting.cashflow.push(newCashflow);
    await user.save()

    req.flash('success_msg', 'Movimiento agregado con éxito');
    if (Object.keys(globalVariable.globalFilterArray).length === 0) {
        res.redirect('/accounting/stock/' + tab);
    } else {
        res.redirect('/accounting/stock/' + tab + 'Filtered');
    }

});

router.post('/accounting/stock/edit/:id/:tab', isAuthenticated, async (req, res) => {
    const { subgroup, entity, amount, stockNumber, observation } = req.body;
    var { date } = req.body;
    const tab = req.params.tab;
    date = new Date(date.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    const user = await User.findById(req.user.id);
    const entityData = await user.accounting.dropdowns.entities.find(data => (data.entity == entity));
    const entityPriority = entityData.entityDataPriority;

    await user.accounting.cashflow.find(function (element) {
        if (element._id == req.params.id) {
            //element.type = type;
            element.entity = entity;
            element.entityPriority = entityPriority;
            element.stockNumber = stockNumber;
            element.subgroup = subgroup;
            element.date = date;
            element.amount = amount;
            element.observation = observation;
        }
    });
    await user.save();

    req.flash('success_msg', 'Movimiento actualizado con éxito');
    if (Object.keys(globalVariable.globalFilterArray).length === 0) {
        res.redirect('/accounting/stock/' + tab);
    } else {
        res.redirect('/accounting/stock/' + tab + 'Filtered');
    }

});


router.get('/accounting/stock/delete/:id/:tab', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const tab = req.params.tab;
    const cashflow = await user.accounting.cashflow.filter(function (element) {
        return element._id != req.params.id;
    });
    user.accounting.cashflow = cashflow;

    await user.save()
    await getAlerts.periodicAlerts(req.user.id)
    req.flash('success_msg', 'Movimiento eliminado con éxito');
    res.redirect('/accounting/stock/' + tab);

});

router.post('/accounting/:group/filter/:tab', isAuthenticated, async (req, res) => {
    const { categoryFilter, conceptFilter, yearFilter, monthFilter, startDateFilter, endDateFilter, vehicleFilter, homeFilter, entityFilter } = req.body;
    var filter = {};
    const tab = req.params.tab;
    const group = req.params.group;
    globalVariable.globalFilterArray = {}
    if (categoryFilter) { filter.category = categoryFilter };
    if (conceptFilter) { filter.concept = conceptFilter };
    if (vehicleFilter) { filter.vehicle = vehicleFilter };
    if (homeFilter) { filter.home = homeFilter };
    if (entityFilter) { filter.entity = entityFilter };
    if (startDateFilter) { filter.startDate = startDateFilter };
    if (endDateFilter) { filter.endDate = endDateFilter };
    if (yearFilter) { filter.yearDate = yearFilter };
    if (monthFilter) { filter.monthDate = monthFilter };
    globalVariable.globalFilterArray = filter;
    if (Object.keys(globalVariable.globalFilterArray).length === 0) {
        res.redirect('/accounting/' + group + '/' + tab);
    } else {
        res.redirect('/accounting/' + group + '/' + tab + 'Filtered');
    }
});

router.get('/accounting/graphs', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Gráficos contabilidad";
    globalVariable.globalFilterArray = {}
    res.redirect('/accounting/graphs/general');
});

router.get('/accounting/graphs/:tab', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Gráficos contabilidad";
    const user = await User.findById(req.user.id);
    var tab = req.params.tab;
    var globalFilterArray = globalVariable.globalFilterArray;
    var cashflow;
    var filter = false;
    if (tab.endsWith('Filtered')) {
        filter = true;
        tab = tab.replace('Filtered', '');
    } else {
        globalVariable.globalFilterArray = {};
    }
    switch (tab) {
        case 'general':
            generalConditions = data => (true);
            var group = 'General';
            break;
        case 'vehicle':
            vehicleConditions = data => (data.group == 'Vehículo');
            var group = 'Vehículo';
            break;
        case 'home':
            homeConditions = data => (data.group == 'Vivienda');
            var group = 'Vivienda';
            break;
        default:
            break;
    }
    const grossCashflow = user.accounting.cashflow.filter(eval(tab + 'Conditions'));
    const category = user.accounting.dropdowns.categories.filter(eval(tab + 'Conditions')).sort((a, b) => (a.type < b.type) ? 1 : (a.type === b.type) ? ((a.categoryPriority > b.categoryPriority) ? 1 : -1) : -1);
    const concept = user.accounting.dropdowns.concepts.filter(eval(tab + 'Conditions')).sort((a, b) => (a.type < b.type) ? 1 : (a.type === b.type) ? ((a.categoryPriority > b.categoryPriority) ? 1 : (a.categoryPriority === b.categoryPriority) ? ((a.conceptPriority > b.conceptPriority) ? 1 : -1) : -1) : -1);
    const vehicle = user.accounting.dropdowns.vehicles;
    const home = user.accounting.dropdowns.homes;
    if (filter) {
        cashflow = filterAccountingArray.filterCashflowArray(grossCashflow, globalVariable.globalFilterArray);
    } else {
        cashflow = grossCashflow;
    }

    const year = getCashflowYearsList.getCashflowYearsList(grossCashflow, 'desc');
    const usedCategories = getCashflowUsedList.getCashflowUsedList(grossCashflow, 'category').sort((a, b) => (a.type < b.type) ? 1 : (a.type === b.type) ? ((a.categoryPriority > b.categoryPriority) ? 1 : -1) : -1);
    const usedCategoriesIncome = usedCategories.filter(data => (data.type == 'Ingreso'));
    const usedCategoriesExpense = usedCategories.filter(data => (data.type == 'Gasto'));
    const usedConcepts = getCashflowUsedList.getCashflowUsedList(grossCashflow, 'concept').sort((a, b) => (a.type < b.type) ? 1 : (a.type === b.type) ? ((a.categoryPriority > b.categoryPriority) ? 1 : (a.categoryPriority === b.categoryPriority) ? ((a.conceptPriority > b.conceptPriority) ? 1 : -1) : -1) : -1);
    const usedHomes = getCashflowUsedList.getCashflowUsedList(grossCashflow, 'home').sort((a, b) => (a.homePriority > b.homePriority) ? 1 : -1);
    const usedVehicles = getCashflowUsedList.getCashflowUsedList(grossCashflow, 'vehicle').sort((a, b) => (a.vehiclePriority > b.vehiclePriority) ? 1 : -1);
    const balanceChartData = graphsCashflowFormat.graphsCashflowBalanceChartFormat(cashflow);
    const pieChartData = graphsCashflowFormat.graphsCashflowPieChartFormat(cashflow);
    res.render('accounting/graphs', { balanceChartData, pieChartData, group, tab, category, concept, vehicle, home, year, usedCategories, usedCategoriesIncome, usedCategoriesExpense, usedConcepts, usedHomes, usedVehicles, globalFilterArray, filter });
});

router.get('/accounting/summary', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Resumenes contabilidad";
    globalVariable.globalFilterArray = {}
    res.redirect('/accounting/summary/annualGeneral');
});

router.get('/accounting/summary/:tab', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Resumenes contabilidad";
    const user = await User.findById(req.user.id);
    var tab = req.params.tab;
    var globalFilterArray;
    var type;
    var isSummary = true;
    var summary = {};
    var cashflow;
    var filter = false;
    if (tab.endsWith('Filtered')) {
        filter = true;
        tab = tab.replace('Filtered', '');
    } else {
        globalVariable.globalFilterArray = {};
    }

    switch (true) {
        case tab.endsWith('General'):
            var grossCashflow = user.accounting.cashflow;
            var summaryType = tab.replace('General', '');
            var group = 'General';
            break;
        case tab.endsWith('Vehículo'):
            var grossCashflow = user.accounting.cashflow.filter(data => (data.group == 'Vehículo'));
            var summaryType = tab.replace('Vehículo', '');
            var group = 'Vehículo';
            break;
        case tab.endsWith('Vivienda'):
            var grossCashflow = user.accounting.cashflow.filter(data => (data.group == 'Vivienda'));
            var summaryType = tab.replace('Vivienda', '');
            var group = 'Vivienda';
            break;
        default:

            break;
    }
    const year = getCashflowYearsList.getCashflowYearsList(grossCashflow, 'desc');
    switch (summaryType) {
        case 'total':
            break;
        case 'annual':
            if (!globalVariable.globalFilterArray.yearDate) { globalVariable.globalFilterArray.yearDate = year.reduce((p, c) => { return (p.year > c.year) ? p : c }).year.toString(); filter = true; }
            break;
        default:
            break;
    }
    const vehicle = user.accounting.dropdowns.vehicles;
    const home = user.accounting.dropdowns.homes;
    if (filter) {
        cashflow = filterAccountingArray.filterCashflowArray(grossCashflow, globalVariable.globalFilterArray);
    } else {
        cashflow = grossCashflow;
    }
    switch (summaryType) {
        case 'total':
            summary = getSummary.getSummaryTotal(cashflow);
            break;
        case 'annual':
            summary = getSummary.getSummaryAnnual(cashflow);
            break;
        default:
            break;
    }
    globalFilterArray = globalVariable.globalFilterArray
    const usedCategories = getCashflowUsedList.getCashflowUsedList(grossCashflow, 'category').sort((a, b) => (a.type < b.type) ? 1 : (a.type === b.type) ? ((a.categoryPriority > b.categoryPriority) ? 1 : -1) : -1);
    var usedCategoriesIncome = usedCategories.filter(data => (data.type == 'Ingreso'));
    var usedCategoriesExpense = usedCategories.filter(data => (data.type == 'Gasto'));
    const usedConcepts = getCashflowUsedList.getCashflowUsedList(grossCashflow, 'concept').sort((a, b) => (a.type < b.type) ? 1 : (a.type === b.type) ? ((a.categoryPriority > b.categoryPriority) ? 1 : (a.categoryPriority === b.categoryPriority) ? ((a.conceptPriority > b.conceptPriority) ? 1 : -1) : -1) : -1);
    const usedHomes = getCashflowUsedList.getCashflowUsedList(grossCashflow, 'home').sort((a, b) => (a.homePriority > b.homePriority) ? 1 : -1);
    const usedVehicles = getCashflowUsedList.getCashflowUsedList(grossCashflow, 'vehicle').sort((a, b) => (a.vehiclePriority > b.vehiclePriority) ? 1 : -1);
    res.render('accounting/summary', { summary, summaryType, isSummary, group, tab, year, usedCategories, usedCategoriesIncome, usedCategoriesExpense, usedConcepts, usedHomes, usedVehicles, filter, globalFilterArray });
});

router.get('/accounting/details', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Detalles contabilidad";
    if (res.locals.sections.accounting.details.vehicle) {
        res.redirect('/accounting/details/vehicle');
    } else if (res.locals.sections.accounting.details.salary) {
        res.redirect('/accounting/details/salary');
    } else {
        res.redirect('/accounting/details/¿?');
    }

});

router.get('/accounting/details/:tab', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Detalles contabilidad";
    const user = await User.findById(req.user.id);
    var globalFilterArray = globalVariable.globalFilterArray;
    var tab = req.params.tab;
    var filter = false;
    var grossData;
    if (tab.endsWith('Filtered')) {
        filter = true;
        grossData = filterAccountingArray.filterCashflowArray(user.accounting.cashflow, globalVariable.globalFilterArray);
        tab = tab.replace('Filtered', '');
    } else {
        globalVariable.globalFilterArray = {};
        grossData = user.accounting.cashflow;
    }
    var vehicleShownProcessed, vehicleGrossData, usedVehicle;
    var salaryShownProcessed, salaryGrossData;
    var homeGrossData, usedHome;
    var entityGrossData, usedEntity;
    var waterGrossData, waterShownProcessed;
    var lightGrossData, lightShownProcessed;
    var stockGrossData, stockShownProcessed;
    var barChartData = {};
    switch (true) {
        case (tab == 'salary'):
            salaryGrossData = grossData.filter(data => (data.subgroup == 'Nómina'));
            salaryShownProcessed = processCashflowData.processSalaryData(salaryGrossData);
            break;
        case (tab == 'vehicle'):
            vehicleGrossData = grossData.filter(data => (data.vehicle));
            usedVehicle = getCashflowUsedList.getCashflowUsedList(vehicleGrossData, 'vehicle').sort((a, b) => (a.vehiclePriority > b.vehiclePriority) ? 1 : -1);
            if (!req.query.vehicle) { var vehicleShown = usedVehicle[0].vehicleUsed; }
            else { var vehicleShown = req.query.vehicle; }
            vehicleShownData = vehicleGrossData.filter(data => (data.vehicle == vehicleShown));
            vehicleShownProcessed = processCashflowData.processVehicleData(vehicleShownData);
            break;
        case (tab == 'water'):
            waterGrossData = grossData.filter(data => (data.subgroup == 'Agua Vivienda'));
            usedHome = getCashflowUsedList.getCashflowUsedList(waterGrossData, 'home').sort((a, b) => (a.homePriority > b.homePriority) ? 1 : -1);
            if (!req.query.home) { var homeShown = usedHome[0].homeUsed; }
            else { var homeShown = req.query.home; }
            waterShownData = waterGrossData.filter(data => (data.home == homeShown));
            waterShownProcessed = processCashflowData.processWaterData(waterShownData);
            barChartData = waterShownProcessed.waterGraph;
            break;
        case (tab == 'light'):
            lightGrossData = grossData.filter(data => (data.subgroup == 'Luz Vivienda'));
            usedHome = getCashflowUsedList.getCashflowUsedList(lightGrossData, 'home').sort((a, b) => (a.homePriority > b.homePriority) ? 1 : -1);
            if (!req.query.home) { var homeShown = usedHome[0].homeUsed; }
            else { var homeShown = req.query.home; }
            lightShownData = lightGrossData.filter(data => (data.home == homeShown));
            lightShownProcessed = processCashflowData.processLightData(lightShownData);
            barChartData = lightShownProcessed.lightGraph;
            break;
        case (tab == 'stock'):
            stockGrossData = grossData.filter(data => (data.type == 'Acciones'));
            usedEntity = getCashflowUsedList.getCashflowUsedList(stockGrossData, 'entity').sort((a, b) => (a.entityPriority > b.entityPriority) ? 1 : -1);
            if (!req.query.entity) {
                var entityShown = 'Todas'; 
                stockShownData = stockGrossData
            }else { 
                var entityShown = req.query.entity; 
                stockShownData = stockGrossData.filter(data => (data.entity == entityShown));
            }
            
            stockShownProcessed = processCashflowData.processStockData(stockShownData);
            barChartData = stockShownProcessed.stockGraph;
            break;
        default:
            break;
    }

    res.render('accounting/details', { tab, usedVehicle, vehicleShown, usedHome, homeShown, usedEntity, entityShown, vehicleShownProcessed, salaryShownProcessed, waterShownProcessed,
         lightShownProcessed, stockShownProcessed, barChartData, globalFilterArray, filter });
});

router.get('/accounting/periodicAlerts', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Alertas contabilidad";
    const user = await User.findById(req.user.id);
    var periodicAlerts = user.accounting.periodicAlerts
    var categories = user.accounting.dropdowns.categories
    var concepts = user.accounting.dropdowns.concepts
    res.render('accounting/periodicAlerts', { periodicAlerts, categories, concepts });
});

router.post('/accounting/periodicAlerts/add', isAuthenticated, async (req, res) => {
    const { concept, period, priority, status } = req.body;
    var { startDate, endDate } = req.body;
    startDate = new Date(startDate.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    endDate = new Date(endDate.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    const user = await User.findById(req.user.id);
    const conceptData = await user.accounting.dropdowns.concepts.find(data => (data.concept == concept));
    const type = conceptData.type;
    const category = conceptData.category;

    const newPeriodicAlert = new PeriodicAlert({
        type, category, concept, startDate, endDate, period, priority, status
    });

    user.accounting.periodicAlerts.push(newPeriodicAlert);
    await user.save()
    await getAlerts.periodicAlerts(req.user.id)
    req.flash('success_msg', 'Aviso agregado con éxito');
    res.redirect('/accounting/periodicAlerts');
});

router.get('/getEditData/periodicAlerts/:id', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const periodicAlerts = await user.accounting.periodicAlerts.find(function (element) {
        return element._id == req.params.id;
    });
    res.json({ variable: periodicAlerts });
});

router.post('/accounting/periodicAlerts/edit/:id', isAuthenticated, async (req, res) => {
    const { concept, period, priority, status } = req.body;
    var { startDate, endDate } = req.body;

    startDate = new Date(startDate.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    if (endDate) {
        endDate = new Date(endDate.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    } else {
        endDate = undefined
    }

    const user = await User.findById(req.user.id);
    const conceptData = await user.accounting.dropdowns.concepts.find(data => (data.concept == concept));
    const type = conceptData.type;
    const category = conceptData.category;

    await user.accounting.periodicAlerts.find(function (element) {
        if (element._id == req.params.id) {
            element.type = type;
            element.category = category;
            element.concept = concept;
            element.startDate = startDate;
            element.endDate = endDate;
            element.period = period;
            element.priority = priority;
            element.status = status;
        }
    });
    await user.save();
    await getAlerts.periodicAlerts(req.user.id)
    req.flash('success_msg', 'Aviso actualizado con éxito');
    res.redirect('/accounting/periodicAlerts');
});

router.get('/accounting/periodicAlerts/delete/:id', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const periodicAlerts = await user.accounting.periodicAlerts.filter(function (element) {
        return element._id != req.params.id;
    });
    user.accounting.periodicAlerts = periodicAlerts;

    await user.save()
    await getAlerts.periodicAlerts(req.user.id)
    req.flash('success_msg', 'Aviso eliminado con éxito');
    res.redirect('/accounting/periodicAlerts');

});
/*
router.get('/notes/edit/:id', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    const note = await Note.findById(req.params.id);
    if (req.user.id == note.user) {
        res.render('notes/edit', { note });
    } else {
        res.send('<h1>No es podible modificar esta entrada desde este usuario<h1>');
    };
});

router.put('/notes/edit/:id', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    const note = await Note.findById(req.params.id);
    if (req.user.id == note.user) {
        const { title, description } = req.body;
        await Note.findByIdAndUpdate(req.params.id, { title, description });
        req.flash('success_msg', 'Nota actualizada con éxito');
        res.redirect('/notes');
    } else {
        res.send('<h1>No es podible modificar esta entrada desde este usuario<h1>');
    };
});

router.delete('/notes/delete/:id', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    const note = await Note.findById(req.params.id);
    if (req.user.id == note.user) {
        await Note.findByIdAndDelete(req.params.id);
        req.flash('success_msg', 'Nota eliminada con éxito');
        res.redirect('/notes');
    } else {
        res.send('<h1>No es podible modificar esta entrada desde este usuario<h1>');
    };
});
*/

router.get('/accounting/dropdowns', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Desplegables contabilidad";
    res.redirect('/accounting/dropdowns/category');
});

router.get('/accounting/dropdowns/category', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Desplegables contabilidad";
    const user = await User.findById(req.user.id);
    const category = user.accounting.dropdowns.categories.sort((a, b) => (a.type < b.type) ? 1 : (a.type === b.type) ? ((a.categoryPriority > b.categoryPriority) ? 1 : -1) : -1);
    const tab = 'category';
    res.render('accounting/dropdowns', { category, tab });
});

router.post('/accounting/dropdowns/category/add', isAuthenticated, async (req, res) => {
    const { type, category, categoryPriority, group } = req.body;
    newCategoryDropdown = new Category({ type, category, categoryPriority, group });
    const user = await User.findById(req.user.id);
    const repetido = await user.accounting.dropdowns.categories.some(data => (data.category == category));

    if (repetido || category.includes('/')) {
        if (repetido) {
            req.flash('error_msg', 'No se ha producido el cambio de nombre de la categoría debido a que "' + category + '" ya esta en uso');
        }
        if (category.includes('/')) {
            req.flash('error_msg', 'No se ha permite "/" en el nombre');
        }
        res.redirect('/accounting/dropdowns/category');
    } else {
        user.accounting.dropdowns.categories.push(newCategoryDropdown);
        await user.save()
        req.flash('success_msg', 'Categoría agregada con éxito');
        res.redirect('/accounting/dropdowns/category');
    }
});

router.get('/getEditData/dropdowns/category/:id', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Desplegables contabilidad";
    const user = await User.findById(req.user.id);
    const category = await user.accounting.dropdowns.categories.find(function (element) {
        return element._id == req.params.id;
    });
    res.json({ variable: category });
});

router.post('/accounting/dropdowns/category/edit/:id', isAuthenticated, async (req, res) => {
    const { type, category, categoryPriority, group } = req.body;
    const user = await User.findById(req.user.id);
    const repetido = await user.accounting.dropdowns.categories.some(data => (data.category == category && data._id != req.params.id));
    if (repetido || category.includes('/')) {
        if (repetido) {
            req.flash('error_msg', 'No se ha producido el cambio de nombre de la categoría debido a que "' + category + '" ya esta en uso');
        }
        if (category.includes('/')) {
            req.flash('error_msg', 'No se ha permite "/" en el nombre');
        }

        res.redirect('/accounting/dropdowns/category');
    } else {
        var previousName;
        await user.accounting.dropdowns.categories.find(function (element) {
            if (element._id == req.params.id) {
                previousName = element.category;
                //element.type = type; The type can't be modified
                element.category = category;
                element.categoryPriority = categoryPriority;
                //element.group = group; The group can't be modified
            }
        });
        await user.accounting.dropdowns.concepts.find(function (element2) {
            if (element2.category == previousName) {
                element2.category = category;
                element2.categoryPriority = categoryPriority;
            }
        });
        await user.accounting.cashflow.find(function (element3) {
            if (element3.category == previousName) {
                element3.category = category;
                element3.categoryPriority = categoryPriority;
            }
        });
        await user.save();

        req.flash('success_msg', 'Categoría actualizada con éxito');
        res.redirect('/accounting/dropdowns/category');
    }
});

router.get('/accounting/dropdowns/category/delete/:id', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const category = await user.accounting.dropdowns.categories.find(data => (data._id == req.params.id)).category;
    var inCashflow;
    const categories = await user.accounting.dropdowns.categories.filter(function (element) {
        if (element._id == req.params.id) {
            inCashflow = element.category;
        }
        return element._id != req.params.id;
    });
    user.accounting.dropdowns.categories = categories;

    var isInCasflow = await user.accounting.cashflow.some(data => (data.category == inCashflow));
    if (!isInCasflow) {
        const concepts = await user.accounting.dropdowns.concepts.filter(function (element) {
            return element.category != category;
        });
        user.accounting.dropdowns.concepts = concepts;

        await user.save()
        req.flash('success_msg', 'Categoría eliminada con éxito');
        res.redirect('/accounting/dropdowns/category');
    } else {
        req.flash('error_msg', 'La categoria no se ha eliminado dado que se esta usando en los movimientos');
        res.redirect('/accounting/dropdowns/category');
    }


});

router.get('/accounting/dropdowns/concept', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Desplegables contabilidad";
    const user = await User.findById(req.user.id);
    const concept = user.accounting.dropdowns.concepts.sort((a, b) => (a.type < b.type) ? 1 : (a.type === b.type) ? ((a.categoryPriority > b.categoryPriority) ? 1 : (a.categoryPriority === b.categoryPriority) ? ((a.conceptPriority > b.conceptPriority) ? 1 : -1) : -1) : -1);
    const category = user.accounting.dropdowns.categories.sort((a, b) => (a.type < b.type) ? 1 : (a.type === b.type) ? ((a.categoryPriority > b.categoryPriority) ? 1 : -1) : -1);
    const tab = 'concept';
    res.render('accounting/dropdowns', { concept, tab, category });
});

router.post('/accounting/dropdowns/concept/add', isAuthenticated, async (req, res) => {
    const { category, concept, conceptPriority, subgroup } = req.body;
    const user = await User.findById(req.user.id);
    const repetido = await user.accounting.dropdowns.concepts.some(data => (data.concept == concept));
    if (repetido || concept.includes('/')) {
        if (repetido) {
            req.flash('error_msg', 'No se ha producido el cambio de nombre del concepto debido a que "' + concept + '" ya esta en uso');
        }
        if (concept.includes('/')) {
            req.flash('error_msg', 'No se ha permite "/" en el nombre');
        }
        res.redirect('/accounting/dropdowns/concept');
    } else {
        const categoryData = await user.accounting.dropdowns.categories.find(function (element) {
            return element.category == category;
        });
        newConceptDropdown = new Concept({ category, concept, conceptPriority, subgroup });
        newConceptDropdown.type = categoryData.type;
        newConceptDropdown.group = categoryData.group;
        newConceptDropdown.categoryPriority = categoryData.categoryPriority;
        user.accounting.dropdowns.concepts.push(newConceptDropdown);
        await user.save();
        req.flash('success_msg', 'Concepto agregado con éxito');
        res.redirect('/accounting/dropdowns/concept');
    }
});

router.get('/getEditData/dropdowns/concept/:id', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Desplegables contabilidad";
    const user = await User.findById(req.user.id);
    const concept = await user.accounting.dropdowns.concepts.find(function (element) {
        return element._id == req.params.id;
    });
    res.json({ variable: concept });
});

router.post('/accounting/dropdowns/concept/edit/:id', isAuthenticated, async (req, res) => {
    const { category, concept, conceptPriority, subgroup } = req.body;
    const user = await User.findById(req.user.id);
    const repetido = await user.accounting.dropdowns.concepts.some(data => (data.concept == concept && data._id != req.params.id));
    if (repetido || concept.includes('/')) {
        if (repetido) {
            req.flash('error_msg', 'No se ha producido el cambio de nombre del concepto debido a que "' + concept + '" ya esta en uso');
        }
        if (concept.includes('/')) {
            req.flash('error_msg', 'No se ha permite "/" en el nombre');
        }

        res.redirect('/accounting/dropdowns/concept');
    } else {
        var previousName;
        await user.accounting.dropdowns.concepts.find(function (element) {
            if (element._id == req.params.id) {
                previousName = element.concept;
                //element.category = category; The category can't be modified
                element.concept = concept;
                element.conceptPriority = conceptPriority;
                //element.subgroup = subgroup; The subgroup can't be modified
            }
        });
        await user.accounting.cashflow.find(function (element2) {
            if (element2.concept == previousName) {
                element2.concept = concept;
                element2.conceptPriority = conceptPriority;
            }
        });

        await user.save();

        req.flash('success_msg', 'Concepto actualizado con éxito');
        res.redirect('/accounting/dropdowns/concept');
    }

});

router.get('/accounting/dropdowns/concept/delete/:id', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    var inCashflow;
    const concepts = await user.accounting.dropdowns.concepts.filter(function (element) {
        if (element._id == req.params.id) {
            inCashflow = element.concept;
        }
        return element._id != req.params.id;
    });
    user.accounting.dropdowns.concepts = concepts;

    var isInCasflow = await user.accounting.cashflow.some(data => (data.concept == inCashflow));
    if (!isInCasflow) {
        await user.save()
        req.flash('success_msg', 'Concepto eliminado con éxito');
        res.redirect('/accounting/dropdowns/concept');
    } else {
        req.flash('error_msg', 'El concepto no se ha eliminado dado que se esta usando en los movimientos');
        res.redirect('/accounting/dropdowns/concept');
    }


});

router.get('/accounting/dropdowns/home', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Desplegables contabilidad";
    const user = await User.findById(req.user.id);
    const home = user.accounting.dropdowns.homes;
    const tab = 'home';
    res.render('accounting/dropdowns', { home, tab });
});

router.post('/accounting/dropdowns/home/add', isAuthenticated, async (req, res) => {
    const { address, homePriority, inOwnership } = req.body;
    newHomeDropdown = new Home({ address, homePriority, inOwnership });
    const user = await User.findById(req.user.id);
    const repetido = await user.accounting.dropdowns.homes.some(data => (data.address == address));
    if (repetido) {
        req.flash('error_msg', 'No se ha producido el cambio de dirección de la vivienda debido a que "' + address + '" ya esta en uso');
        res.redirect('/accounting/dropdowns/home');
    } else {
        user.accounting.dropdowns.homes.push(newHomeDropdown);
        await user.save()
        req.flash('success_msg', 'Vivienda agregada con éxito');
        res.redirect('/accounting/dropdowns/home');
    }
});

router.get('/getEditData/dropdowns/home/:id', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Desplegables contabilidad";
    const user = await User.findById(req.user.id);
    const home = await user.accounting.dropdowns.homes.find(function (element) {
        return element._id == req.params.id;
    });
    res.json({ variable: home });
});

router.post('/accounting/dropdowns/home/edit/:id', isAuthenticated, async (req, res) => {
    const { address, homePriority, inOwnership } = req.body;
    const user = await User.findById(req.user.id);
    const repetido = await user.accounting.dropdowns.homes.some(data => (data.address == address && data._id != req.params.id));
    if (repetido) {
        req.flash('error_msg', 'No se ha producido el cambio de dirección de la vivienda debido a que "' + address + '" ya esta en uso');
        res.redirect('/accounting/dropdowns/home');
    } else {
        var previousName;
        await user.accounting.dropdowns.homes.find(function (element) {
            if (element._id == req.params.id) {
                previousName = element.address;
                element.address = address;
                element.homePriority = homePriority;
                element.inOwnership = inOwnership;
            }
        });
        await user.accounting.cashflow.find(function (element2) {
            if (element2.home == previousName) {
                element2.home = address;
                element2.homePriority = homePriority;
            }
        });
        await user.save();

        req.flash('success_msg', 'Vivienda actualizada con éxito');
        res.redirect('/accounting/dropdowns/home');
    }
});

router.get('/accounting/dropdowns/home/delete/:id', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    var inCashflow;
    const homes = await user.accounting.dropdowns.homes.filter(function (element) {
        if (element._id == req.params.id) {
            inCashflow = element.address;
        }
        return element._id != req.params.id;
    });
    user.accounting.dropdowns.homes = homes;

    var isInCasflow = await user.accounting.cashflow.some(data => (data.home == inCashflow));
    if (!isInCasflow) {
        await user.save()
        req.flash('success_msg', 'Vivienda eliminada con éxito');
        res.redirect('/accounting/dropdowns/home');
    } else {
        req.flash('error_msg', 'La vivienda no se ha eliminado dado que se esta usando en los movimientos');
        res.redirect('/accounting/dropdowns/home');
    }


});

router.get('/accounting/dropdowns/vehicle', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Desplegables contabilidad";
    const user = await User.findById(req.user.id);
    const vehicle = user.accounting.dropdowns.vehicles;
    const tab = 'vehicle';
    res.render('accounting/dropdowns', { vehicle, tab });
});

router.post('/accounting/dropdowns/vehicle/add', isAuthenticated, async (req, res) => {
    const { description, vehiclePriority, vehicleType, inOwnership } = req.body;
    newVehicleDropdown = new Vehicle({ description, vehiclePriority, vehicleType, inOwnership });
    const user = await User.findById(req.user.id);
    const repetido = await user.accounting.dropdowns.vehicles.find(data => (data.description == description));
    if (repetido) {
        req.flash('error_msg', 'No se ha producido el cambio de descripción del vehículo debido a que "' + description + '" ya esta en uso');
        res.redirect('/accounting/dropdowns/vehicle');
    } else {
        user.accounting.dropdowns.vehicles.push(newVehicleDropdown);
        await user.save()
        req.flash('success_msg', 'Vehículo agregado con éxito');
        res.redirect('/accounting/dropdowns/vehicle');
    }
});

router.get('/getEditData/dropdowns/vehicle/:id', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Desplegables contabilidad";
    const user = await User.findById(req.user.id);
    const vehicle = await user.accounting.dropdowns.vehicles.find(function (element) {
        return element._id == req.params.id;
    });
    res.json({ variable: vehicle });
});

router.post('/accounting/dropdowns/vehicle/edit/:id', isAuthenticated, async (req, res) => {
    const { description, vehiclePriority, vehicleType, inOwnership } = req.body;
    const user = await User.findById(req.user.id);
    const repetido = await user.accounting.dropdowns.vehicles.some(data => (data.description == description && data._id != req.params.id));
    if (repetido) {
        req.flash('error_msg', 'No se ha producido el cambio de descripción del vehículo debido a que "' + description + '" ya esta en uso');
        res.redirect('/accounting/dropdowns/vehicle');
    } else {
        var previousName;
        await user.accounting.dropdowns.vehicles.find(function (element) {
            if (element._id == req.params.id) {
                previousName = element.description;
                element.description = description;
                element.vehiclePriority = vehiclePriority;
                //element.vehicleType = vehicleType; The category can't be modified
                element.inOwnership = inOwnership;
            }
        });
        await user.accounting.cashflow.find(function (element2) {
            if (element2.vehicle == previousName) {
                element2.vehicle = description;
                element2.vehiclePriority = vehiclePriority;
            }
        });
        await user.save();

        req.flash('success_msg', 'Vehículo actualizado con éxito');
        res.redirect('/accounting/dropdowns/vehicle');
    }

});

router.get('/accounting/dropdowns/vehicle/delete/:id', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    var inCashflow;
    const vehicles = await user.accounting.dropdowns.vehicles.filter(function (element) {
        if (element._id == req.params.id) {
            inCashflow = element.description;
        }
        return element._id != req.params.id;
    });
    user.accounting.dropdowns.vehicles = vehicles;
    var isInCasflow = await user.accounting.cashflow.some(data => (data.vehicle == inCashflow));
    if (!isInCasflow) {
        await user.save()
        req.flash('success_msg', 'Vehículo eliminado con éxito');
        res.redirect('/accounting/dropdowns/vehicle');
    } else {
        req.flash('error_msg', 'El vehiculo no se ha eliminado dado que se esta usando en los movimientos');
        res.redirect('/accounting/dropdowns/vehicle');
    }

});

router.get('/accounting/dropdowns/entity', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Desplegables contabilidad";
    const user = await User.findById(req.user.id);
    const entity = user.accounting.dropdowns.entities;
    const tab = 'entity';
    res.render('accounting/dropdowns', { entity, tab });
});

router.post('/accounting/dropdowns/entity/add', isAuthenticated, async (req, res) => {
    const { entity, quotation, entityPriority } = req.body;
    var modificationDate = new Date();
    newEntityDropdown = new Entity({ entity, quotation, entityPriority, modificationDate });
    const user = await User.findById(req.user.id);
    const repetido = await user.accounting.dropdowns.entities.find(data => (data.entity == entity));
    if (repetido) {
        req.flash('error_msg', 'No se ha añadido la entidad debido a que "' + entity + '" ya existe');
        res.redirect('/accounting/dropdowns/vehicle');
    } else {
        user.accounting.dropdowns.entities.push(newEntityDropdown);
        await user.save()
        req.flash('success_msg', 'Entidad agregada con éxito');
        res.redirect('/accounting/dropdowns/entity');
    }
});

router.get('/getEditData/dropdowns/entity/:id', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Desplegables contabilidad";
    const user = await User.findById(req.user.id);
    const entity = await user.accounting.dropdowns.entities.find(function (element) {
        return element._id == req.params.id;
    });
    res.json({ variable: entity });
});

router.post('/accounting/dropdowns/entity/edit/:id', isAuthenticated, async (req, res) => {
    const { entity, quotation, entityPriority } = req.body;
    var modificationDate = new Date();

    const user = await User.findById(req.user.id);
    const repetido = await user.accounting.dropdowns.entities.some(data => (data.entity == entity && data._id != req.params.id));
    if (repetido) {
        req.flash('error_msg', 'No se ha producido el cambio en la entidad debido a que "' + description + '" ya existe');
        res.redirect('/accounting/dropdowns/entity');
    } else {
        var previousName;
        await user.accounting.dropdowns.entities.find(function (element) {
            if (element._id == req.params.id) {
                previousName = element.entity;
                element.entity = entity;
                element.entityPriority = entityPriority;
                element.quotation = quotation;
                element.modificationDate = modificationDate;
            }
        });
        await user.accounting.cashflow.find(function (element2) {
            if (element2.entity == previousName) {
                element2.entity = entity;
                element2.entityPriority = entityPriority;
            }
        });
        await user.save();

        req.flash('success_msg', 'Entidad actualizada con éxito');
        res.redirect('/accounting/dropdowns/entity');
    }
});

router.get('/accounting/dropdowns/entity/delete/:id', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    var inCashflow;
    const entities = await user.accounting.dropdowns.entities.filter(function (element) {
        if (element._id == req.params.id) {
            inCashflow = element.entity;
        }
        return element._id != req.params.id;
    });
    user.accounting.dropdowns.entities = entities;
    var isInCasflow = await user.accounting.cashflow.some(data => (data.entity == inCashflow));
    if (!isInCasflow) {
        await user.save()
        req.flash('success_msg', 'Entidad eliminada con éxito');
        res.redirect('/accounting/dropdowns/entity');
    } else {
        req.flash('error_msg', 'La entidad no se ha eliminado dado que se esta usando en los movimientos');
        res.redirect('/accounting/dropdowns/entity');
    }
});



router.get('/getCategoryData/:category', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const category = await user.accounting.dropdowns.categories.find(function (element) {
        return element.category == req.params.category;
    });
    const concepts = await user.accounting.dropdowns.concepts.find(function (element) {
        return element.category == req.params.category;
    });
    res.json({ category: category, concepts: concepts });
});

router.get('/getConceptData/:concept', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const concept = await user.accounting.dropdowns.concepts.find(function (element) {
        return element.concept == req.params.concept;

    });
    res.json({ concept: concept });
});
router.get('/getVehicleData/:description', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const vehicle = await user.accounting.dropdowns.vehicles.find(function (element) {
        return element.description == req.params.description;
    });
    res.json({ vehicle: vehicle });
});

router.get('/accounting/lastBankUptadeToday', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id)
    user.accounting.lastBankUpdate = new Date()
    globalVariable.lastBankUpdate = user.accounting.lastBankUpdate.toString()
    await user.save()
    res.json(globalVariable.lastBankUpdate)
});

module.exports = router;