const express = require('express');
const router = express.Router();
const User = require('../models/User');
const Element = require('../models/user/inventory/Element');
const Place = require('../models/user/inventory/dropdowns/Place');
const Type = require('../models/user/inventory/dropdowns/Type');
const { isAuthenticated } = require('../helpers/auth');


const filterInventoryElementArray = require('../helpers/filterInventoryElementArray');
const globalVariable = require('../globalVariables/variables');

router.get('/inventory/element/:tab', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Inventario";
    var inventoryFilterArray = globalVariable.inventoryFilterArray;
    const user = await User.findById(req.user.id);
    var tab = req.params.tab;
    var filter = false;
    if (tab.endsWith('Filtered')) {
        filter = true;
        tab = tab.replace('Filtered', '');
    } else {
        globalVariable.inventoryFilterArray = {};
    }
    var element = user.inventory.elements.sort((a, b) => (a.possesionLostDate > b.possesionLostDate) ? 1 : (a.possesionLostDate === b.possesionLostDate) ? ((a.acquisitionDate < b.acquisitionDate) ? 1 : -1) : -1)
    const place = user.inventory.dropdowns.places;
    const type = user.inventory.dropdowns.types;
    if (filter) {
        element = filterInventoryElementArray.filterInventoryElementArray(element, globalVariable.inventoryFilterArray);
    } else {
        element = element/*.filter(dateLimitTable)*/;
    }
    res.render('inventory/elements', { element, place, type, tab, filter, inventoryFilterArray });

});

router.post('/inventory/element/add/:tab', isAuthenticated, async (req, res) => {
    const { element, place, type, brand, identifying, acquisitionPlace, acquisitionPrice, annotation } = req.body;
    var { acquisitionDate, possesionLostDate } = req.body;
    const tab = req.params.tab;
    acquisitionDate = new Date(acquisitionDate.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    if (possesionLostDate){
        possesionLostDate = new Date(possesionLostDate.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    }
    const user = await User.findById(req.user.id);
    const newElement = new Element({
        element, place, type, brand, identifying, acquisitionPlace, acquisitionDate, acquisitionPrice, possesionLostDate, annotation
    });
    user.inventory.elements.push(newElement);
    await user.save()
    req.flash('success_msg', 'Elemento agregado con éxito');
    if (Object.keys(globalVariable.inventoryFilterArray).length === 0) {
        res.redirect('/inventory/element/' + tab);
    } else {
        res.redirect('/inventory/element/' + tab + 'Filtered');
    }

});

router.get('/getEditData/element/:id', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const element = await user.inventory.elements.find(function (element) {
        return element._id == req.params.id;
    });
    res.json({ variable: element });
});

router.post('/inventory/element/edit/:id/:tab', isAuthenticated, async (req, res) => {
    const { element, place, type, brand, identifying, acquisitionPlace, acquisitionPrice, annotation } = req.body;
    var { acquisitionDate, possesionLostDate } = req.body;
    const tab = req.params.tab;
    acquisitionDate = new Date(acquisitionDate.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    if (possesionLostDate){
        possesionLostDate = new Date(possesionLostDate.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3")); //Necesario cambiar el formato de dd/mm/yyyy a mm/dd/yyyy
    }
    const user = await User.findById(req.user.id);
    
    await user.inventory.elements.find(function (elementEdit) {
        if (elementEdit._id == req.params.id) {
            elementEdit.element = element;
            elementEdit.place = place;
            elementEdit.type = type;
            elementEdit.brand = brand;
            elementEdit.identifying = identifying;
            elementEdit.acquisitionPlace = acquisitionPlace;
            elementEdit.acquisitionDate = acquisitionDate;
            elementEdit.acquisitionPrice = acquisitionPrice;
            elementEdit.possesionLostDate = possesionLostDate;
            elementEdit.annotation = annotation;
        }
    });
    await user.save();
    req.flash('success_msg', 'Elemento actualizado con éxito');
    if (Object.keys(globalVariable.inventoryFilterArray).length === 0) {
        res.redirect('/inventory/element/' + tab);
    } else {
        res.redirect('/inventory/element/' + tab + 'Filtered');
    }

});

router.get('/inventory/element/delete/:id/:tab', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const tab = req.params.tab;
    const element = await user.inventory.elements.filter(function (element) {
        return element._id != req.params.id;
    });
    user.inventory.elements = element;

    await user.save()
    req.flash('success_msg', 'Elemento eliminado con éxito');
    res.redirect('/inventory/element/' + tab);

});

router.post('/inventory/:group/filter/:tab', isAuthenticated, async (req, res) => {
    const { placeFilter, typeFilter} = req.body;
    var filter = {};
    const tab = req.params.tab;
    const group = req.params.group;
    globalVariable.inventoryFilterArray = {}
    if (placeFilter) { filter.place = placeFilter };
    if (typeFilter) { filter.type = typeFilter };
    globalVariable.inventoryFilterArray = filter;
    if (Object.keys(globalVariable.inventoryFilterArray).length === 0) {
        res.redirect('/inventory/' + group + '/' + tab);
    } else {
        res.redirect('/inventory/' + group + '/' + tab + 'Filtered');
    }
});

router.get('/inventory/dropdowns/place', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Desplegables inventario";
    const user = await User.findById(req.user.id);
    const place = user.inventory.dropdowns.places;
    const tab = 'place';
    res.render('inventory/dropdowns', { place, tab });
});

router.post('/inventory/dropdowns/place/add', isAuthenticated, async (req, res) => {
    const { place } = req.body;
    newPlaceDropdown = new Place({ place });
    const user = await User.findById(req.user.id);
    const repetido = await user.inventory.dropdowns.places.some(data => (data.place == place));

    if (repetido || place.includes('/')) {
        if (repetido) {
            req.flash('error_msg', 'No se ha producido el cambio de nombre del tipo debido a que "' + place + '" ya esta en uso');
        }
        if (place.includes('/')) {
            req.flash('error_msg', 'No se ha permite "/" en el nombre');
        }
        res.redirect('/inventory/dropdowns/place');
    } else {
        user.inventory.dropdowns.places.push(newPlaceDropdown);
        await user.save()
        req.flash('success_msg', 'Lugar agregado con éxito');
        res.redirect('/inventory/dropdowns/place');
    }
});

router.get('/getEditData/dropdowns/place/:id', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Desplegables inventario";
    const user = await User.findById(req.user.id);
    const place = await user.inventory.dropdowns.places.find(function (element) {
        return element._id == req.params.id;
    });
    res.json({ variable: place });
});

router.post('/inventory/dropdowns/place/edit/:id', isAuthenticated, async (req, res) => {
    const { place, } = req.body;
    const user = await User.findById(req.user.id);
    const repetido = await user.inventory.dropdowns.places.some(data => (data.place == place && data._id != req.params.id));
    if (repetido || place.includes('/')) {
        if (repetido) {
            req.flash('error_msg', 'No se ha producido el cambio de nombre del tipo debido a que "' + place + '" ya esta en uso');
        }
        if (place.includes('/')) {
            req.flash('error_msg', 'No se ha permite "/" en el nombre');
        }

        res.redirect('/inventory/dropdowns/place');
    } else {
        var previousName;
        await user.inventory.dropdowns.places.find(function (element) {
            if (element._id == req.params.id) {
                previousName = element.place;
                element.place = place;
            }
        });
        await user.inventory.elements.find(function (element3) {
            if (element3.place == previousName) {
                element3.place = place;
            }
        });
        await user.save();

        req.flash('success_msg', 'Categoría actualizada con éxito');
        res.redirect('/inventory/dropdowns/place');
    }
});

router.get('/inventory/dropdowns/place/delete/:id', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const place = await user.inventory.dropdowns.places.find(data => (data._id == req.params.id)).place;
    var inInventory;
    const places = await user.inventory.dropdowns.places.filter(function (element) {
        if (element._id == req.params.id) {
            inInventory = element.place; 
        }
        return element._id != req.params.id;
    });
    user.inventory.dropdowns.places = places;

    var isInInventory = await user.inventory.elements.some(data => (data.place == inInventory));
    if (!isInInventory) {

        await user.save()
        req.flash('success_msg', 'Lugar eliminado con éxito');
        res.redirect('/inventory/dropdowns/place');
    } else {
        req.flash('error_msg', 'El lugar no se ha eliminado dado que se esta usando en los movimientos');
        res.redirect('/inventory/dropdowns/place');
    }
});

router.get('/inventory/dropdowns/type', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Desplegables inventario";
    const user = await User.findById(req.user.id);
    const type = user.inventory.dropdowns.types;
    const tab = 'type';
    res.render('inventory/dropdowns', { type, tab });
});

router.post('/inventory/dropdowns/type/add', isAuthenticated, async (req, res) => {
    const { type } = req.body;
    newTypeDropdown = new Type({ type });
    const user = await User.findById(req.user.id);
    const repetido = await user.inventory.dropdowns.types.some(data => (data.type == type));

    if (repetido || type.includes('/')) {
        if (repetido) {
            req.flash('error_msg', 'No se ha producido el cambio de nombre del tipo debido a que "' + type + '" ya esta en uso');
        }
        if (type.includes('/')) {
            req.flash('error_msg', 'No se ha permite "/" en el nombre');
        }
        res.redirect('/inventory/dropdowns/type');
    } else {
        user.inventory.dropdowns.types.push(newTypeDropdown);
        await user.save()
        req.flash('success_msg', 'Lugar agregado con éxito');
        res.redirect('/inventory/dropdowns/type');
    }
});

router.get('/getEditData/dropdowns/type/:id', isAuthenticated, async (req, res) => {
    res.locals.currentUrl = req.originalUrl;
    res.locals.dashboard = "Desplegables inventario";
    const user = await User.findById(req.user.id);
    const type = await user.inventory.dropdowns.types.find(function (element) {
        return element._id == req.params.id;
    });
    res.json({ variable: type });
});

router.post('/inventory/dropdowns/type/edit/:id', isAuthenticated, async (req, res) => {
    const { type, } = req.body;
    const user = await User.findById(req.user.id);
    const repetido = await user.inventory.dropdowns.types.some(data => (data.type == type && data._id != req.params.id));
    if (repetido || type.includes('/')) {
        if (repetido) {
            req.flash('error_msg', 'No se ha producido el cambio de nombre del tipo debido a que "' + type + '" ya esta en uso');
        }
        if (type.includes('/')) {
            req.flash('error_msg', 'No se ha permite "/" en el nombre');
        }

        res.redirect('/inventory/dropdowns/type');
    } else {
        var previousName;
        await user.inventory.dropdowns.types.find(function (element) {
            if (element._id == req.params.id) {
                previousName = element.type;
                element.type = type;
            }
        });

        await user.inventory.elements.find(function (element3) {
            if (element3.type == previousName) {
                element3.type = type;
            }
        });
        await user.save();

        req.flash('success_msg', 'Categoría actualizada con éxito');
        res.redirect('/inventory/dropdowns/type');
    }
});

router.get('/inventory/dropdowns/type/delete/:id', isAuthenticated, async (req, res) => {
    const user = await User.findById(req.user.id);
    const type = await user.inventory.dropdowns.types.find(data => (data._id == req.params.id)).type;
    var inInventory;
    const types = await user.inventory.dropdowns.types.filter(function (element) {
        if (element._id == req.params.id) {
            inInventory = element.type;
        }
        return element._id != req.params.id;
    });
    user.inventory.dropdowns.types = types;

    var isInInventory = await user.inventory.elements.some(data => (data.type == inInventory));
    if (!isInInventory) {
        await user.save()
        req.flash('success_msg', 'Lugar eliminado con éxito');
        res.redirect('/inventory/dropdowns/type');
    } else {
        req.flash('error_msg', 'El tipo no se ha eliminado dado que se esta usando en los movimientos');
        res.redirect('/inventory/dropdowns/type');
    }
});

module.exports = router;