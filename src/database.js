const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost',{
    useUnifiedTopology: true,
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    dbName: 'AGPdiario-DB'
})
    .then(db => console.log('DB is connected'))
    .catch(err => console.log(err));