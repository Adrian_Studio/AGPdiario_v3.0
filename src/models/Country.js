const mongoose = require('mongoose');
const { Schema } = mongoose;

const CountrySchema = new Schema({
        "name": { type: String },
        "name-Spanish": { type: String },
        "alpha-2": { type: String },
        "alpha-3": { type: String },
        "country-code": { type: Number },
        "iso_3166-2": { type: String },
        "region": { type: String },
        "region-Spanish": { type: String },
        "sub-region": { type: String },
        "intermediate-region": { type: String },
        "region-code": { type: Number },
        "sub-region-code": { type: Number },
        "intermediate-region-code": { type: String },
})

module.exports = mongoose.model('Country', CountrySchema)