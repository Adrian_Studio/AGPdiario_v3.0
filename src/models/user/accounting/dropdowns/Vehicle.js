const mongoose = require('mongoose');
const {Schema} = mongoose;

const VehicleSchema = new Schema({
    description: {type: String, required: true},
    vehiclePriority: {type: Number, required: true},
    vehicleType: {type: String, enum: ['Eléctrico', 'Gasolina', 'Diesel', 'Híbrido', 'Híbrido enchufable'], required: true},
    inOwnership: {type: String, enum: ['true', 'false'], required: true}, //String debido a que si esta vacio no aparece como false
    creationDate: {type: Date, default: Date.now}
}, { minimize: false })

module.exports = mongoose.model('Vehicle', VehicleSchema)