const mongoose = require('mongoose');
const {Schema} = mongoose;

const CategorySchema = new Schema({
    type: {type: String, enum: ['Ingreso', 'Gasto'], required: true},
    category: {type: String, required: true},
    categoryPriority: {type: Number, required: true},
    group: {type: String, enum: ['General', 'Vehículo', 'Vivienda'], required: true},
    creationDate: {type: Date, default: Date.now}
})

module.exports = mongoose.model('Category', CategorySchema)