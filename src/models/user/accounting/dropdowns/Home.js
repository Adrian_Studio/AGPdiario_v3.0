const mongoose = require('mongoose');
const {Schema} = mongoose;

const HomeSchema = new Schema({
    address: {type: String, required: true},
    homePriority: {type: Number, required: true},
    inOwnership: {type: String, enum: ['true', 'false'], required: true}, //String debido a que si esta vacio no aparece como false
    creationDate: {type: Date, default: Date.now}
})

module.exports = mongoose.model('Home', HomeSchema)