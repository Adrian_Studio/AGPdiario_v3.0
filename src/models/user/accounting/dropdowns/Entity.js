const mongoose = require('mongoose');
const {Schema} = mongoose;

const EntitySchema = new Schema({
    entity: {type: String, required: true},
    entityPriority: {type: Number, required: true},
    quotation: {type: Number},
    modificationDate: {type: Date},
    creationDate: {type: Date, default: Date.now}
})

module.exports = mongoose.model('Entity', EntitySchema)