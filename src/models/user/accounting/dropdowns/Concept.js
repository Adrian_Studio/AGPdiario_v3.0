const mongoose = require('mongoose');
const {Schema} = mongoose;

const ConceptSchema = new Schema({
    type: {type: String, enum: ['Ingreso', 'Gasto'], required: true},
    category: {type: String, required: true},
    categoryPriority: {type: Number, required: true},
    concept: {type: String, required: true},
    conceptPriority: {type: Number, required: true},
    group: {type: String, enum: ['General', 'Vehículo', 'Vivienda'], required: true},
    subgroup:{type: String, enum: ['', 'Nómina','Agua Vivienda','Luz Vivienda', 'Gas Vivienda','Fuente energía Vehículo', 'Mantenimiento Vehículo']},
    creationDate: {type: Date, default: Date.now}
})

module.exports = mongoose.model('Concept', ConceptSchema)