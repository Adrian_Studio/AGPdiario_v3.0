const mongoose = require('mongoose');
const {Schema} = mongoose;

const PeriodicAlertySchema = new Schema({
    type: {type: String, enum: ['Ingreso', 'Gasto'], required: true},
    category: {type: String, required: true},
    concept: {type: String, required: true},
    period: {type: Number, required: true}, //Periodicity in months
    startDate: {type: Date, required: true},
    endDate: {type: Date},
    priority: {type: Number, required: true},
    status: {type: String, enum: ['Alerta activa', 'Alerta pausada'], required: true},
    creationDate: {type: Date, default: Date.now}
})

module.exports = mongoose.model('PeriodicAlerty', PeriodicAlertySchema)