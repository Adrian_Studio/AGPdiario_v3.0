const mongoose = require('mongoose');
const {Schema} = mongoose;

const PeriodicAlertMessageSchema = new Schema({
    title: {type: String, required: true},
    text: {type: String, required: true},
    creationDate: {type: Date, default: Date.now}
})

module.exports = mongoose.model('PeriodicAlertMessage', PeriodicAlertMessageSchema)