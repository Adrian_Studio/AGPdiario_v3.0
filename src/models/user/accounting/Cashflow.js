const mongoose = require('mongoose');
const {Schema} = mongoose;

const CashflowSchema = new Schema({
    date: {type: Date},
    type: {type: String, enum: ['Ingreso', 'Gasto', 'Acciones']},
    category: {type: String},
    categoryPriority: {type: Number},
    group: {type: String, enum: ['General', 'Vehículo', 'Vivienda']},
    concept: {type: String},
    conceptPriority: {type: Number},
    subgroup:{type: String, enum: ['','Nómina','Agua Vivienda','Luz Vivienda', 'Gas Vivienda','Fuente energía Vehículo', 'Mantenimiento Vehículo', 'Compra', 'Venta', 'Dividendos']},
    amount: {type: Number},
    irpf: {type: Number},
    grossAmount: {type: Number},
    observation: {type: String},
    home: {type: String},// required: function() { return this.group === 'Vivienda'; }},
    homePriority: {type: Number},// required: function() { return this.group === 'Vivienda'; }},
    homeWaterConsumtion: {type: Number},
    homeLightConsumtion: {type: Number},
    homeGasConsumtion: {type: Number},
    vehicle: {type: String},// required: function() { return this.group === 'Vehículo'; }},
    vehiclePriority: {type: Number},// required: function() { return this.group === 'Vehículo'; }},
    vehicleType: {type: String, enum: ['Eléctrico', 'Gasolina', 'Diesel', 'Híbrido', 'Híbrido enchufable']},// required: function() { return this.group === 'Vehículo'; }},
    vehicleMilometer: {type: Number},
    vehicleGasConsumtion: {type: Number},
    vehicleLightConsumtion: {type: Number},
    entity: {type: String },
    entityPriority: {type: Number },
    stockNumber: {type: Number },
    creationDate: {type: Date, default: Date.now}
})

module.exports = mongoose.model('Cashflow', CashflowSchema)
