const mongoose = require('mongoose');
const {Schema} = mongoose;

const ElementSchema = new Schema({
    element: {type: String},
    place: {type: String},
    type: {type: String},
    brand: {type: String},
    identifying: {type: String},
    acquisitionPlace: {type: String},
    acquisitionDate: {type: Date},
    acquisitionPrice: {type: Number},
    possesionLostDate: {type: Date},
    annotation: {type: String},
    creationDate: {type: Date, default: Date.now}
})

module.exports = mongoose.model('Element', ElementSchema)
