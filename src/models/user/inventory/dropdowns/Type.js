const mongoose = require('mongoose');
const {Schema} = mongoose;

const TypeSchema = new Schema({
    type: {type: String, required: true},
    creationDate: {type: Date, default: Date.now}
})

module.exports = mongoose.model('Type', TypeSchema)