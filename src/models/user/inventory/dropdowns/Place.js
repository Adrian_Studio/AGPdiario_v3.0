const mongoose = require('mongoose');
const {Schema} = mongoose;

const PlaceSchema = new Schema({
    place: {type: String},
    creationDate: {type: Date, default: Date.now}
})

module.exports = mongoose.model('Place', PlaceSchema)