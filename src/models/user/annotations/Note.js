const mongoose = require('mongoose');
const {Schema} = mongoose;

const NoteSchema = new Schema({
    title: {type: String, required: true},
    description: {type: String, required: false},
    priority: {type: Number, required: true},
    creationDate: {type: Date, default: Date.now}
})

module.exports = mongoose.model('Note', NoteSchema)