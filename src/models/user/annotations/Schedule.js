const mongoose = require('mongoose');
const {Schema} = mongoose;

const ScheduleSchema = new Schema({
    title: {type: String, required: true},
    description: {type: String, required: false},
    date: {type: Date, required: true},
    alertDate: {type: Date},
    creationDate: {type: Date, default: Date.now}
})

module.exports = mongoose.model('Schedule', ScheduleSchema)