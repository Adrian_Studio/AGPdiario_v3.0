const mongoose = require('mongoose');
const {Schema} = mongoose;

const TasksSchema = new Schema({
    title: {type: String, required: true},
    description: {type: String, required: false},
    priority: {type: Number, required: true},
    alertActive: {type: String, enum: ['true', 'false'], required: true}, //String debido a que si esta vacio no aparece como false
    creationDate: {type: Date, default: Date.now}
})

module.exports = mongoose.model('Tasks', TasksSchema)