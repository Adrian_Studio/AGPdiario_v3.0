const mongoose = require('mongoose');
const {Schema} = mongoose;

const TripSchema = new Schema({
    date: {type: Date},
    region: {type: String},
    country: {type: String},
    countryCode: {type: String},
    latitude: {type: Number},
    longitude: {type: Number},
    place: {type: String},
    category: {type: String},
    annotation: {type: String},
    creationDate: {type: Date, default: Date.now}
})

module.exports = mongoose.model('Trip', TripSchema)
