const mongoose = require('mongoose');
const { Schema } = mongoose;
const bcrypt = require('bcryptjs');

const Note = require('./user/annotations/Note');
const Schedule = require('./user/annotations/Schedule');
const ScheduleMessages = require('./user/annotations/ScheduleMessages');
const Task = require('./user/annotations/Task');
const TaskMessages = require('./user/annotations/TaskMessages');
const Cashflow = require('./user/accounting/Cashflow');
const Category = require('./user/accounting/dropdowns/Category');
const Concept = require('./user/accounting/dropdowns/Concept');
const Home = require('./user/accounting/dropdowns/Home');
const Vehicle = require('./user/accounting/dropdowns/Vehicle');
const Entity = require('./user/accounting/dropdowns/Entity');
const PeriodicAlert = require('./user/accounting/PeriodicAlert');
const PeriodicAlertMessages = require('./user/accounting/PeriodicAlertMessages');
const Element = require('./user/inventory/Element');
const InventoryPlace = require('./user/inventory/dropdowns/Place');
const Type = require('./user/inventory/dropdowns/Type');
const Trip = require('./user/trips/Trip');
const TripMessages = require('./user/trips/TripMessages');

const UserSchema = new Schema({
    username: { type: String, required: true },
    password: { type: String, required: true },
    name: { type: String, required: true },
    surname: { type: String, required: true },
    email: { type: String, required: true },
    emailVerified: { type: Boolean, default: false, required: true },
    administrator: { type: Boolean, default: false },
    annotations: {
        notes: { type: [Note.schema] }, 
        schedule: { type: [Schedule.schema] },
        scheduleMessages: { type: [ScheduleMessages.schema] },
        tasks: { type: [Task.schema] },
        taskMessages: { type: [TaskMessages.schema] },
    },
    accounting: {
        cashflow: { type: [Cashflow.schema] },
        lastBankUpdate: {type: Date, default: null },
        dropdowns: {
            categories: { type: [Category.schema] },
            concepts: { type: [Concept.schema] },
            homes: { type: [Home.schema] },
            vehicles: { type: [Vehicle.schema] },
            entities: { type: [Entity.schema] }
        },
        periodicAlerts: { type: [PeriodicAlert.schema] },
        periodicAlertMessages: { type: [PeriodicAlertMessages.schema] }
    },
    inventory: {
        elements: { type: [Element.schema] },
        dropdowns: {
            places: { type: [InventoryPlace.schema] },
            types: { type: [Type.schema] },
        },
    },
    trips: {
        trips: { type: [Trip.schema] },
        tripsMessages: { type: [TripMessages.schema] } 
    },
    sections: {
        annotations: { type: Boolean, default: true },
        accounting: {
            section: { type: Boolean, default: true },
            stock: { type: Boolean, default: true },
            details: {
                section: { type: Boolean, default: true },
                salary: { type: Boolean, default: true },
                vehicle: { type: Boolean, default: true },
                light: { type: Boolean, default: true },
                water: { type: Boolean, default: true },
            }
        },
        inventory: { type: Boolean, default: true },
        trips: { type: Boolean, default: true },
    },
    creationDate: { type: Date, default: Date.now }
})

UserSchema.methods.encryptPassword = async (password) => {
    const salt = await bcrypt.genSalt(10);
    const hash = bcrypt.hash(password, salt);
    return hash;
};

UserSchema.methods.matchPassword = async function (password) {
    return await bcrypt.compare(password, this.password);
}

module.exports = mongoose.model('User', UserSchema)