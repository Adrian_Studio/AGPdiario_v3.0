const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const User = require('../models/User');

passport.use(new LocalStrategy({
    usernameField: 'username'
}, async (username, password, done) => {
/*  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(username)){
        var user = await User.findOne({email: username});
        var tipo = 'email'
    }else{
*/      var user = await User.findOne({username: username});
        var tipo = 'usuario'
//  }
    if(!user){
        if (tipo == 'usuario'){
            return done(null, false, {message: 'Usuario no encontrado'})
        }else if (tipo == 'email'){
            return done(null, false, {message: 'Email no encontrado'})
        }
    }else{
        if(user.emailVerified){
            const match = await user.matchPassword(password);
            if(match){
                return done(null, user);
            }else{
                return done(null, false, {message: 'Contraseña incorrecta'})
            }
        }else{
            return done(null, false, {message: 'El usuario esta creado pero no verificado. Verificar para acceder'})
        }

    }
}));

passport.serializeUser((user,done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => {
        done(err, user);
    });
});