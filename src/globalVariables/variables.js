var variable = {};

variable.globalFilterArray = {};
variable.inventoryFilterArray = {};
variable.tripsFilterArray = {};
variable.sidebarCollapsed = false;
variable.sidebarRight = {sidebar: false, userData: false, userPassword: false, sections: false};
variable.initialUrl = '/accounting/cashflow/expensesCashflow';
variable.currentUrl = variable.initialUrl;
variable.currentAlerts = {tasks: [], schedule: [], periodicAlerts: [], trips:[]};
variable.sections;
variable.signUpEnabled = false;
variable.lastBankUpdate;
module.exports = variable;