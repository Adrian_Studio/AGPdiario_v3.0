const constant = {};

constant.globalMonthsArray = [
    { number: '01', name: 'Enero', shortName: 'Ene.', numberDays: 31 },
    { number: '02', name: 'Febrero', shortName: 'Feb.', numberDays: 28 },
    { number: '03', name: 'Marzo', shortName: 'Mar.', numberDays: 31 },
    { number: '04', name: 'Abril', shortName: 'Abr.', numberDays: 30 },
    { number: '05', name: 'Mayo', shortName: 'May.', numberDays: 31 },
    { number: '06', name: 'Junio', shortName: 'Jun.', numberDays: 30 },
    { number: '07', name: 'Julio', shortName: 'Jul.', numberDays: 31 },
    { number: '08', name: 'Agosto', shortName: 'Ago.', numberDays: 31 },
    { number: '09', name: 'Septiembre', shortName: 'Sep.', numberDays: 30 },
    { number: '10', name: 'Octubre', shortName: 'Oct.', numberDays: 31 },
    { number: '11', name: 'Noviembre', shortName: 'Nov.', numberDays: 30 },
    { number: '12', name: 'Diciembre', shortName: 'Dic.', numberDays: 31 }
];

constant.tripsRegionsArray= [
    {region:"Europa"},
    {region:"Asía"},
    {region:"África"},
    {region:"América"},
    {region:"Oceanía"},
    {region:"Antarctida"}
]
constant.tripsCategoriesArray= [
    {category:"Visitado"},
    {category:"Programado"},
    {category:"Viviendo"},
    {category:"Trabajando"},
]

module.exports = constant;