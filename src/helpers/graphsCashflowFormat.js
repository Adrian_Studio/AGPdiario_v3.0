const helpers = {};

helpers.graphsCashflowBalanceChartFormat = (array) => {
    var processedArray = { Label: [], Income: [], Expense: [], Stock: [], BalanceR: [], BalanceT: [], Observation: [] };
    var balanceR = 0;
    var balanceT = 0;
    var tmp = {};
    array.sort(function (a, b) {
        a = new Date(a.date);
        b = new Date(b.date);
        return a > b ? 1 : a < b ? -1 : 0;
    });
    array.forEach(element => {
        var date = element.date;
        processedArray.Label.push(date);
        if (element.type == 'Ingreso') {
            balanceR += element.amount;
            balanceT += element.amount;
            processedArray.Income.push(element.amount);
            processedArray.Expense.push(null);
            processedArray.Stock.push(null);
            processedArray.BalanceR.push(balanceR);
            processedArray.BalanceT.push(balanceT);
            processedArray.Observation.push(element.observation);
        } else if (element.type == 'Gasto') {
            balanceR -= element.amount;
            balanceT -= element.amount;
            processedArray.Income.push(null);
            processedArray.Expense.push(element.amount);
            processedArray.Stock.push(null);
            processedArray.BalanceR.push(balanceR);
            processedArray.BalanceT.push(balanceT);
            processedArray.Observation.push(element.observation);
        } else if (element.type == 'Acciones') {
            if (!tmp[element.entity]) {
                tmp[element.entity] = { stock: 0, cashflow: 0 };
            }
            if (element.subgroup == "Compra") {
                balanceR -= element.amount;
                tmp[element.entity].stock += element.stockNumber;
                tmp[element.entity].cashflow -= element.amount;
                processedArray.BalanceR.push(balanceR);
            } else if (element.subgroup == "Venta") {
                balanceR += element.amount;
                tmp[element.entity].stock -= element.stockNumber;
                tmp[element.entity].cashflow += element.amount;
                processedArray.BalanceR.push(balanceR);
            } else if (element.subgroup == "Dividendos") {
                balanceR += element.amount;
                processedArray.BalanceR.push(balanceR);
            }

            if (tmp[element.entity].stock > 0 && tmp[element.entity].cashflow > 0) {
                balanceT += tmp[element.entity].cashflow;
                processedArray.Income.push(null);
                processedArray.Expense.push(null);
                processedArray.Stock.push(tmp[element.entity].cashflow);
                processedArray.BalanceT.push(balanceT);
                processedArray.Observation.push(element.observation);
                tmp[element.entity].cashflow = 0;
            } else if (tmp[element.entity].stock == 0 && tmp[element.entity].cashflow > 0) {
                balanceT += tmp[element.entity].cashflow;
                processedArray.Income.push(null);
                processedArray.Expense.push(null);
                processedArray.Stock.push(tmp[element.entity].cashflow);
                processedArray.BalanceT.push(balanceT);
                processedArray.Observation.push(element.observation);
                tmp[element.entity].cashflow = 0;
            } else if (tmp[element.entity].stock == 0 && tmp[element.entity].cashflow < 0) {
                balanceT += tmp[element.entity].cashflow;
                processedArray.Income.push(null);
                processedArray.Expense.push(null);
                processedArray.Stock.push(tmp[element.entity].cashflow);
                processedArray.BalanceT.push(balanceT);
                processedArray.Observation.push(element.observation);
                tmp[element.entity].cashflow = 0;
            } else {
                processedArray.Income.push(null);
                processedArray.Expense.push(null);
                processedArray.Stock.push(null);
                processedArray.BalanceT.push(balanceT);
                processedArray.Observation.push(element.observation);
            }
        };
    });
    return processedArray;
};

helpers.graphsCashflowPieChartFormat = (array) => {
    var processedArray = {};
    var tmp = {};
    
    array.sort((a, b) => (a.type < b.type) ? 1 : (a.categoryPriority === b.categoryPriority) ? ((a.conceptPriority > b.conceptPriority) ? 1 : -1) : -1);
    array.forEach(element => {
        if (element.type == 'Ingreso' || element.type == 'Gasto') {
            if (!processedArray[element.type + '-Categories']) {
                processedArray[element.type + '-Categories'] = [];
            }
            if (!processedArray[element.category + '-Concepts']) {
                processedArray[element.category + '-Concepts'] = [];
            }
        }
    });

    processedArray['Beneficios Acciones' + '-Concepts'] = [];
    processedArray['Perdidas Acciones' + '-Concepts'] = [];
    
    Object.keys(processedArray).forEach(element => {
        processedSubArray = {}

        if (element.endsWith('-Categories')) {
            type = element.replace('-Categories', "")
            typeArray = array.filter(data => (data.type == type));
            amount = 0;
            typeArray.forEach(typeItem => {
                if (!processedSubArray[typeItem.category]) { processedSubArray[typeItem.category] = 0 }
                processedSubArray[typeItem.category] += typeItem.amount;
            });
        }

        if (element.endsWith('-Concepts')) {
            category = element.replace('-Concepts', "")
            categoryArray = array.filter(data => (data.category == category));
            amount = 0;
            categoryArray.forEach(categoryItem => {
                if (!processedSubArray[categoryItem.concept]) { processedSubArray[categoryItem.concept] = 0 }
                processedSubArray[categoryItem.concept] += categoryItem.amount;
            });
        }
        Object.entries(processedSubArray).forEach(element2 => {
            processedArray[element].push(element2);
        });
    });

    var stock = array.filter(data => (data.type == 'Acciones'));
    processedSubArray = { 'Ingreso-Categories': [], 'Gasto-Categories': [] }
    stock.forEach(element => {
        if (!tmp[element.entity]) {
            tmp[element.entity] = { stock: 0, cashflow: 0 };
        }
        if (element.subgroup == "Compra") {
            tmp[element.entity].stock += element.stockNumber;
            tmp[element.entity].cashflow -= element.amount;
        } else if (element.subgroup == "Venta") {
            tmp[element.entity].stock -= element.stockNumber;
            tmp[element.entity].cashflow += element.amount;
        } else if (element.subgroup == "Dividendos") {
            if (!processedSubArray['Ingreso-Categories']['Dividendos Acciones']) { processedSubArray['Ingreso-Categories']['Dividendos Acciones'] = 0 }
            processedSubArray['Ingreso-Categories']['Dividendos Acciones'] += element.amount;
        }

        if (tmp[element.entity].stock > 0 && tmp[element.entity].cashflow > 0) {
            if (!processedSubArray['Ingreso-Categories']['Beneficios Acciones']) { processedSubArray['Ingreso-Categories']['Beneficios Acciones'] = 0 }
            processedSubArray['Ingreso-Categories']['Beneficios Acciones'] += tmp[element.entity].cashflow;
            tmp[element.entity].cashflow = 0;
        } else if (tmp[element.entity].stock == 0 && tmp[element.entity].cashflow > 0) {
            if (!processedSubArray['Ingreso-Categories']['Beneficios Acciones']) { processedSubArray['Ingreso-Categories']['Beneficios Acciones'] = 0 }
            processedSubArray['Ingreso-Categories']['Beneficios Acciones'] += tmp[element.entity].cashflow;
            tmp[element.entity].cashflow = 0;
        } else if (tmp[element.entity].stock == 0 && tmp[element.entity].cashflow < 0) {
            if (!processedSubArray['Gasto-Categories']['Perdidas Acciones']) { processedSubArray['Gasto-Categories']['Perdidas Acciones'] = 0 }
            processedSubArray['Gasto-Categories']['Perdidas Acciones'] -= tmp[element.entity].cashflow;
            tmp[element.entity].cashflow = 0;
            
        } else {
        }
    });
    
//console.log(processedSubArray['Ingreso-Categories'])
    if (processedSubArray['Ingreso-Categories']['Beneficios Acciones'] || processedSubArray['Ingreso-Categories']['Dividendos Acciones']) {
        processedArray['Ingreso-Categories'].push(['Beneficios Acciones', processedSubArray['Ingreso-Categories']['Beneficios Acciones'] + processedSubArray['Ingreso-Categories']['Dividendos Acciones']]);   
        processedArray['Beneficios Acciones' + '-Concepts'] = [];
    }
    if (processedSubArray['Ingreso-Categories']['Beneficios Acciones']) {
        processedArray['Beneficios Acciones' + '-Concepts'].push(['Beneficios Acciones', processedSubArray['Ingreso-Categories']['Beneficios Acciones']]);   
    }
    if (processedSubArray['Ingreso-Categories']['Dividendos Acciones']) {
        processedArray['Beneficios Acciones' + '-Concepts'].push(['Dividendos Acciones', processedSubArray['Ingreso-Categories']['Dividendos Acciones']]);   
    }
    if (processedSubArray['Gasto-Categories']['Perdidas Acciones']) {
        processedArray['Gasto-Categories'].push(['Perdidas Acciones',processedSubArray['Gasto-Categories']['Perdidas Acciones']]);
        processedArray['Perdidas Acciones' + '-Concepts'] = [];
        processedArray['Perdidas Acciones' + '-Concepts'].push(['Perdidas Acciones',processedSubArray['Gasto-Categories']['Perdidas Acciones']]);
    }

    //console.log(processedArray)
    return processedArray;
};

module.exports = helpers;