const helpers = {};

helpers.filterTripsArray = (array, filters) => {
    if (filters === {} ) {
        return array;
    }
    const filterEntries = Object.entries(filters);
    return array.filter(function (item) {
        var matchAll = true;
        for (let i = 0; i < filterEntries.length; i++) {
            var matchOne = false;
            const group = filterEntries[i][0];
            var comparison = filterEntries[i][1];
            switch (group) {
                default:
                    if(typeof comparison == 'string'){comparison = [comparison];}
                    for (let i = 0; i < comparison.length; i++) {
                        const element = comparison[i];
                        if (item[group] == element) {
                            matchOne = true;
                            break;
                        }
                    }
                    if (!matchOne) {
                        matchAll = false;
                    }
                    break;
            }
        }
        return matchAll
    });
};

module.exports = helpers;