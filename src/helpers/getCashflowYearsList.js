const helpers = {};

helpers.getCashflowYearsList = (array, sentido) => {
    var yearlist = [];
    array.forEach(element => {
        var year = new Date(element.date).getFullYear();
        if(!yearlist.some((position) => (position.year == year))){
            yearlist.push({year: year});
        }
            
    });
    if(sentido.toLowerCase() == 'asc')
        return yearlist.sort((a, b) => (a.year > b.year) ? 1 : -1);
    else if(sentido.toLowerCase() == 'desc'){
        return yearlist.sort((a, b) => (a.year < b.year) ? 1 : -1);
    }else{
        return yearlist;
    }
};

module.exports = helpers;