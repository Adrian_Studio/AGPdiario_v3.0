const monthArray = require('./../globalVariables/constants').globalMonthsArray;
const helpers = {};

helpers.getSummaryTotal = (array) => {
    var yearsArray = [];
    var summary = { header: [], table: [] };
    var dataArray = {};
    var tmp = {};

    array.forEach(element => {
        var year = new Date(element.date).getFullYear();
        if (!yearsArray.some((position) => (position == year))) {
            yearsArray.push(year);
        }
    });
    summary.header.push('GRUPO');
    summary.header.push('Total');
    summary.header.push('Media');
    yearsArray.sort((a, b) => (a < b) ? 1 : -1).forEach(element => {
        summary.header.push(element);
    });
    array = array.sort((a, b) => (a.date < b.date) ? 1 : -1);
    array = array.sort((a, b) => (a.type < b.type) ? 1 : (a.type === b.type) ? ((a.categoryPriority > b.categoryPriority) ? 1 : (a.categoryPriority === b.categoryPriority) ? ((a.conceptPriority > b.conceptPriority) ? 1 : -1) : -1) : -1);
    array.forEach(element => {
        var year = new Date(element.date).getFullYear();
        var benefits, losses;
        if (!dataArray['Balance (Teórico)']) {
            dataArray['Balance (Teórico)'] = {};
            dataArray['Balance (Teórico)']['Clasificacion'] = 'BalanceT';
            dataArray['Balance (Teórico)']['GRUPO'] = 'Balance (Teórico)';
            dataArray['Balance (Teórico)']['Total'] = 0;
            dataArray['Balance (Teórico)']['Media'] = undefined;

        }
        if (!dataArray['Balance (Teórico)'][year]) {
            dataArray['Balance (Teórico)'][year] = 0;
        }
        if (!dataArray['Balance (Cashflow)']) {
            dataArray['Balance (Cashflow)'] = {};
            dataArray['Balance (Cashflow)']['Clasificacion'] = 'BalanceR';
            dataArray['Balance (Cashflow)']['GRUPO'] = 'Balance (Cashflow)';
            dataArray['Balance (Cashflow)']['Total'] = 0;
            dataArray['Balance (Cashflow)']['Media'] = undefined;

        }
        if (!dataArray['Balance (Cashflow)'][year]) {
            dataArray['Balance (Cashflow)'][year] = 0;
        }

        if (!dataArray[element.type]) {
            dataArray[element.type] = {};
            dataArray[element.type]['Clasificacion'] = element.type;
            dataArray[element.type]['GRUPO'] = element.type;
            dataArray[element.type]['Total'] = 0;
            dataArray[element.type]['Media'] = undefined;
        }
        if (!dataArray[element.type][year]) {
            dataArray[element.type][year] = 0;
        }
        if (element.type == 'Ingreso' || element.type == 'Gasto') {
            if (!dataArray[element.category]) {
                dataArray[element.category] = {};
                dataArray[element.category]['Clasificacion'] = 'Categoria';
                dataArray[element.category]['GRUPO'] = element.category;
                dataArray[element.category]['Total'] = 0;
                dataArray[element.category]['Media'] = undefined;
            }
            if (!dataArray[element.category][year]) {
                dataArray[element.category][year] = 0;
            }
            if (!dataArray[element.concept]) {
                dataArray[element.concept] = {};
                dataArray[element.concept]['Clasificacion'] = 'Concepto';
                dataArray[element.concept]['GRUPO'] = element.concept;
                dataArray[element.concept]['Total'] = 0;
                dataArray[element.concept]['Media'] = undefined;
            }
            if (!dataArray[element.concept][year]) {
                dataArray[element.concept][year] = 0;
            }
            if (element.type == 'Ingreso') {
                dataArray['Balance (Teórico)']['Total'] += element.amount;
                dataArray['Balance (Teórico)'][year] += element.amount;
                dataArray['Balance (Cashflow)']['Total'] += element.amount;
                dataArray['Balance (Cashflow)'][year] += element.amount;
            } else if (element.type == 'Gasto') {
                dataArray['Balance (Teórico)']['Total'] -= element.amount;
                dataArray['Balance (Teórico)'][year] -= element.amount;
                dataArray['Balance (Cashflow)']['Total'] -= element.amount;
                dataArray['Balance (Cashflow)'][year] -= element.amount;
            }

            dataArray[element.type]['Total'] += element.amount;
            dataArray[element.type][year] += element.amount;
            dataArray[element.category]['Total'] += element.amount;
            dataArray[element.category][year] += element.amount;
            dataArray[element.concept]['Total'] += element.amount;
            dataArray[element.concept][year] += element.amount;
        } else if (element.type == 'Acciones') {
            if (!tmp[element.entity]) {
                tmp[element.entity] = { stock: 0, cashflow: 0 };
            }
            if (element.subgroup == "Compra") {
                tmp[element.entity].stock += element.stockNumber;
                tmp[element.entity].cashflow -= element.amount;
            } else if (element.subgroup == "Venta") {
                tmp[element.entity].stock -= element.stockNumber;
                tmp[element.entity].cashflow += element.amount;
            }

            if (tmp[element.entity].stock > 0 && tmp[element.entity].cashflow > 0) {
                benefits = tmp[element.entity].cashflow;
                tmp[element.entity].cashflow = 0;
            } else if (tmp[element.entity].stock == 0 && tmp[element.entity].cashflow > 0) {
                benefits = tmp[element.entity].cashflow;
                tmp[element.entity].cashflow = 0;
            } else if (tmp[element.entity].stock == 0 && tmp[element.entity].cashflow < 0) {
                losses = - tmp[element.entity].cashflow;
                tmp[element.entity].cashflow = 0;
            }
            if (element.subgroup == "Dividendos") {
                if (!dataArray[element.subgroup]) {
                    dataArray[element.subgroup] = {};
                    dataArray[element.subgroup]['Clasificacion'] = 'Concepto';
                    dataArray[element.subgroup]['GRUPO'] = element.subgroup;
                    dataArray[element.subgroup]['Total'] = 0;
                    dataArray[element.subgroup]['Media'] = undefined;
                }
                if (!dataArray[element.subgroup][year]) {
                    dataArray[element.subgroup][year] = 0;
                }
                dataArray['Balance (Teórico)']['Total'] += element.amount;
                dataArray['Balance (Teórico)'][year] += element.amount;
                dataArray['Balance (Cashflow)']['Total'] += element.amount;
                dataArray['Balance (Cashflow)'][year] += element.amount;
                dataArray[element.type]['Total'] += element.amount;
                dataArray[element.type][year] += element.amount;
                dataArray[element.subgroup]['Total'] += element.amount;
                dataArray[element.subgroup][year] += element.amount;
            }else if (element.subgroup == "Compra"){
                dataArray['Balance (Cashflow)']['Total'] -= element.amount;
                dataArray['Balance (Cashflow)'][year] -= element.amount;
            }else if (element.subgroup == "Venta"){
                dataArray['Balance (Cashflow)']['Total'] += element.amount;
                dataArray['Balance (Cashflow)'][year] += element.amount;
            }
            if (benefits) {
                if (!dataArray['Beneficios']) {
                    dataArray['Beneficios'] = {};
                    dataArray['Beneficios']['Clasificacion'] = 'Concepto';
                    dataArray['Beneficios']['GRUPO'] = 'Beneficios';
                    dataArray['Beneficios']['Total'] = 0;
                    dataArray['Beneficios']['Media'] = undefined;
                }
                if (!dataArray['Beneficios'][year]) {
                    dataArray['Beneficios'][year] = 0;
                }
                dataArray['Balance (Teórico)']['Total'] += benefits;
                dataArray['Balance (Teórico)'][year] += benefits;
                dataArray[element.type]['Total'] += benefits;
                dataArray[element.type][year] += benefits;
                dataArray['Beneficios']['Total'] += benefits;
                dataArray['Beneficios'][year] += benefits;
            } else if (losses) {
                if (!dataArray['Perdidas']) {
                    dataArray['Perdidas'] = {};
                    dataArray['Perdidas']['Clasificacion'] = 'Concepto';
                    dataArray['Perdidas']['GRUPO'] = 'Perdidas';
                    dataArray['Perdidas']['Total'] = 0;
                    dataArray['Perdidas']['Media'] = undefined;
                }
                if (!dataArray['Perdidas'][year]) {
                    dataArray['Perdidas'][year] = 0;
                }
                dataArray['Balance (Teórico)']['Total'] -= losses;
                dataArray['Balance (Teórico)'][year] -= losses;
                dataArray[element.type]['Total'] -= losses;
                dataArray[element.type][year] -= losses;
                dataArray['Perdidas']['Total'] += losses;
                dataArray['Perdidas'][year] += losses;
            }

        }
    });
    Object.values(dataArray).forEach(element => {
        dataArray[element.GRUPO].Media = element.Total / yearsArray.length;
    });
    summary.table = Object.values(dataArray);

    return summary;
};

helpers.getSummaryAnnual = (array) => {
    var summary = { header: [], table: [] };
    var dataArray = {}
    var tmp = {};

    summary.header.push('GRUPO');
    summary.header.push('Total');
    summary.header.push('Media');
    monthArray.forEach(element => {
        summary.header.push(element.name);
    });
    array = array.sort((a, b) => (a.date < b.date) ? 1 : -1);
    array = array.sort((a, b) => (a.type < b.type) ? 1 : (a.type === b.type) ? ((a.categoryPriority > b.categoryPriority) ? 1 : (a.categoryPriority === b.categoryPriority) ? ((a.conceptPriority > b.conceptPriority) ? 1 : -1) : -1) : -1);
    array.forEach(element => {
        var month = new Date(element.date).getMonth();
        var monthName = monthArray[month].name;
        var benefits, losses;
        if (!dataArray['Balance (Teórico)']) {
            dataArray['Balance (Teórico)'] = {};
            dataArray['Balance (Teórico)']['Clasificacion'] = 'BalanceT';
            dataArray['Balance (Teórico)']['GRUPO'] = 'Balance (Teórico)';
            dataArray['Balance (Teórico)']['Total'] = 0;
            dataArray['Balance (Teórico)']['Media'] = undefined;

        }
        if (!dataArray['Balance (Teórico)'][monthName]) {
            dataArray['Balance (Teórico)'][monthName] = 0;
        }
        if (!dataArray['Balance (Cashflow)']) {
            dataArray['Balance (Cashflow)'] = {};
            dataArray['Balance (Cashflow)']['Clasificacion'] = 'BalanceR';
            dataArray['Balance (Cashflow)']['GRUPO'] = 'Balance (Cashflow)';
            dataArray['Balance (Cashflow)']['Total'] = 0;
            dataArray['Balance (Cashflow)']['Media'] = undefined;

        }
        if (!dataArray['Balance (Cashflow)'][monthName]) {
            dataArray['Balance (Cashflow)'][monthName] = 0;
        }
        if (!dataArray[element.type]) {
            dataArray[element.type] = {};
            dataArray[element.type]['Clasificacion'] = element.type;
            dataArray[element.type]['GRUPO'] = element.type;
            dataArray[element.type]['Total'] = 0;
            dataArray[element.type]['Media'] = undefined;
        }
        if (!dataArray[element.type][monthName]) {
            dataArray[element.type][monthName] = 0;
        }
        if (element.type == 'Ingreso' || element.type == 'Gasto') {
            if (!dataArray[element.category]) {
                dataArray[element.category] = {};
                dataArray[element.category]['Clasificacion'] = 'Categoria';
                dataArray[element.category]['GRUPO'] = element.category;
                dataArray[element.category]['Total'] = 0;
                dataArray[element.category]['Media'] = undefined;
            }
            if (!dataArray[element.category][monthName]) {
                dataArray[element.category][monthName] = 0;
            }
            if (!dataArray[element.concept]) {
                dataArray[element.concept] = {};
                dataArray[element.concept]['Clasificacion'] = 'Concepto';
                dataArray[element.concept]['GRUPO'] = element.concept;
                dataArray[element.concept]['Total'] = 0;
                dataArray[element.concept]['Media'] = undefined;
            }
            if (!dataArray[element.concept][monthName]) {
                dataArray[element.concept][monthName] = 0;
            }
            if (element.type == 'Ingreso') {
                dataArray['Balance (Teórico)']['Total'] += element.amount;
                dataArray['Balance (Teórico)'][monthName] += element.amount;
                dataArray['Balance (Cashflow)']['Total'] += element.amount;
                dataArray['Balance (Cashflow)'][monthName] += element.amount;
            } else if (element.type == 'Gasto') {
                dataArray['Balance (Teórico)']['Total'] -= element.amount;
                dataArray['Balance (Teórico)'][monthName] -= element.amount;
                dataArray['Balance (Cashflow)']['Total'] -= element.amount;
                dataArray['Balance (Cashflow)'][monthName] -= element.amount;
            }
            dataArray[element.type]['Total'] += element.amount;
            dataArray[element.type][monthName] += element.amount;
            dataArray[element.category]['Total'] += element.amount;
            dataArray[element.category][monthName] += element.amount;
            dataArray[element.concept]['Total'] += element.amount;
            dataArray[element.concept][monthName] += element.amount;
        } else if (element.type == 'Acciones') {
            if (!tmp[element.entity]) {
                tmp[element.entity] = { stock: 0, cashflow: 0 };
            }
            if (element.subgroup == "Compra") {
                tmp[element.entity].stock += element.stockNumber;
                tmp[element.entity].cashflow -= element.amount;
            } else if (element.subgroup == "Venta") {
                tmp[element.entity].stock -= element.stockNumber;
                tmp[element.entity].cashflow += element.amount;
            }
            if (tmp[element.entity].stock > 0 && tmp[element.entity].cashflow > 0) {
                benefits = tmp[element.entity].cashflow;
                tmp[element.entity].cashflow = 0;      
            } else if (tmp[element.entity].stock == 0 && tmp[element.entity].cashflow > 0) {
                benefits = tmp[element.entity].cashflow;
                tmp[element.entity].cashflow = 0;
            } else if (tmp[element.entity].stock == 0 && tmp[element.entity].cashflow < 0) {
                losses = - tmp[element.entity].cashflow;
                tmp[element.entity].cashflow = 0;
            }
            if (element.subgroup == "Dividendos") {
                if (!dataArray[element.subgroup]) {
                    dataArray[element.subgroup] = {};
                    dataArray[element.subgroup]['Clasificacion'] = 'Concepto';
                    dataArray[element.subgroup]['GRUPO'] = element.subgroup;
                    dataArray[element.subgroup]['Total'] = 0;
                    dataArray[element.subgroup]['Media'] = undefined;
                }
                if (!dataArray[element.subgroup][monthName]) {
                    dataArray[element.subgroup][monthName] = 0;
                }
                dataArray['Balance (Teórico)']['Total'] += element.amount;
                dataArray['Balance (Teórico)'][monthName] += element.amount;
                dataArray['Balance (Cashflow)']['Total'] += element.amount;
                dataArray['Balance (Cashflow)'][monthName] += element.amount;
                dataArray[element.type]['Total'] += element.amount;
                dataArray[element.type][monthName] += element.amount;
                dataArray[element.subgroup]['Total'] += element.amount;
                dataArray[element.subgroup][monthName] += element.amount;
            }else if (element.subgroup == "Compra"){
                dataArray['Balance (Cashflow)']['Total'] -= element.amount;
                dataArray['Balance (Cashflow)'][monthName] -= element.amount;
            }else if (element.subgroup == "Venta"){
                dataArray['Balance (Cashflow)']['Total'] += element.amount;
                dataArray['Balance (Cashflow)'][monthName] += element.amount;
            }
            if (benefits) {
                if (!dataArray['Beneficios']) {
                    dataArray['Beneficios'] = {};
                    dataArray['Beneficios']['Clasificacion'] = 'Concepto';
                    dataArray['Beneficios']['GRUPO'] = 'Beneficios';
                    dataArray['Beneficios']['Total'] = 0;
                    dataArray['Beneficios']['Media'] = undefined;
                }
                if (!dataArray['Beneficios'][monthName]) {
                    dataArray['Beneficios'][monthName] = 0;
                }
                dataArray['Balance (Teórico)']['Total'] += benefits;
                dataArray['Balance (Teórico)'][monthName] += benefits;
                dataArray[element.type]['Total'] += benefits;
                dataArray[element.type][monthName] += benefits;
                dataArray['Beneficios']['Total'] += benefits;
                dataArray['Beneficios'][monthName] += benefits;
            } else if (losses) {
                if (!dataArray['Perdidas']) {
                    dataArray['Perdidas'] = {};
                    dataArray['Perdidas']['Clasificacion'] = 'Concepto';
                    dataArray['Perdidas']['GRUPO'] = 'Perdidas';
                    dataArray['Perdidas']['Total'] = 0;
                    dataArray['Perdidas']['Media'] = undefined;
                }
                if (!dataArray['Perdidas'][monthName]) {
                    dataArray['Perdidas'][monthName] = 0;
                }
                dataArray['Balance (Teórico)']['Total'] -= losses;
                dataArray['Balance (Teórico)'][monthName] -= losses;
                dataArray[element.type]['Total'] -= losses;
                dataArray[element.type][monthName] -= losses;
                dataArray['Perdidas']['Total'] += losses;
                dataArray['Perdidas'][monthName] += losses;
            }

        }
    });
    Object.values(dataArray).forEach(element => {
        dataArray[element.GRUPO].Media = element.Total / 12;
    });
    summary.table = Object.values(dataArray);

    return summary;
};

module.exports = helpers;