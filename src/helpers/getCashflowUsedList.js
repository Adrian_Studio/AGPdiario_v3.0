const helpers = {};

helpers.getCashflowUsedList = (array, field) => {
    var fieldList = [];
    array.forEach(element => {

        if (!fieldList.some((position) => (position[field + 'Used'] == element[field]))) {
            switch (field) {
                case 'type':
                    fieldList.push({ type: element['type'] });
                    break;
                case 'category':
                    if (element['type'] != 'Acciones') {
                       fieldList.push({ type: element['type'], categoryUsed: element['category'], categoryPriority: element['categoryPriority'] }); 
                    }         
                    break;
                case 'concept':
                    if (element['type'] != 'Acciones') {
                        fieldList.push({ type: element['type'], categoryUsed: element['category'], categoryPriority: element['categoryPriority'], conceptUsed: element['concept'], conceptPriority: element['conceptPriority'] });
                    }
                    break;
                case 'home':
                    fieldList.push({ homeUsed: element['home'], homePriority: element['homePriority'] });
                    break;
                case 'vehicle':
                    fieldList.push({ vehicleUsed: element['vehicle'], vehiclePriority: element['vehiclePriority'] });
                    break;
                case 'entity':
                    fieldList.push({ entityUsed: element['entity'], entityPriority: element['entityPriority'] });
                    break;
                default:
                    console.log('This field need to be defined');
                    break;
            }

        }
    });
    return fieldList;
};

module.exports = helpers;