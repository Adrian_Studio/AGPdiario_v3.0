const helpers = {};

helpers.processSalaryData = (array) => {
    var salaryData = {};
    if (array.length) {
        salaryData.amount = array.map(data => (data.amount)).reduce((a, b) => (a + b));
        salaryData.grossAmount = array.map(data => (data.grossAmount)).reduce((a, b) => (a + b));
        salaryData.taxes = salaryData.grossAmount - salaryData.amount;
        salaryData.taxesPercentage = 100 * salaryData.taxes / salaryData.grossAmount;
    } else {
        salaryData.amount = 0;
        salaryData.grossAmount = 0;
        salaryData.taxes = 0;
        salaryData.taxesPercentage = 0;
    }

    salaryData.salaryTable = { header: [], table: [] }
    salaryData.salaryTable.header = ['Fecha', 'Salario Bruto', 'Salario Neto', 'Impuestos', 'Impuestos (%)', 'Observaciones']
    array.forEach(element => {
        var taxes = element.grossAmount - element.amount;
        var taxesPercentage = 100 * taxes / element.grossAmount;
        salaryData.salaryTable.table.push({ _id: element._id, 'Fecha': element.date, 'Salario Bruto': helpers.numberFormat(element.grossAmount, 2, '€'), 'Salario Neto': helpers.numberFormat(element.amount, 2, '€'), 'Impuestos': helpers.numberFormat(taxes, 2, '€'), 'Impuestos (%)': helpers.numberFormat(taxesPercentage, 2, '%'), 'Observaciones': element.observation })
    });
    salaryData.amount = helpers.numberFormat(salaryData.amount, 2, '€');
    salaryData.grossAmount = helpers.numberFormat(salaryData.grossAmount, 2, '€');
    salaryData.taxes = helpers.numberFormat(salaryData.taxes, 2, '€');
    salaryData.taxesPercentage = helpers.numberFormat(salaryData.taxesPercentage, 2, '%');
    salaryData.salaryTable.table.sort((a, b) => (a.Fecha < b.Fecha) ? 1 : -1);
    return salaryData;
};

helpers.processVehicleData = (array) => {
    var vehicleData = {};
    var processData = {};
    var vehicleType = array[0].vehicleType;
    processData.financing = array.filter(data => (data.subgroup == ''));
    processData.powerSource = array.filter(data => (data.subgroup == 'Fuente energía Vehículo'));
    processData.maintenance = array.filter(data => (data.subgroup == 'Mantenimiento Vehículo'));
    if (processData.financing.length) {
        vehicleData.financingAmount = processData.financing.map(data => (data.amount)).reduce((a, b) => (a + b));
    } else {
        vehicleData.financingAmount = 0;
    }
    if (processData.powerSource.length) {
        vehicleData.powerSourceAmount = processData.powerSource.map(data => (data.amount)).reduce((a, b) => (a + b));
    } else {
        vehicleData.powerSourceAmount = 0;
    }
    if (processData.maintenance.length) {
        vehicleData.maintenanceAmount = processData.maintenance.map(data => (data.amount)).reduce((a, b) => (a + b));
    } else {
        vehicleData.maintenanceAmount = 0;
    }
    vehicleData.totalAmount = vehicleData.financingAmount + vehicleData.powerSourceAmount + vehicleData.maintenanceAmount;
    vehicleData.milometer = processData.powerSource.map(data => (data.vehicleMilometer)).reduce((a, b) => (a > b ? a : b));
    switch (vehicleType) {
        case 'Gasolina' || 'Diesel' || 'Híbrido':
            vehicleData.powerSourceLitres = processData.powerSource.map(data => (data.vehicleGasConsumtion)).reduce((a, b) => { if (a + b) { return (a + b) } else { return a } });
            vehicleData.gasPrice = vehicleData.powerSourceAmount / vehicleData.powerSourceLitres;
            vehicleData.gasConsumption = (vehicleData.powerSourceLitres - processData.powerSource[0].vehicleGasConsumtion) / (vehicleData.milometer / 100);
            break;
        case 'Eléctrico':
            vehicleData.powerSourceKwh = processData.powerSource.map(data => (data.vehicleLightConsumtion)).reduce((a, b) => { if (a + b) { return (a + b) } else { return a } });
            vehicleData.lightPrice = vehicleData.powerSourceAmount / vehicleData.vehicleLightConsumtion;
            vehicleData.lightConsumption = (vehicleData.powerSourceKwh - processData.powerSource[0].vehicleLightConsumtion) / (vehicleData.milometer / 100);
            break;
        case 'Híbrido enchufable':
            vehicleData.powerSourceLitres = processData.powerSource.map(data => (data.vehicleGasConsumtion)).reduce((a, b) => { if (a + b) { return (a + b) } else { return a } });
            vehicleData.gasPrice = vehicleData.powerSourceAmount / vehicleData.powerSourceLitres;
            vehicleData.gasConsumption = (vehicleData.powerSourceLitres - processData.powerSource[0].vehicleGasConsumtion) / (vehicleData.milometer / 100);
            vehicleData.powerSourceKwh = processData.powerSource.map(data => (data.vehicleLightConsumtion)).reduce((a, b) => { if (a + b) { return (a + b) } else { return a } });
            vehicleData.lightPrice = vehicleData.powerSourceAmount / vehicleData.vehicleLightConsumtion;
            vehicleData.lightConsumption = (vehicleData.powerSourceKwh - processData.powerSource[0].vehicleLightConsumtion) / (vehicleData.milometer / 100);
            break;
        default:
            if (vehicleType) {
                console.log('No esta preparado para este tipo de fuente de energía')
            }
            break;
    }
    vehicleData.kilometerPrice = (vehicleData.totalAmount - processData.powerSource[0].amount) / vehicleData.milometer;
    var previousMilometer = undefined;
    vehicleData.powerSourceTable = { header: [], table: [] }
    vehicleData.powerSourceTable.header = ['Fecha', 'Kilómetros', 'Litros', 'Importe', 'Km recorridos', 'Km/L', 'L/100Km', '€/Km', '€/L', 'Observaciones']
    processData.powerSource.forEach(element => {
        if (previousMilometer >= 0) {
            var kilometersTraveled = (element.vehicleMilometer - previousMilometer);
            var km_l = kilometersTraveled / element.vehicleGasConsumtion;
            var l_100km = element.vehicleGasConsumtion / (kilometersTraveled / 100);
            var eur_km = element.amount / kilometersTraveled;
            var eur_l = element.amount / element.vehicleGasConsumtion;
        }
        previousMilometer = element.vehicleMilometer
        vehicleData.powerSourceTable.table.push({ _id: element._id, 'Fecha': element.date, 'Kilómetros': helpers.numberFormat(element.vehicleMilometer, 0, 'km'), 'Litros': helpers.numberFormat(element.vehicleGasConsumtion, 2, 'l'), 'Importe': helpers.numberFormat(element.amount, 2, '€'), 'Km recorridos': helpers.numberFormat(kilometersTraveled, 0, 'km'), 'Km/L': helpers.numberFormat(km_l, 2, 'km/l'), 'L/100Km': helpers.numberFormat(l_100km, 2, 'l/100km'), '€/Km': helpers.numberFormat(eur_km, 3, '€/km'), '€/L': helpers.numberFormat(eur_l, 3, '€/l'), 'Observaciones': element.observation })
    });
    vehicleData.financingAmount = helpers.numberFormat(vehicleData.financingAmount, 2, '€');
    vehicleData.powerSourceAmount = helpers.numberFormat(vehicleData.powerSourceAmount, 2, '€');
    vehicleData.maintenanceAmount = helpers.numberFormat(vehicleData.maintenanceAmount, 2, '€');
    vehicleData.totalAmount = helpers.numberFormat(vehicleData.totalAmount, 2, '€');
    vehicleData.milometer = helpers.numberFormat(vehicleData.milometer, 0, 'km');
    vehicleData.powerSourceLitres = helpers.numberFormat(vehicleData.powerSourceLitres, 2, 'l');
    vehicleData.gasPrice = helpers.numberFormat(vehicleData.gasPrice, 3, '€/l');
    vehicleData.gasConsumption = helpers.numberFormat(vehicleData.gasConsumption, 2, 'l/100km');
    vehicleData.kilometerPrice = helpers.numberFormat(vehicleData.kilometerPrice, 3, '€/km');
    vehicleData.powerSourceTable.table.sort((a, b) => (a.Fecha < b.Fecha) ? 1 : -1);
    return vehicleData;
};

helpers.processWaterData = (array) => {
    var waterData = {};
    array.sort((a, b) => (a.date > b.date) ? 1 : -1);
    if (array.length) {
        waterData.amount = array.map(data => (data.amount)).reduce((a, b) => (a + b));
        waterData.homeWaterConsumtion = array.map(data => (data.homeWaterConsumtion)).reduce((a, b) => (a + b));
        waterData.waterPrice = waterData.amount / waterData.homeWaterConsumtion;
    } else {
        waterData.amount = 0;
        waterData.homeWaterConsumtion = 0;
        waterData.waterPrice = 0;
    }
    concepts = array.map(item => item.concept).filter((value, index, self) => self.indexOf(value) === index)
    waterData.waterGraph = {}
    waterData.waterGraph.Label = [];
    waterData.waterGraph.Amount = [];
    waterData.waterGraph.Dataset = [];
    waterData.waterGraph.Observation = [];
    concepts.forEach(concept => {
        waterData.waterGraph[concept] = [];
        waterData.waterGraph.Dataset.push(concept);
    });
    waterData.waterTable = { header: [], table: [] }
    waterData.waterTable.header = ['Fecha', 'Precio', 'Consumo', 'Observaciones']
    array.forEach(element => {
        waterData.waterGraph.Label.push(element.date);
        concepts.forEach(concept => {
            if (concept == element.concept) {
                waterData.waterGraph[concept].push(element.homeWaterConsumtion);
            } else {
                waterData.waterGraph[concept].push(null);
            }
        });
        waterData.waterGraph.Amount.push(element.amount);
        waterData.waterGraph.Observation.push(element.observation);
        waterData.waterTable.table.push({ _id: element._id, 'Fecha': element.date, 'Precio': helpers.numberFormat(element.amount, 2, '€'), 'Consumo': helpers.numberFormat(element.homeWaterConsumtion, 0, 'm<sup>3</sup>'), 'Observaciones': element.observation })
    });
    waterData.amount = helpers.numberFormat(waterData.amount, 2, '€');
    waterData.homeWaterConsumtion = helpers.numberFormat(waterData.homeWaterConsumtion, 0, 'm<sup>3</sup>');
    waterData.waterPrice = helpers.numberFormat(waterData.waterPrice, 2, '€/m<sup>3</sup>');
    waterData.waterTable.table.sort((a, b) => (a.Fecha < b.Fecha) ? 1 : -1);
    return waterData;
};

helpers.processLightData = (array) => {
    var lightData = {};
    array = array.filter(data => data.amount >= 0)
    array.sort((a, b) => (a.date > b.date) ? 1 : -1);
    console.log(array)
    if (array.length) {
        lightData.amount = array.map(data => (data.amount)).reduce((a, b) => (a + b));
        lightData.homeLightConsumtion = array.map(data => (data.homeLightConsumtion)).reduce((a, b) => (a + b));
        lightData.lightPrice = lightData.amount / lightData.homeLightConsumtion;
    } else {
        lightData.amount = 0;
        lightData.homeLightConsumtion = 0;
        lightData.lightPrice = 0;
    }
    concepts = array.map(item => item.concept).filter((value, index, self) => self.indexOf(value) === index)
    lightData.lightGraph = {}
    lightData.lightGraph.Label = [];
    lightData.lightGraph.Amount = [];
    lightData.lightGraph.Dataset = [];
    lightData.lightGraph.Observation = [];
    concepts.forEach(concept => {
        lightData.lightGraph[concept] = [];
        lightData.lightGraph.Dataset.push(concept);
    });
    lightData.lightTable = { header: [], table: [] }
    lightData.lightTable.header = ['Fecha', 'Precio', 'Consumo', 'Observaciones']
    array.forEach(element => {
        lightData.lightGraph.Label.push(element.date);
        concepts.forEach(concept => {
            if (concept == element.concept) {
                lightData.lightGraph[concept].push(element.homeLightConsumtion);
            } else {
                lightData.lightGraph[concept].push(null);
            }
        });
        lightData.lightGraph.Amount.push(element.amount);
        lightData.lightGraph.Observation.push(element.observation);
        lightData.lightTable.table.push({ _id: element._id, 'Fecha': element.date, 'Precio': helpers.numberFormat(element.amount, 2, '€'), 'Consumo': helpers.numberFormat(element.homeLightConsumtion, 0, 'kWh'), 'Observaciones': element.observation })
    });
    lightData.amount = helpers.numberFormat(lightData.amount, 2, '€');
    lightData.homeLightConsumtion = helpers.numberFormat(lightData.homeLightConsumtion, 0, 'kWh');
    lightData.lightPrice = helpers.numberFormat(lightData.lightPrice, 2, '€/kWh');
    lightData.lightTable.table.sort((a, b) => (a.Fecha < b.Fecha) ? 1 : -1);
    return lightData;
};

helpers.processStockData = (array) => {
    var stockData = { balanceR: 0, balanceT: 0, stockNumber: 0 };
    var tmp ={};
    array.sort((a, b) => (a.date > b.date) ? 1 : -1);

    stockData.stockGraph = {}
    stockData.stockGraph.Label = [];
    stockData.stockGraph.BalanceT = [];
    stockData.stockGraph.BalanceR = [];
    stockData.stockGraph.Dataset = ['Beneficios', 'Perdidas', 'Compra', 'Venta', 'Dividendos'];
    stockData.stockGraph.Beneficios = [];
    stockData.stockGraph.Perdidas = [];
    stockData.stockGraph.Compra = [];
    stockData.stockGraph.Venta = [];
    stockData.stockGraph.Dividendos = [];
    stockData.stockGraph.Observation = [];
    stockData.stockTable = { header: [], table: [] }
    stockData.stockTable.header = ['Fecha', 'Subgrupo','Nº Acciones <br/>Acum. Entidad', 'Entidad', 'Importe <br/>Operacion', 'Nº Acciones <br/>Operacion', 'Importe/Acción <br/>Operacion','Observaciones'];

    array.forEach(element => {
        stockData.stockGraph.Label.push(element.date);
        if (!tmp[element.entity]) {
            tmp[element.entity] = { stock: 0, cashflow: 0 };
        }
        if (element.subgroup == "Compra") {
            tmp[element.entity].stock += element.stockNumber;
            tmp[element.entity].cashflow -= element.amount;
            stockData.balanceR -= element.amount;
            stockData.stockNumber += element.stockNumber;
            stockData.stockGraph['Compra'].push(element.amount);
            stockData.stockGraph['Venta'].push(null);
            stockData.stockGraph['Dividendos'].push(null);
        } else if (element.subgroup == "Venta") {
            tmp[element.entity].stock -= element.stockNumber;
            tmp[element.entity].cashflow += element.amount;
            stockData.balanceR += element.amount;
            stockData.stockNumber -= element.stockNumber;
            stockData.stockGraph['Compra'].push(null);
            stockData.stockGraph['Venta'].push(element.amount);
            stockData.stockGraph['Dividendos'].push(null);
        } else if (element.subgroup == 'Dividendos') {
            stockData.balanceR += element.amount;
            stockData.balanceT += element.amount;
            stockData.stockGraph['Compra'].push(null);
            stockData.stockGraph['Venta'].push(null);
            stockData.stockGraph['Dividendos'].push(element.amount);
        }
        if (tmp[element.entity].stock > 0 && tmp[element.entity].cashflow > 0) {
            stockData.balanceT += tmp[element.entity].cashflow;
            stockData.stockGraph['Beneficios'].push(tmp[element.entity].cashflow);
            stockData.stockGraph['Perdidas'].push(null);
            tmp[element.entity].cashflow = 0;
        } else if (tmp[element.entity].stock == 0 && tmp[element.entity].cashflow > 0) {
            stockData.balanceT += tmp[element.entity].cashflow;
            stockData.stockGraph['Beneficios'].push(tmp[element.entity].cashflow);
            stockData.stockGraph['Perdidas'].push(null);
            tmp[element.entity].cashflow = 0;
        } else if (tmp[element.entity].stock == 0 && tmp[element.entity].cashflow < 0) {
            stockData.balanceT += tmp[element.entity].cashflow;
            stockData.stockGraph['Beneficios'].push(null);
            stockData.stockGraph['Perdidas'].push(-tmp[element.entity].cashflow);
            tmp[element.entity].cashflow = 0;
        }else{
            stockData.stockGraph['Beneficios'].push(null);
            stockData.stockGraph['Perdidas'].push(null);
        }

        stockData.stockGraph.BalanceT.push(stockData.balanceT);
        stockData.stockGraph.BalanceR.push(stockData.balanceR);
        stockData.stockGraph.Observation.push(element.observation);

        stockData.stockTable.table.push({ _id: element._id, 'Fecha': element.date, 'Subgrupo': element.type, 'Nº Acciones <br/>Acum. Entidad': tmp[element.entity].stock , 'Entidad': element.entity, 'Nº Acciones <br/>Operacion': element.stockNumber, 'Importe <br/>Operacion': element.amount, 'Importe/Acción <br/>Operacion': helpers.numberFormat(element.amount / element.stockNumber, 2, '€'), 'Observaciones': element.observation })
    });
    stockData.balanceT = helpers.numberFormat(stockData.balanceT, 2, '€');
    stockData.balanceR = helpers.numberFormat(stockData.balanceR, 2, '€');
    stockData.stockTable.table.sort((a, b) => (a.Fecha < b.Fecha) ? 1 : -1);
    return stockData;
};

helpers.numberFormat = (amount, decimalNumber, symbol) => {
    if (typeof amount != 'number') { return amount };
    return (
        amount
            .toFixed(decimalNumber) // always two decimal digits
            .replace('.', ',') // replace decimal point character with ,
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + ' ' + symbol
    )
};

module.exports = helpers;