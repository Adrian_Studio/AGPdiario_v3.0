var path = require('path');
var register = function (Handlebars) {
    var helpers = {
        formatDate: function (date) {
            return new Date(date).toLocaleString('en-GB', { year: "numeric", month: "2-digit", day: "2-digit" }).replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");
        },
        if_eq: function (conditional, comparison, options) {
            if (conditional == comparison) {
                return options.fn(this);
            } else {
                return options.inverse(this);
            }
        },
        if_includes: function (conditional, comparison, options) {
            if (conditional.includes(comparison)) {
                return options.fn(this);
            } else {
                return options.inverse(this);
            }
        },
        currency: function (amount) {
            if (typeof amount != 'number') { return amount };
            return (
                amount
                    .toFixed(2) // always two decimal digits
                    .replace('.', ',') // replace decimal point character with ,
                    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + ' €'
            )
        },
        currency3d: function (amount) {
            if (typeof amount != 'number') { return amount };
            return (
                amount
                    .toFixed(3) // always two decimal digits
                    .replace('.', ',') // replace decimal point character with ,
                    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + ' €'
            )
        },
        twoDigits: function (d) {
            return (d < 10) ? '0' + d.toString() : d.toString();
        },
        className: function (className) {
            if (className) {
                return className.replace(" ", "-") + 'Class';
            }else{
                return
            }
        },
        json: function (context) {
            return JSON.stringify(context);
        },
        if_inArray: function (array, comparison, options) {
            if (typeof array == 'string') { array = [array]; }
            if (array && array.some(data => (data == comparison))) {
                return options.fn(this);
            } else {
                return options.inverse(this);
            }
        },
        if_urlStart: function (url, start, options) {
            if (url.startsWith(start)) {
                return options.fn(this);
            } else {
                return options.inverse(this);
            }
        },
        array_lenght: function (array) {
            return array.length
        },
        eq: function (v1, v2) {
            return v1 === v2;
        },
        ne: function (v1, v2) {
            return v1 !== v2;
        },
        lt: function (v1, v2) {
            return v1 < v2;
        },
        gt: function (v1, v2) {
            return v1 > v2;
        },
        lte: function (v1, v2) {
            return v1 <= v2;
        },
        gte: function (v1, v2) {
            return v1 >= v2;
        },
        and: function () {
            return Array.prototype.slice.call(arguments).every(Boolean);
        },
        or: function () {
            return Array.prototype.slice.call(arguments, 0, -1).some(Boolean);
        },
        div: function (v1, v2) {
            return v1 / v2;
        },
    };

    if (Handlebars && typeof Handlebars.registerHelper === "function") {
        for (var prop in helpers) {
            Handlebars.registerHelper(prop, helpers[prop]);
        }
    } else {
        return helpers;
    }

};

module.exports.register = register;