const helpers = {};

helpers.filterCashflowArray = (array, filters) => {
    if (filters === {} ) {
        return array;
    }
    const filterEntries = Object.entries(filters);
    return array.filter(function (item) {
        var matchAll = true;
        for (let i = 0; i < filterEntries.length; i++) {
            var matchOne = false;
            const group = filterEntries[i][0];
            var comparison = filterEntries[i][1];
            switch (group) {
                case 'startDate':
                    var itemDate = new Date(item['date']);                    
                    var comparisonDate = new Date(comparison.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3"));
                    if (itemDate <= comparisonDate) {
                        matchAll = false;
                    }
                    break;
                case 'endDate':
                    var itemDate = new Date(item['date']);
                    var comparisonDate = new Date(comparison.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3"));
                    if (itemDate >= comparisonDate) {
                        matchAll = false;
                    }
                    break;
                case 'yearDate':
                    if(typeof comparison == 'string'){comparison = [comparison];}
                    var itemDate = new Date(item['date']).getFullYear();
                    for (let i = 0; i < comparison.length; i++) {
                        const element = comparison[i];
                        if (itemDate == element) {
                            matchOne = true;
                            break;
                        }
                    }
                    if (!matchOne) {
                        matchAll = false;
                    }
                    break;
                case 'monthDate':
                    if(typeof comparison == 'string'){comparison = [comparison];}
                    var itemDate = ("0" + (new Date(item['date']).getMonth() + 1 )).slice(-2);
                    for (let i = 0; i < comparison.length; i++) {
                        const element = comparison[i];
                        if (itemDate == element) {
                            matchOne = true;
                            break;
                        }
                    }
                    if (!matchOne) {
                        matchAll = false;
                    }
                    break;

                default:
                    if(typeof comparison == 'string'){comparison = [comparison];}
                    for (let i = 0; i < comparison.length; i++) {
                        const element = comparison[i];
                        if (item[group] == element) {
                            matchOne = true;
                            break;
                        }
                    }
                    if (!matchOne) {
                        matchAll = false;
                    }
                    break;
            }
        }
        return matchAll
    });
};


helpers.filterDetailsArray = (array, filters) => {
    if (filters === {} ) {
        return array;
    }
    const filterEntries = Object.entries(filters);
    return array.filter(function (item) {
        var matchAll = true;
        for (let i = 0; i < filterEntries.length; i++) {
            var matchOne = false;
            const group = filterEntries[i][0];
            var comparison = filterEntries[i][1];
            switch (group) {
                case 'startDate':
                    var itemDate = new Date(item['date']);                    
                    var comparisonDate = new Date(comparison.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3"));
                    if (itemDate <= comparisonDate) {
                        matchAll = false;
                    }
                    break;
                case 'endDate':
                    var itemDate = new Date(item['date']);
                    var comparisonDate = new Date(comparison.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3"));
                    if (itemDate >= comparisonDate) {
                        matchAll = false;
                    }
                    break;
                default:
                    if(typeof comparison == 'string'){comparison = [comparison];}
                    for (let i = 0; i < comparison.length; i++) {
                        const element = comparison[i];
                        if (item[group] == element) {
                            matchOne = true;
                            break;
                        }
                    }
                    if (!matchOne) {
                        matchAll = false;
                    }
                    break;
            }
        }
        return matchAll
    });
};

module.exports = helpers;