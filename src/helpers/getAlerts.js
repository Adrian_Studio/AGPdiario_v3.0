const helpers = {};
const User = require('../models/User');
const globalConstants = require('../globalVariables/constants');
const globalVariables = require('../globalVariables/variables');

helpers.updateAll = async () => {
    await helpers.scheduleAlerts();
    await sleep(500);
    await helpers.tasksAlerts();
    await sleep(500);
    await helpers.periodicAlerts();
    await sleep(500);
    await helpers.tripsAlerts();
    await sleep(500);
};

helpers.getAll = async (actualUserId) => {
    user = await User.findById(actualUserId);
    globalVariables.currentAlerts.tasks = user.annotations.taskMessages;
    globalVariables.currentAlerts.schedule = user.annotations.scheduleMessages;
    globalVariables.currentAlerts.periodicAlerts = user.accounting.periodicAlertMessages;
    globalVariables.currentAlerts.trips = user.trips.tripsMessages;
};

helpers.tasksAlerts = async (actualUserId) => {
    var users
    if (actualUserId) {
        users = [await User.findById(actualUserId)];
    } else {
        users = await User.find();
    }
    const usersId = users.map(a => a._id);
    usersId.forEach(async userId => {
        const user = await User.findById(userId)
        var tasks = user.annotations.tasks;
        var taskMessages = [];
        tasks = tasks.sort((a, b) => (a.priority > b.priority) ? 1 : -1)
        tasks.forEach(task => {
            if (task.alertActive == "true") {
                taskMessages.push({
                    title: task.title,
                    text: task.description
                })
            }
        });
        user.annotations.taskMessages = taskMessages;

        await user.save();
        if (actualUserId) {
            globalVariables.currentAlerts.tasks = taskMessages;
        }
    });

    return undefined //
};

helpers.scheduleAlerts = async (actualUserId) => {
    var users
    if (actualUserId) {
        users = [await User.findById(actualUserId)];
    } else {
        users = await User.find();
    }
    const usersId = users.map(a => a._id);
    usersId.forEach(async userId => {
        const user = await User.findById(userId)
        var schedule = user.annotations.schedule;
        var scheduleMessages = [];
        schedule = schedule.sort((a, b) => (a.priority > b.priority) ? 1 : -1)
        schedule.forEach(schedule => {
            if (schedule.alertDate) {
                var alertDate = new Date(schedule.alertDate);
                var date = new Date(schedule.date);
                if (date > new Date() && alertDate < new Date) {
                    scheduleMessages.push({
                        title: schedule.title,
                        text: schedule.description
                    })
                }
            }
        });
        user.annotations.scheduleMessages = scheduleMessages;

        await user.save();
        if (actualUserId) {
            globalVariables.currentAlerts.schedule = scheduleMessages;
        }
    });
    return undefined //
};

helpers.periodicAlerts = async (actualUserId) => {
    var users
    if (actualUserId) {
        users = [await User.findById(actualUserId)];
    } else {
        users = await User.find();
    }
    const usersId = users.map(a => a._id);
    usersId.forEach(async userId => {
        const user = await User.findById(userId)
        var cashflow = user.accounting.cashflow;
        var periodicAlerts = user.accounting.periodicAlerts;
        var periodicAlertsMessages = [];
        periodicAlerts = periodicAlerts.sort((a, b) => (a.priority > b.priority) ? 1 : -1)
        periodicAlerts.forEach(periodicAlert => {
            if (periodicAlert.status == "Alerta activa") {
                var startDate = new Date(new Date(periodicAlert.startDate).setDate(1));
                var date = new Date(periodicAlert.startDate);
                if (!periodicAlert.endDate || periodicAlert.endDate > new Date()) { var endDate = new Date() } else { var endDate = periodicAlert.endDate }
                var casflowAdded = cashflow.filter(data => (data.concept == periodicAlert.concept && data.date >= startDate && data.date <= endDate))
                while (date <= endDate) {
                    date = new Date(date);
                    if (date.getDate() != new Date(periodicAlert.startDate).getDate()) {
                        if (date.getDate() < 15) {
                            date = new Date(new Date(date).setDate(0))

                        } else {
                            date = new Date(date.setMonth(date.getMonth() + 1))
                            date = new Date(date.setDate(0))

                        }
                        if (globalConstants.globalMonthsArray.find(data => (data.number == (date.getMonth() + 1))).numberDays > new Date(periodicAlert.startDate).getDate()) {
                            date = new Date(date.setDate(new Date(periodicAlert.startDate).getDate()))
                        } else if ((date.getFullYear() % 4) == 0 && date.getMonth() == 1 && new Date(periodicAlert.startDate).getDate() >= 29) {
                            date = new Date(date.setDate(29))
                        }
                    }
                    if (!(casflowAdded.some(data => (new Date(data.date).getMonth() == new Date(date).getMonth() && new Date(data.date).getFullYear() == new Date(date).getFullYear())))) {
                        periodicAlertsMessages.push(
                            {
                                title: globalConstants.globalMonthsArray.find(data => (data.number == (date.getMonth() + 1))).name + " " + date.getFullYear() + " → " + periodicAlert.concept,
                                text: 'El importe con categoría <strong>"' + periodicAlert.category + '"</strong> y concepto <strong>"' + periodicAlert.concept + '"</strong> esta sin introducir'
                            })
                    }
                    date = date.setMonth(date.getMonth() + periodicAlert.period);
                }

            }
        });
        user.accounting.periodicAlertMessages = periodicAlertsMessages;
        await user.save();
        if (actualUserId) {
            globalVariables.currentAlerts.periodicAlerts = periodicAlertsMessages;
        }
    });
    return undefined //
};

helpers.tripsAlerts = async (actualUserId) => {
    var users
    if (actualUserId) {
        users = [await User.findById(actualUserId)];
    } else {
        users = await User.find();
    }
    const usersId = users.map(a => a._id);
    usersId.forEach(async userId => {
        const user = await User.findById(userId)
        var trips = user.trips.trips;
        var tripsMessages = [];
        trips = trips.sort((a, b) => (a.date > b.date) ? 1 : -1)
        trips.forEach(trip => {
            if (trip.category == "Programado") {
                date = new Date(trip.date);
                var textString = 'Viaje programado a <strong>"' + trip.country + '"</strong> ';
                if (trip.place) {
                    textString += 'concretamente al lugar conocido como <strong>"' + trip.place + '"</strong>'
                }
                tripsMessages.push(
                    {
                        title: globalConstants.globalMonthsArray.find(data => (data.number == (date.getMonth() + 1))).name + " " + date.getFullYear() + " → " + trip.country,
                        text: textString
                    })
            }
        });
        user.trips.tripsMessages = tripsMessages;

        await user.save();
        if (actualUserId) {
            globalVariables.currentAlerts.trips = tripsMessages;
        }
    });
    return undefined //
};

function sleep(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
}

module.exports = helpers;